theory TypeSystem
  imports Main Security Language 
begin

subsection \<open> Logic Context \<close>

type_synonym ('var,'val) TyEnv = "'var \<rightharpoonup> ('var,'val) lpred"

text \<open> {P, \<Gamma>} in further definition referred to as C \<close>
record ('var, 'val) Context =
  Pred :: "('var, 'val) lpred"
  Type :: "('var, 'val) TyEnv"

text \<open> low_or_eq P \<close>
definition stable_type
  where "stable_type P Q Re \<equiv> \<forall>mem mem' h. eval\<^sub>p mem h P \<longrightarrow> eval\<^sub>p mem h Q \<longrightarrow> (mem, mem') \<in> Re \<longrightarrow> eval\<^sub>p mem' h Q"

text \<open> stable_P \<close>
abbreviation stable_pred
  where "stable_pred P Re \<equiv> stable_type PTrue P Re"

text \<open> stable_\<Gamma> \<close>
definition stable_class
  where "stable_class P Re \<equiv> {x. \<forall>mem\<^sub>1 mem\<^sub>1' mem\<^sub>2 mem\<^sub>2' h\<^sub>1 h\<^sub>2. 
    eval\<^sub>p mem\<^sub>1 h\<^sub>1 P \<longrightarrow> eval\<^sub>p mem\<^sub>2 h\<^sub>2 P \<longrightarrow> ((mem\<^sub>1,mem\<^sub>2), (mem\<^sub>1',mem\<^sub>2')) \<in> Re \<longrightarrow> mem\<^sub>1 x = mem\<^sub>2 x \<longrightarrow> mem\<^sub>1' x = mem\<^sub>2' x}"

instantiation  Var :: (linorder,linorder) linorder
begin

definition less_Var_def: "less d1 d2 = (case d1 of Local x \<Rightarrow> (case d2 of Local y \<Rightarrow> less x y | _ \<Rightarrow> True) | Global x \<Rightarrow> (case d2 of Global y \<Rightarrow> less x y | _ \<Rightarrow> False))"
definition less_eq_Var_def: "less_eq (d1 :: ('a, 'b) Var) d2 = (d1 = d2 \<or> less d1 d2)"

instance
  apply (intro_classes; case_tac x; auto simp: less_eq_Var_def less_Var_def)
         apply (case_tac y; auto simp: less_eq_Var_def less_Var_def)
        apply (case_tac y; auto simp: less_eq_Var_def less_Var_def)
       apply (case_tac y; case_tac z; auto simp: less_eq_Var_def less_Var_def)
      apply (case_tac y; case_tac z; auto simp: less_eq_Var_def less_Var_def)
  by (case_tac y; auto simp: less_eq_Var_def less_Var_def)+
end

lemma Var_finite:
  "finite (UNIV :: 'a set) \<and> finite (UNIV :: 'b set) \<longrightarrow> finite (UNIV :: ('a,'b) Var set) "
proof -
  have "\<forall>x \<in> (UNIV :: ('a,'b) Var set). (x \<in> Global ` (UNIV :: 'b set)) \<or> (x \<in> Local ` (UNIV :: 'a set))"
    apply clarsimp
    apply (case_tac x)
    by auto
  hence "(UNIV :: ('a,'b) Var set) \<subseteq> Global ` (UNIV :: 'b set) \<union> Local ` (UNIV :: 'a set)"
    by auto
  thus ?thesis by (metis finite_Un finite_imageI subset_antisym top_greatest)
qed

instance  Var :: (finite,finite) finite
  apply (intro_classes)
  using Var_finite by force


locale sifum_types =
  sifum_lang_no_dma ev\<^sub>A ev\<^sub>B aexp_to_exp bexp_to_pred bexp_neg +
  sifum_security_no_det eval\<^sub>w 
  for ev\<^sub>A :: "(('Local :: {linorder,finite}, 'Global :: {linorder,finite}) Var, 'Val) Mem \<Rightarrow> 'AExp \<Rightarrow> 'Val"
  and ev\<^sub>B :: "(('Local,'Global) Var, 'Val) Mem \<Rightarrow> 'BExp \<Rightarrow> bool"
  and aexp_to_exp :: "'AExp \<Rightarrow> (('Local,'Global) Var,'Val) lexp"
  and bexp_to_pred :: "'BExp \<Rightarrow> (('Local,'Global) Var,'Val) lpred"
  and bexp_neg :: "'BExp \<Rightarrow> 'BExp"

sublocale sifum_types \<subseteq> sifum_security eval\<^sub>w
  by (unfold_locales , auto simp: eval\<^sub>w_det)

context sifum_types
begin

text \<open>The wellformedness properties for the environment
  1: Transitive R
  2: Reflexive R 
  3: No primed variables in R security policy
  4: No primed variables in G security policy
  5: No self references in G security policy
  6: Only one iteration of primed variables in R
\<close>
definition wfE
  where "wfE E \<equiv> 
    (\<forall>mem mem' mem''. eval\<^sub>p mem (\<lambda>n. mem') (RP E) \<and> eval\<^sub>p mem' mem'' (RP E) \<longrightarrow> eval\<^sub>p mem mem'' (RP E)) \<and>
    (\<forall>mem. eval\<^sub>p mem (\<lambda>n. mem) (RP E)) \<and>
    (\<forall>x. vars'\<^sub>p (\<L>\<^sub>R E x) = {}) \<and>
    (\<forall>x. vars'\<^sub>p (\<L>\<^sub>G E x) = {}) \<and>
    (\<forall>x. x \<notin> vars\<^sub>p (\<L>\<^sub>G E x)) \<and>
    (vars'\<^sub>p (RP E) \<subseteq> {(x, 0) |x. True})"

subsection \<open> Abbreviations \<close>

text \<open>The following abbreviations define infix operators for definitions inherited from
      locales\<close>

abbreviation eval_abv\<^sub>a ::
  "(_, ('Local,'Global) Var , 'Val) LocalConf \<Rightarrow> (('Local,'Global) Var, 'AExp, 'BExp) Action \<Rightarrow> (_, _, _) LocalConf \<Rightarrow> bool"
  ("_ \<leadsto>\<^sub>_ _" [11,0,0] 70)
  where
  "x \<leadsto>\<^sub>\<alpha> y \<equiv> (x, \<alpha>, y) \<in> eval\<^sub>a"

abbreviation eval_abv\<^sub>w ::
  "(_, ('Local,'Global) Var , 'Val) LocalConf \<Rightarrow> (_, _, _) LocalConf \<Rightarrow> bool"
  (infixl "\<leadsto>" 60)
  where
  "x \<leadsto> y \<equiv> (x, y) \<in> eval\<^sub>w"
 
abbreviation conf_det_abv ::
  "(('Local,'Global) Var,'AExp,'BExp) Stmt  \<Rightarrow> DetChoice list \<Rightarrow> (('Local,'Global) Var ,'Val) Mem  \<Rightarrow> (_,_,_,_,_) LC"
  ("\<langle>_, _, _\<rangle>" [0, 60, 120] 70)
  where
  "\<langle>c, det, mem\<rangle> \<equiv> (((c, det)), mem)"

abbreviation either :: "(('Local,'Global) Var,'Val) Mem \<Rightarrow> (_,_) Mem \<Rightarrow> (_,_) Mem \<Rightarrow> (_,_) Mem \<Rightarrow> (_,_) lpred \<Rightarrow> bool"
  where "either mem\<^sub>1 h\<^sub>1 mem\<^sub>2 h\<^sub>2 P \<equiv> eval\<^sub>p mem\<^sub>1 h\<^sub>1 P \<or> eval\<^sub>p mem\<^sub>2 h\<^sub>2 P"

abbreviation both :: "(('Local,'Global) Var,'Val) Mem \<Rightarrow> (_,_) Mem \<Rightarrow> (_,_) Mem \<Rightarrow> (_,_) Mem \<Rightarrow> (_,_) lpred \<Rightarrow> bool"
  where "both mem\<^sub>1 h\<^sub>1 mem\<^sub>2 h\<^sub>2 P \<equiv> eval\<^sub>p mem\<^sub>1 h\<^sub>1 P \<and> eval\<^sub>p mem\<^sub>2 h\<^sub>2 P"

abbreviation tdom :: "(('Local,'Global) Var, 'Val) Context \<Rightarrow> ('Local,'Global) Var set"
  where "tdom C \<equiv> dom (Type C)"

abbreviation type :: "(('Local,'Global) Var, 'Val) Context \<Rightarrow> (('Local,'Global) Var, 'Val) SP"
  where "type C \<equiv> \<lambda>x. the (Type C x)"

subsection \<open>Context Definitions\<close>

text \<open>
  TASK:Is this where \<Gamma>\<langle>x\<rangle> is defined?
\<close>
text \<open>Abstract local security policy, low if locally low or globally low\<close>
definition \<L>\<^sub>\<Gamma> :: "(('Local,'Global) Var, 'Val) Context \<Rightarrow> (('Local,'Global) Var, 'Val) Env \<Rightarrow> (('Local,'Global) Var,'Val) SP"
  where "\<L>\<^sub>\<Gamma> C E \<equiv> \<lambda>x. PDisj (\<L> E x) (if x \<in> tdom C then type C x else PFalse)"

text \<open>Concrete local security policy, considers local state only if it is defined\<close>
definition \<L>\<^sub>T :: "(('Local,'Global) Var, 'Val) Context \<Rightarrow> (('Local,'Global) Var, 'Val) Env \<Rightarrow> (('Local,'Global) Var, 'Val) SP"
  where "\<L>\<^sub>T C E \<equiv> \<lambda>x. if x \<in> tdom C then type C x else \<L> E x"

text \<open>Wellformedness for analysis context, ensures environment steps won't invalidate it\<close>
text \<open> context_wf P \<Gamma> = stable_P \<and> low_or_eq P \<and> stable_\<Gamma> \<close>
definition context_wf :: "(('Local,'Global) Var, 'Val) Context \<Rightarrow> (('Local,'Global) Var, 'Val) Env \<Rightarrow> bool"
  where "context_wf C E \<equiv> 
    stable_pred (Pred C) (R E) \<and> 
    (\<forall>x \<in> tdom C. stable_type (Pred C) (PNeg (type C x)) (R E)) \<and>
    tdom C \<subseteq> stable_class (Pred C) (R\<^sub>L E)"

text \<open>Ordering for logic context\<close>
text \<open> {P, \<Gamma>} \<ge> {P', \<Gamma>'} \<close>
definition context_ord :: "(('Local,'Global) Var,'Val) Context \<Rightarrow> (_,_) Env \<Rightarrow> (_,_) Context \<Rightarrow> bool"
  ("_ >\<^sub>_ _" [100, 100, 100] 100)
  where "C >\<^sub>E C' \<equiv> 
    ((context_wf C E \<longrightarrow> context_wf C' E) \<and> 
    (Pred C \<turnstile> Pred C') \<and> 
    (\<forall>x. PConj (Pred C) (\<L>\<^sub>\<Gamma> C' E x) \<turnstile> (\<L>\<^sub>\<Gamma> C E x)))"

text \<open>Determine the type of the reads of an Action\<close>
definition Rtype :: "(('Local,'Global) Var,'Val) Context \<Rightarrow> (_,_) Env \<Rightarrow> (_,'AExp,'BExp) Action \<Rightarrow> (_,_) lpred"
  where "Rtype C E \<alpha> \<equiv> fold PConj (map (\<L>\<^sub>T C E) (sorted_list_of_set (reads \<alpha>))) PTrue"

text \<open>Determine the type of the writes of an Action\<close>
definition Wtype :: "(('Local,'Global) Var,'Val) Env \<Rightarrow> (_,'AExp,'BExp) Action \<Rightarrow> (_,_) lpred"
  where "Wtype E \<alpha> \<equiv> case \<alpha> of x \<leftarrow> e \<Rightarrow> \<L>\<^sub>G E x | _ \<Rightarrow> PTrue"

definition \<Gamma>_map :: "(('Local,'Global) Var,'Val) TyEnv \<Rightarrow> ( (_,_) lpred \<Rightarrow> (_,_) lpred) \<Rightarrow> (('Local,'Global) Var,'Val) TyEnv"
  where "\<Gamma>_map \<Gamma> f \<equiv>  map_comp (\<lambda>x. Some (f x)) \<Gamma>"

lemma \<Gamma>_map_alt:
   "\<Gamma>_map \<Gamma> f = (\<lambda>x. if x \<in> dom \<Gamma> then Some (f (the (\<Gamma> x))) else None)"
  apply (simp add: \<Gamma>_map_def)
  using domIff by fastforce

definition \<Gamma>_filter :: "(('Local,'Global) Var,'Val) TyEnv \<Rightarrow> ('Local,'Global) Var set \<Rightarrow> (('Local,'Global) Var,'Val) TyEnv"
  where "\<Gamma>_filter \<Gamma> V \<equiv> \<lambda>x. if x \<in> V then \<Gamma> x else None"

definition noWrite :: "(('Local,'Global) Var,'Val) lpred \<Rightarrow> (('Local,'Global) Var, 'Val) Env \<Rightarrow> ('Local,'Global) Var set"
  where "noWrite P E = {x. \<forall>mem mem' h. eval\<^sub>p mem h P \<longrightarrow> (mem,mem') \<in> R E \<longrightarrow> mem x = mem' x}"

definition lowWrite :: "(('Local,'Global) Var,'Val) lpred \<Rightarrow> (('Local,'Global) Var, 'Val) Env \<Rightarrow> ('Local,'Global) Var set"
  where "lowWrite P E = {x. P \<turnstile> \<L>\<^sub>R E x}"

text \<open>Update the predicate due to an action\<close>
fun pred_act_upd :: "(('Local,'Global) Var,'Val) lpred \<Rightarrow> (_,'AExp,'BExp) Action \<Rightarrow> (_,_) lpred"
  ("_+[_]" [120, 120] 900)
  where 
    "P+[x\<leftarrow>e] = assign x (aexp_to_exp e) P" |
    "P+[[b?]]  = PConj P (bexp_to_pred b)" |
    "P+[Fence] = P"

text \<open>Update the type context due to an action\<close>
fun tyenv_act_upd :: "(('Local,'Global) Var,'Val) Context \<Rightarrow> (_,'AExp,'BExp) Action \<Rightarrow> (_,_) Env \<Rightarrow> (_,_) TyEnv"
  ("_+[_]\<^sub>_" [120, 120, 120] 900)
  where
    "C+[x\<leftarrow>e]\<^sub>E  = \<Gamma>_map ((Type C) (x := Some (Rtype C E (x \<leftarrow> e)))) (\<lambda>p. prime\<^sub>p p {x})" |
    "C+[_]\<^sub>_ = Type C"

text \<open>Update the context due to an action\<close>
definition context_act :: "(('Local,'Global) Var,'Val) Context \<Rightarrow> (_,'AExp,'BExp) Action \<Rightarrow> (_,_) Env \<Rightarrow> (_,_) Context"
  ("_ ;[_]\<^sub>_" [100, 100, 1000] 900)
  where "C;[\<alpha>]\<^sub>E = \<lparr> Pred = (Pred C)+[\<alpha>], Type = C+[\<alpha>]\<^sub>E \<rparr>"

text \<open>Update the context due to the environment\<close>
definition context_env :: "(('Local,'Global) Var,'Val) Context \<Rightarrow> (_,_) Env \<Rightarrow> (_,_) Context"
  ("_ ;[_]" [200, 100] 900)
  where "C;[E] \<equiv> \<lparr> Pred = PConj (prime\<^sub>p (Pred C) UNIV) ( (RP E)), 
                   Type = \<Gamma>_filter (\<Gamma>_map (Type C) (\<lambda>p. prime\<^sub>p p UNIV)) 
                          (
                           noWrite (PConj (prime\<^sub>p (Pred C) UNIV) ( (RP E))) E \<union>
                           lowWrite (PConj (prime\<^sub>p (Pred C) UNIV) ( (RP E))) E) \<rparr>"

subsection \<open> Type Checking Rules \<close>

definition guar :: "(('Local,'Global) Var,'Val) lpred \<Rightarrow> (_,_) Env \<Rightarrow>  (_,_,_) Action \<Rightarrow> bool"
  where "guar P E \<alpha> \<equiv> \<forall>mem h mem'. eval\<^sub>p mem h P \<and> eval_action mem \<alpha> mem' \<longrightarrow> (mem,mem') \<in> G E"

text \<open> variables that are led by a variable written in \<alpha>, i.e., ctrled(\<alpha>) \<close>
definition infl :: "(('Local,'Global) Var,'Val) Env \<Rightarrow>  (_,'AExp, 'BExp) Action \<Rightarrow> _ set"
  where "infl E \<alpha> = {x. writes \<alpha> \<inter> vars\<^sub>p (\<L> E x) \<noteq> {} }" 

fun wp :: "(('Local,'Global) Var,'Val) lpred \<Rightarrow>  (_,'AExp, 'BExp) Action \<Rightarrow> (_,_) lpred"
  where "wp P (x\<leftarrow>e) = subst\<^sub>p P (aexp_to_exp e) x"  | "wp P _ = P"

abbreviation subtype
  ("_ <:\<^sub>_ _" [400, 1000, 400] 400)
  where "subtype l P h \<equiv> Pred P \<turnstile> PImp h l"

text \<open>Type checking rules for actions\<close>
inductive 
  action_has_type :: "(_,_) Env \<Rightarrow> (_,_) Context \<Rightarrow> (_,'AExp,'BExp) Action \<Rightarrow> (_,_) Context \<Rightarrow> bool"
  ("_ \<turnstile>\<^sub>a _ {_} _" [120, 120, 120] 900)
where
  action [intro]: "\<lbrakk> Rtype C E \<alpha> <:\<^sub>C Wtype E \<alpha>; \<forall>y \<in> infl E \<alpha>. \<L>\<^sub>\<Gamma> C E y <:\<^sub>C wp (\<L> E y) \<alpha>; guar (Pred C) E \<alpha> \<rbrakk> \<Longrightarrow>
    E \<turnstile>\<^sub>a C { \<alpha> } (C;[\<alpha>]\<^sub>E;[E])"

inductive_cases action_has_type_elim [elim]: "E \<turnstile>\<^sub>a P\<^sub>1 { \<alpha> } P\<^sub>2"

text \<open>Type checking rules for statements\<close>
inductive 
  has_type :: "(_,_) Env \<Rightarrow> (('Local,'Global) Var,'Val) Context \<Rightarrow> (('Local,'Global) Var, 'AExp, 'BExp) Stmt \<Rightarrow> (_,_) Context \<Rightarrow> bool"
  ("_ \<turnstile>\<^sub>s _ {_} _" [20, 20, 20] 20)
where
  stop_type [intro]:   "E \<turnstile>\<^sub>s P { Stop } P" |
  act_type [intro]:    "\<lbrakk> E \<turnstile>\<^sub>a P\<^sub>1 { \<alpha> } P\<^sub>2 ; E \<turnstile>\<^sub>s P\<^sub>2 { c } P\<^sub>3 \<rbrakk> \<Longrightarrow> E \<turnstile>\<^sub>s P\<^sub>1 { \<alpha> ;;; c } P\<^sub>3" |
  choice_type [intro]: "\<lbrakk> E \<turnstile>\<^sub>s P\<^sub>1 { c\<^sub>1 } P\<^sub>2 ; E \<turnstile>\<^sub>s P\<^sub>1 { c\<^sub>2 } P\<^sub>2 \<rbrakk> \<Longrightarrow> E \<turnstile>\<^sub>s P\<^sub>1 { c\<^sub>1 ? c\<^sub>2 } P\<^sub>2" |
  loop_type [intro]:   "\<lbrakk> E \<turnstile>\<^sub>s P\<^sub>1 { c\<^sub>1 } P\<^sub>1 ; E \<turnstile>\<^sub>s P\<^sub>1 { c\<^sub>2 } P\<^sub>2 \<rbrakk> \<Longrightarrow> E \<turnstile>\<^sub>s P\<^sub>1 { Loop c\<^sub>1 c\<^sub>2 } P\<^sub>2" |
  rewrite [intro]:     "\<lbrakk> E \<turnstile>\<^sub>s P\<^sub>1 { c } P\<^sub>1'; P\<^sub>2 >\<^sub>E P\<^sub>1; P\<^sub>1' >\<^sub>E P\<^sub>2' \<rbrakk> \<Longrightarrow> E \<turnstile>\<^sub>s P\<^sub>2 { c } P\<^sub>2'"

subsubsection \<open> Lemmas for Expression Classifications \<close>

text \<open>
Helper lemmas for dealing with local classifications.
\<close>
lemma \<L>\<^sub>\<Gamma>_elim:
  assumes "eval\<^sub>p mem\<^sub>1 h\<^sub>1 (\<L>\<^sub>\<Gamma> P E x)"
  obtains (global) "eval\<^sub>p mem\<^sub>1 h\<^sub>1 (\<L> E x)" |
          (local) "x \<in> tdom P" "eval\<^sub>p mem\<^sub>1 h\<^sub>1 (type P x)"
  using that assms by (auto simp: \<L>\<^sub>\<Gamma>_def split: if_splits)

lemma \<L>\<^sub>\<Gamma>_delim:
  assumes "either mem\<^sub>1 h\<^sub>1 mem\<^sub>2 h\<^sub>2 (\<L>\<^sub>\<Gamma> P E x)"
  obtains (global) "either mem\<^sub>1 h\<^sub>1 mem\<^sub>2 h\<^sub>2 (\<L> E x)" |
          (local) "x \<in> tdom P" "either mem\<^sub>1 h\<^sub>1 mem\<^sub>2 h\<^sub>2 (type P x)"
  using that assms by (auto simp: \<L>\<^sub>\<Gamma>_def split: if_splits)

lemma \<L>\<^sub>T_\<L>\<^sub>\<Gamma>:
  shows "\<L>\<^sub>T P E x \<turnstile> \<L>\<^sub>\<Gamma> P E x"
  by (auto simp: \<L>\<^sub>T_def \<L>\<^sub>\<Gamma>_def entail_def split: if_splits)

lemma read_type_all_low:
  assumes "eval\<^sub>p mem h (Rtype P E \<alpha>)"
  shows "\<forall>x \<in> reads \<alpha>. eval\<^sub>p mem h (\<L>\<^sub>\<Gamma> P E x)"
proof -
  have "\<forall>P \<in> set (map (\<L>\<^sub>T P E) (sorted_list_of_set (reads \<alpha>))). eval\<^sub>p mem h P"
    using assms pred_fold_PConj Rtype_def by metis
  hence "\<forall>x \<in> reads \<alpha>. eval\<^sub>p mem h (\<L>\<^sub>T P E x)" by auto
  thus ?thesis using \<L>\<^sub>T_\<L>\<^sub>\<Gamma> by (auto simp: entail_def)
qed

subsubsection \<open> context_wf properties \<close> 

text \<open>
Properties of the context wellformedness definition, mostly related to ensuring
the context cannot be invalidated by the environment.
\<close>

lemma prime_ignore_mem:
  shows "\<forall>mem\<^sub>1 mem\<^sub>2. eval\<^sub>p mem\<^sub>1 h (prime\<^sub>p P UNIV) \<longrightarrow> eval\<^sub>p mem\<^sub>2 h (prime\<^sub>p P UNIV)"
proof -
  have "vars\<^sub>p (prime\<^sub>p P UNIV) = {}" by auto
  thus ?thesis by (metis ex_in_conv predicate_lang.lpred_eval_vars_det)
qed

lemma force_pred_stable:
  assumes "wfE E"
  shows "stable_pred (Pred (C;[E])) (R E)"
  unfolding context_env_def stable_type_def R_def RtoRel_def eval\<^sub>r_def
proof (clarsimp, intro conjI)
  fix mem h mem'
  assume "eval\<^sub>p mem h (prime\<^sub>p (Pred C) UNIV)"
  thus "eval\<^sub>p mem' h (prime\<^sub>p (Pred C) UNIV)" using prime_ignore_mem by blast
next
  fix mem h mem'
  assume "eval\<^sub>p mem h ( (RP E))" "eval\<^sub>p mem' (\<lambda> n. mem ) ( (RP E))" 
  thus "eval\<^sub>p mem' h ( (RP E))" using assms(1) unfolding wfE_def by blast (* transitive *)
qed

lemma force_type_stable:
  assumes "x \<in> tdom (C;[E])"
  shows "stable_type (Pred (C;[E])) (PNeg (type (C;[E]) x)) (R E)"
  using assms
  unfolding context_env_def stable_type_def R_def RtoRel_def eval\<^sub>r_def \<Gamma>_filter_def \<Gamma>_map_alt
  apply (auto split: if_splits)
  using prime_ignore_mem by blast+

lemma force_dom_stable:
  assumes "wfE E"
  shows "tdom (C;[E]) \<subseteq> stable_class (Pred (C;[E])) (R\<^sub>L E)"  
  unfolding  stable_class_def R_def RtoRel_def eval\<^sub>r_def 
proof (clarsimp, case_tac "x \<in> modified mem\<^sub>1 mem\<^sub>1' \<union> modified mem\<^sub>2 mem\<^sub>2'")
  fix x mem\<^sub>1 mem\<^sub>2 mem\<^sub>1' mem\<^sub>2'
  assume "(x :: ('Local,'Global) Var) \<notin> modified (mem\<^sub>1 :: (('Local,'Global) Var,'Val) Mem) mem\<^sub>1' \<union> modified mem\<^sub>2 mem\<^sub>2'" "mem\<^sub>1 x = mem\<^sub>2 x"
  thus "mem\<^sub>1' x = mem\<^sub>2' x" by (auto simp: modified_def)
next 
  fix x y assume t: "Type (C ;[E]) x = Some y"
  fix mem\<^sub>1 mem\<^sub>2 h\<^sub>1 h\<^sub>2 mem\<^sub>1' mem\<^sub>2' assume rp: "((mem\<^sub>1, mem\<^sub>2), mem\<^sub>1', mem\<^sub>2') \<in> R\<^sub>L E" 
  hence r: "(mem\<^sub>1, mem\<^sub>1') \<in> R E" "(mem\<^sub>2,mem\<^sub>2') \<in> R E" by (auto simp: R\<^sub>L_def)
  assume m: "(x :: ('Local,'Global) Var) \<in> modified (mem\<^sub>1 :: (('Local,'Global) Var,'Val) Mem) mem\<^sub>1' \<union> modified mem\<^sub>2 mem\<^sub>2'"
  assume e: "eval\<^sub>p mem\<^sub>1 h\<^sub>1 (Pred (C ;[E]))" "eval\<^sub>p mem\<^sub>2 h\<^sub>2 (Pred (C ;[E]))"
  hence e': "eval\<^sub>p mem\<^sub>1' h\<^sub>1 (Pred (C ;[E]))" "eval\<^sub>p mem\<^sub>2' h\<^sub>2 (Pred (C ;[E]))"
    using eval\<^sub>p.simps(1) force_pred_stable r(1,2) stable_type_def assms(1) by blast+
  have "x \<notin> noWrite (PConj (prime\<^sub>p (Pred C) UNIV) ( (RP E))) E"
    using m e r by (auto simp: context_env_def noWrite_def modified_def)
  hence "x \<in> lowWrite (PConj (prime\<^sub>p (Pred C) UNIV) ( (RP E))) E"
    using t by (auto simp: context_env_def \<Gamma>_filter_def split: if_splits)
  hence "PConj (prime\<^sub>p (Pred C) UNIV) ( (RP E)) \<turnstile> \<L>\<^sub>R E x"
    by (auto simp: lowWrite_def)
  hence "eval\<^sub>p mem\<^sub>1' h\<^sub>1 (\<L>\<^sub>R E x)" "eval\<^sub>p mem\<^sub>2' h\<^sub>2 (\<L>\<^sub>R E x)"
    using e' by (auto simp: context_env_def entail_def)
  moreover have "mem\<^sub>1' =\<^sup>g\<^bsub>\<L>\<^sub>R E,(modified mem\<^sub>1 mem\<^sub>1' \<union> modified mem\<^sub>2 mem\<^sub>2')\<^esub> mem\<^sub>2'"
    using rp by (auto simp: R\<^sub>L_def)
  ultimately show "mem\<^sub>1' x = mem\<^sub>2' x" using m by (auto simp: low_eq_def)
qed

lemma context_wf_upd:
  assumes "wfE E"
  shows "context_wf (P;[\<alpha>]\<^sub>E;[E]) E"
  by (meson assms(1) force_type_stable context_wf_def force_dom_stable force_pred_stable) 

subsubsection \<open> context_ord properties \<close>

text \<open>
Properties of the context ordering definition, such as refl and trans.
\<close>

lemma context_ord_refl [intro]:
  "P >\<^sub>E P"
  by (auto simp: context_ord_def entail_def)

lemma context_ord_trans:
  assumes "P\<^sub>1 >\<^sub>E P\<^sub>2"
  assumes "P\<^sub>2 >\<^sub>E P\<^sub>3"
  shows "P\<^sub>1 >\<^sub>E P\<^sub>3"
  unfolding context_ord_def
proof (intro conjI)
  show "Pred P\<^sub>1 \<turnstile> Pred P\<^sub>3" using assms entail_trans by (auto simp: context_ord_def)
next
  show "context_wf P\<^sub>1 E \<longrightarrow> context_wf P\<^sub>3 E" using assms by (auto simp: context_ord_def)
next
  have "\<forall>x. PConj (Pred P\<^sub>1) (\<L>\<^sub>\<Gamma> P\<^sub>2 E x) \<turnstile> (\<L>\<^sub>\<Gamma> P\<^sub>1 E x)" 
    using assms(1,2) by (auto simp: context_ord_def)
  thus "\<forall>x. PConj (Pred P\<^sub>1) (\<L>\<^sub>\<Gamma> P\<^sub>3 E x) \<turnstile> (\<L>\<^sub>\<Gamma> P\<^sub>1 E x)"
    using assms(1,2) by (auto simp: context_ord_def entail_def)
qed

subsubsection \<open> Additional Type Checking Rules \<close>

text \<open>
Various additional intro and elim rules for the type checking rules.
\<close>

lemma stop_elim [elim]:
  assumes "E \<turnstile>\<^sub>s P { Stop } P'"
  obtains "P >\<^sub>E P'"
  using assms
proof (induct P "Stop :: (('Local,'Global) Var, 'AExp, 'BExp) Stmt" P' rule: has_type.induct)
  case (stop_type P)
  thus ?case by auto
next
  case (rewrite P\<^sub>1 P\<^sub>1' P\<^sub>2 P\<^sub>2')
  thus ?case using context_ord_trans by blast
qed

lemma act_elim [elim]:
  assumes "E \<turnstile>\<^sub>s P { a ;;; c } P'"
  obtains P\<^sub>i P\<^sub>i' where "P >\<^sub>E P\<^sub>i" "E \<turnstile>\<^sub>a P\<^sub>i { a } P\<^sub>i'" "E \<turnstile>\<^sub>s P\<^sub>i' { c } P'"
  using assms
proof (induct P "a ;;; c" P' arbitrary: c rule: has_type.induct)
  case (act_type P\<^sub>1 P\<^sub>2 c P\<^sub>3)
  thus ?case using context_ord_refl by blast
next
  case (rewrite P\<^sub>1 P\<^sub>1' P\<^sub>2 P\<^sub>2')
  thus ?case using context_ord_refl context_ord_trans by blast
qed

lemma choice_elim [elim]:
  assumes "E \<turnstile>\<^sub>s P { c\<^sub>1 ? c\<^sub>2 } P'"
  obtains "E \<turnstile>\<^sub>s P { c\<^sub>1 } P'" "E \<turnstile>\<^sub>s P { c\<^sub>2 } P'"
  using assms by (induct P "c\<^sub>1 ? c\<^sub>2" P' rule: has_type.induct; blast)

lemma loop_elim :
  assumes "E \<turnstile>\<^sub>s P { Loop c\<^sub>1 c\<^sub>2 } P'"
  obtains Pi where "P >\<^sub>E Pi" "E \<turnstile>\<^sub>s Pi { c\<^sub>1 } Pi" "E \<turnstile>\<^sub>s Pi { c\<^sub>2 } P'"
  using assms
proof (induct P "Loop c\<^sub>1 c\<^sub>2" P' rule: has_type.induct)
  case (loop_type P\<^sub>1 P\<^sub>2)
  thus ?case using context_ord_refl by blast
next
  case (rewrite P\<^sub>1 P\<^sub>1' P\<^sub>2 P\<^sub>2')
  thus ?case using context_ord_refl context_ord_trans by blast
qed

lemma seq_type [intro]:
  assumes "E \<turnstile>\<^sub>s P { c\<^sub>1 } Q"
  assumes "E \<turnstile>\<^sub>s Q { c\<^sub>2 } P'"
  shows "E \<turnstile>\<^sub>s P { c\<^sub>1 ;; c\<^sub>2 } P'"
  using assms
proof (induct rule: has_type.induct)
  case (rewrite P\<^sub>1 c P\<^sub>1' P\<^sub>2 P\<^sub>2')
  thus ?case using context_ord_refl by blast
qed (auto)

lemma rep_type [intro]:
  assumes "E \<turnstile>\<^sub>s P { c } P"
  shows "E \<turnstile>\<^sub>s P { unroll c n } P"
  using assms by (induct arbitrary: c rule: unroll.induct) auto

lemma loop_elim_unroll [elim]:
  assumes "E \<turnstile>\<^sub>s P { Loop c\<^sub>1 c\<^sub>2 } P'"
  obtains "E \<turnstile>\<^sub>s P { unroll c\<^sub>1 n ;; c\<^sub>2 } P'"
  using assms by (meson context_ord_refl loop_elim rep_type rewrite seq_type) 

subsection \<open>Bisimulation Definition\<close>

text \<open>
We define the bisimulation to preserve a local security policy property
as well as various wellformedness properties.
\<close>
inductive \<R> ::
  "('Local,'Global,'Val,'AExp,'BExp) LC \<Rightarrow> (('Local,'Global) Var,'Val) Context \<Rightarrow> (('Local,'Global) Var,_) Env \<Rightarrow> (_,_,_,_,_) LC \<Rightarrow> bool"
  ("_ \<R>\<^bsub>_,_\<^esub> _" [20, 120, 120, 20] 1000)
  and \<R>_set :: "(('Local,'Global) Var,'Val) Context \<Rightarrow> (_,_) Env \<Rightarrow> ('Local,'Global, 'Val, 'AExp, 'BExp) LC rel"
where
  "\<R>_set P' E \<equiv> {(lc\<^sub>1, lc\<^sub>2). \<R> lc\<^sub>1 P' E lc\<^sub>2}" |
  intro [intro] :
  "\<lbrakk> c\<^sub>1 = c\<^sub>2; d\<^sub>1 = d\<^sub>2;
     E \<turnstile>\<^sub>s P { c\<^sub>1 } P';
     context_wf P E;
     mem\<^sub>1, h\<^sub>1 =\<^sup>l\<^bsub>\<L>\<^sub>\<Gamma> P E\<^esub> h\<^sub>2, mem\<^sub>2;
     both mem\<^sub>1 h\<^sub>1 mem\<^sub>2 h\<^sub>2 (Pred P) \<rbrakk> \<Longrightarrow> 
  \<langle>c\<^sub>1, d\<^sub>1, mem\<^sub>1\<rangle> \<R>\<^bsub>P',E\<^esub> \<langle>c\<^sub>2, d\<^sub>2, mem\<^sub>2\<rangle>" 

inductive_cases \<R>_elim [elim]: "\<langle>c\<^sub>1, det\<^sub>1, mem\<^sub>1\<rangle> \<R>\<^bsub>P,E\<^esub> \<langle>c\<^sub>2, det\<^sub>2, mem\<^sub>2\<rangle>"

subsection \<open>Bisimulation Stability\<close>

text \<open>
Demonstrate that the bisimulation's properties will not be invalidated by
an environment step constrained by this components rely.
\<close>

lemma type_stable:
  assumes prd: "eval\<^sub>p mem h (Pred P)"
  assumes cwf: "context_wf P E"
  assumes rel: "(mem, mem') \<in> R E"
  shows "\<forall>x \<in> tdom P. eval\<^sub>p mem' h (type P x) \<longrightarrow> eval\<^sub>p mem h (type P x)"
  using assms unfolding context_wf_def stable_type_def eval\<^sub>p.simps(5) by blast

lemma pred_stable:
  assumes cwf: "context_wf P E"
  assumes rel: "(mem, mem') \<in> R E"
  shows "eval\<^sub>p mem h (Pred P) \<longrightarrow> eval\<^sub>p mem' h (Pred P)"
  using assms unfolding context_wf_def stable_type_def by auto

lemma dom_stable:
  assumes cwf: "context_wf P E"
  assumes prd: "both mem\<^sub>1 h\<^sub>1 mem\<^sub>2 h\<^sub>2 (Pred P)"
  assumes rel: "((mem\<^sub>1, mem\<^sub>2), (mem\<^sub>1', mem\<^sub>2')) \<in> R\<^sub>L E"
  shows "\<forall>x \<in> tdom P. mem\<^sub>1 x = mem\<^sub>2 x \<longrightarrow> mem\<^sub>1' x = mem\<^sub>2' x"
  using assms unfolding context_wf_def stable_class_def by blast

lemma \<R>_stable: "stable (\<R>_set P' E) (R\<^sub>L E)"
  unfolding stable_def conf_def
proof (clarsimp)
  fix c\<^sub>1 d\<^sub>1 lm\<^sub>1 mem\<^sub>1 c\<^sub>2 d\<^sub>2 lm\<^sub>2 mem\<^sub>2 mem\<^sub>1' mem\<^sub>2'
  assume Rp: "((mem\<^sub>1, mem\<^sub>2), mem\<^sub>1', mem\<^sub>2') \<in> R\<^sub>L E"
  assume "\<langle>c\<^sub>1, d\<^sub>1, mem\<^sub>1\<rangle> \<R>\<^bsub>P',E\<^esub> \<langle>c\<^sub>2, d\<^sub>2, mem\<^sub>2\<rangle>"
  then obtain P h\<^sub>1 h\<^sub>2 where props:
       "c\<^sub>1 = c\<^sub>2" "d\<^sub>1 = d\<^sub>2" 
       "E \<turnstile>\<^sub>s P {c\<^sub>1} P'"
       "context_wf P E"
       "mem\<^sub>1, h\<^sub>1 =\<^sup>l\<^bsub>\<L>\<^sub>\<Gamma> P E\<^esub> h\<^sub>2, mem\<^sub>2"
       "both mem\<^sub>1 h\<^sub>1 mem\<^sub>2 h\<^sub>2 (Pred P)"
    by blast

  \<comment> \<open>Show the low bisimulation property is preserved across env actions\<close>
  have "mem\<^sub>1', h\<^sub>1 =\<^sup>l\<^bsub>\<L>\<^sub>\<Gamma> P E\<^esub> h\<^sub>2, mem\<^sub>2'"
    unfolding low_eq_def
  proof (intro allI impI ballI; elim \<L>\<^sub>\<Gamma>_delim)
    \<comment> \<open>The environment will preserve the global policy\<close>
    fix x assume "either mem\<^sub>1' h\<^sub>1 mem\<^sub>2' h\<^sub>2 (\<L> E x)"
    moreover have "mem\<^sub>1' =\<^sup>g\<^bsub>\<L> E\<^esub> mem\<^sub>2'" using Rp by (auto simp: R\<^sub>L_def)
    ultimately show "mem\<^sub>1' x = mem\<^sub>2' x" by (auto simp: low_eq_def)
  next
    \<comment> \<open>The environment will preserve the local context and policy, given context_wf\<close>
    fix x assume a: "x \<in> tdom P" "either mem\<^sub>1' h\<^sub>1 mem\<^sub>2' h\<^sub>2 (type P x)"
    hence "either mem\<^sub>1 h\<^sub>1 mem\<^sub>2 h\<^sub>2 (type P x)"
      using type_stable props(4,6) Rp R\<^sub>L_def by blast
    hence "mem\<^sub>1 x = mem\<^sub>2 x" 
      using a props(5) by (auto simp: \<L>\<^sub>\<Gamma>_def low_eq_def split: if_splits)
    thus "mem\<^sub>1' x = mem\<^sub>2' x"
      using dom_stable props(4,6) Rp a(1) by auto
  qed

  \<comment> \<open>Show the predicate is preserved across env actions\<close>
  moreover have "both mem\<^sub>1' h\<^sub>1 mem\<^sub>2' h\<^sub>2 (Pred P)"
    using Rp props(6) pred_stable[OF props(4)] 
    by (auto simp: R\<^sub>L_def)

  ultimately show "\<langle>c\<^sub>1, d\<^sub>1, mem\<^sub>1'\<rangle> \<R>\<^bsub>P',E\<^esub> \<langle>c\<^sub>2, d\<^sub>2, mem\<^sub>2'\<rangle>" using props by auto
qed

subsection \<open>Bisimulation Symmetry\<close>

text \<open>
Establish symmetry of the bisimulation. This is trivial.
\<close>
lemma \<R>_sym: "sym (\<R>_set P' E)"
  unfolding sym_def
proof (clarsimp)
  fix c\<^sub>1 d\<^sub>1 mem\<^sub>1 c\<^sub>2 d\<^sub>2 mem\<^sub>2 
  assume "\<langle>c\<^sub>1, d\<^sub>1, mem\<^sub>1\<rangle> \<R>\<^bsub>P',E\<^esub> \<langle>c\<^sub>2, d\<^sub>2, mem\<^sub>2\<rangle>"
  then obtain P h\<^sub>1 h\<^sub>2 where props:
       "c\<^sub>1 = c\<^sub>2" "d\<^sub>1 = d\<^sub>2" 
       "E \<turnstile>\<^sub>s P {c\<^sub>2} P'"
       "context_wf P E"
       "mem\<^sub>1, h\<^sub>1 =\<^sup>l\<^bsub>\<L>\<^sub>\<Gamma> P E\<^esub> h\<^sub>2, mem\<^sub>2"
       "both mem\<^sub>1 h\<^sub>1 mem\<^sub>2 h\<^sub>2 (Pred P)"
    by blast
  thus "\<langle>c\<^sub>2, d\<^sub>2, mem\<^sub>2\<rangle> \<R>\<^bsub>P',E\<^esub> \<langle>c\<^sub>1, d\<^sub>1, mem\<^sub>1\<rangle>" 
    by (intro \<R>.intro, auto simp: low_eq_def)
qed

subsection \<open>Bisimulation Step\<close>

subsubsection \<open> \<Gamma> map and filter lemmas \<close>

text \<open>
Various properties over the type domain of \<Gamma>.
\<close>

lemma \<Gamma>_map_dom [simp]:
  shows "dom (\<Gamma>_map \<Gamma> f) = dom \<Gamma>"
  unfolding \<Gamma>_map_def using map_comp_None_iff by fastforce

lemma \<Gamma>_filter_dom [simp]:
  shows "dom (\<Gamma>_filter \<Gamma> V) = dom \<Gamma> \<inter> V"
  unfolding \<Gamma>_filter_def by (auto split: if_splits)

lemma tdom_upd [simp]:
  shows "tdom (P;[\<alpha>]\<^sub>E) = tdom P \<union> writes \<alpha>"
  by (cases \<alpha>; auto simp: context_act_def)

lemma tdom_env_upd :
  shows "tdom (P;[E]) \<subseteq> tdom P"
  by (auto simp: context_env_def)

lemma tdom_eval_upd:
  assumes "E \<turnstile>\<^sub>a P {\<alpha>} P'"
  shows "tdom P' \<subseteq> tdom P \<union> writes \<alpha>"
proof -
  have "P' = P;[\<alpha>]\<^sub>E;[E]" using assms(1) by (elim action_has_type_elim)
  thus ?thesis using tdom_env_upd tdom_upd by blast 
qed

subsubsection \<open> Context Update Lemmas \<close>

text \<open>
This section handles the complexities of temporary variables, used when updating
P and \<Gamma> based on an action or the environment.
Builds show that we preserve information across type checking an action,
such that the unmodified entries in \<Gamma> are equivalent and predicates are equivalent
across the pre and post memories.
\<close>

abbreviation full_mem_upd
  ("_,_ \<leadsto>\<^sub>_ _,_" 100)
  where "mem,h \<leadsto>\<^sub>\<alpha> mem',h' \<equiv> 
      eval_action mem \<alpha> mem' \<and> h' = h_upd (h_upd h mem (writes \<alpha>)) mem' UNIV"

lemma pred_upd [simp]:
  assumes "eval_action mem \<alpha> mem'"
  shows "eval\<^sub>p mem' (h_upd h mem (writes \<alpha>)) (P+[\<alpha>]) = eval\<^sub>p mem h P"  
proof (cases \<alpha>)
  case (Assign x e)
  hence "mem' = mem (x := eval\<^sub>e mem h (aexp_to_exp e))" 
    using assms by (simp add: aexp_to_exp_correct)
  then show ?thesis using assign_eval Assign by (auto simp: pred_def)
next
  case Fence
  then show ?thesis using assms by auto
next
  case (Guard b)
  then show ?thesis using assms bexp_to_pred_correct by auto
qed

lemma pred_upd' [simp]:
  assumes "eval_action mem \<alpha> mem'"
  shows "eval\<^sub>p mem' (h_upd h mem (writes \<alpha>)) (Pred (P;[\<alpha>]\<^sub>E)) = eval\<^sub>p mem h (Pred P)"  
proof (cases \<alpha>)
  case (Assign x e)
  hence "mem' = mem (x := eval\<^sub>e mem h (aexp_to_exp e))" 
    using assms by (simp add: aexp_to_exp_correct)
  then show ?thesis using assign_eval Assign by (auto simp: context_act_def)
next
  case Fence
  then show ?thesis using assms by (auto simp: context_act_def)
next
  case (Guard b)
  then show ?thesis using assms bexp_to_pred_correct by (auto simp: context_act_def)
qed

lemma type_upd [simp]:
  assumes "eval_action mem \<alpha> mem'"
  assumes "x \<in> tdom P \<union> writes \<alpha>"
  shows "eval\<^sub>p mem' (h_upd h mem (writes \<alpha>)) (type (P;[\<alpha>]\<^sub>E) x) = 
         eval\<^sub>p mem h (if x \<in> writes \<alpha> then Rtype P E \<alpha> else type P x)"
  using assms by (cases \<alpha>) (auto simp: \<Gamma>_map_def context_act_def)

lemma pred_env_upd [simp]:
  assumes "wfE E"
  shows "eval\<^sub>p mem (h_upd h mem UNIV) (Pred (P;[E])) = eval\<^sub>p mem h (Pred P)"
proof (auto simp: context_env_def)
  let ?P = " (RP E)" and ?h = "h_upd h mem UNIV" and ?h' = "(\<lambda>n. mem)"
  have "vars'\<^sub>p ?P \<subseteq> {(x,0)|x. True}"
    using assms(1) by (simp add: wfE_def)
  moreover have "?h 0 = mem" unfolding h_upd_def by auto
  ultimately have a: "\<forall>v \<in> vars'\<^sub>p ?P. ?h (snd v) (fst v) = ?h' (snd v) (fst v)"
    by auto
  have "eval\<^sub>p mem ?h' ?P" using assms(1) unfolding wfE_def by simp 
  thus "eval\<^sub>p mem ?h ?P" using lpred_eval_vars'_det[OF a] by auto
qed

lemma type_env_upd [simp]:
  assumes "x \<in> tdom (P;[E])"
  shows "eval\<^sub>p mem (h_upd h mem UNIV) (type (P;[E]) x) = eval\<^sub>p mem h (type P x)"
  using assms by (auto simp: context_env_def \<Gamma>_filter_def \<Gamma>_map_alt split: if_splits)

lemma pred_eval_upd:
  assumes "wfE E"
  assumes "E \<turnstile>\<^sub>a P {\<alpha>} P'"
  assumes "mem, h \<leadsto>\<^sub>\<alpha> mem',h'"
  shows "eval\<^sub>p mem' h' (Pred P') = eval\<^sub>p mem h (Pred P)" (is "?l = ?r")
proof -
  have "P' = P;[\<alpha>]\<^sub>E;[E]" using assms(2) by (elim action_has_type_elim)
  hence "?l = eval\<^sub>p mem' (h_upd h mem (writes \<alpha>)) (Pred (P;[\<alpha>]\<^sub>E))" using assms(1,3) by auto
  moreover have "eval\<^sub>p mem' (h_upd h mem (writes \<alpha>)) (Pred (P;[\<alpha>]\<^sub>E)) = ?r" using assms(3) by auto
  ultimately show ?thesis by auto
qed

lemma type_eval_upd:
  assumes "E \<turnstile>\<^sub>a P {\<alpha>} P'"
  assumes "mem, h \<leadsto>\<^sub>\<alpha> mem',h'"
  assumes "x \<in> tdom P'"
  shows "eval\<^sub>p mem' h' (type P' x) = 
          eval\<^sub>p mem h (if x \<in> writes \<alpha> then Rtype P E \<alpha> else type P x)" (is "?l = ?r")
proof -
  have x: "x \<in> tdom P \<union> writes \<alpha>" using assms(1,3) tdom_eval_upd by blast
  have p: "P' = P;[\<alpha>]\<^sub>E;[E]" using assms(1) by (elim action_has_type_elim)
  hence "?l = eval\<^sub>p mem' (h_upd h mem (writes \<alpha>)) (type (P;[\<alpha>]\<^sub>E) x)" using assms by auto
  also have "... = ?r" using assms(2) x by auto
  finally show ?thesis .
qed

subsubsection \<open> Typed Step \<close>

text \<open>
Variables should not be able to affect their own classification.
\<close>
lemma no_self_control:
  assumes "wfE E"
  shows "\<forall>y \<in> writes \<alpha>. vars\<^sub>p (\<L>\<^sub>G E y) \<inter> writes \<alpha> = {}"
  unfolding Ball_def
proof (intro allI impI)
  fix x assume a: "x \<in> writes \<alpha>"
  obtain LG where e: "\<L>\<^sub>G E x = ( LG x)"
    by (cases E; auto simp: \<L>_def \<L>\<^sub>G_def  )
  hence "x \<notin> vars\<^sub>p ( ( LG x))" using assms unfolding wfE_def by metis
  hence "x \<notin> vars\<^sub>p (\<L>\<^sub>G E x)" using e by (auto)
  thus "vars\<^sub>p (\<L>\<^sub>G E x) \<inter> writes \<alpha> = {}" using a by (cases \<alpha>; auto)
qed

text \<open>
Global security policies \<L>G and \<L>R cannot refer to primed variables, as encoded in their type.
We use this information to show entailment across any h.
\<close>
lemma \<L>\<^sub>G_vars' [simp]:
  assumes "wfE E"
  shows "vars'\<^sub>p (\<L>\<^sub>G E x) = {}"
  using assms unfolding wfE_def by auto 
  
lemma \<L>\<^sub>R_vars' [simp]:
  assumes "wfE E"
  shows "vars'\<^sub>p (\<L>\<^sub>R E x) = {}"
  using assms unfolding wfE_def by auto 

lemma \<L>_vars' [simp]:
  assumes "wfE E"
  shows "vars'\<^sub>p (\<L> E x) = {}"
  using assms unfolding \<L>_def by auto

lemma \<L>_ind_h [intro]:
  assumes "wfE E"
  shows "eval\<^sub>p mem h (\<L> E x) \<Longrightarrow> eval\<^sub>p mem h' (\<L> E x)"
  by (meson assms \<L>_vars' eval\<^sub>p.simps(5) eval\<^sub>p_no_prime)

lemma wp_eval:
  assumes "wfE E"
  assumes evl: "mem, h \<leadsto>\<^sub>\<alpha> mem',h'"
  shows "eval\<^sub>p mem h (wp (\<L> E y) \<alpha>) = eval\<^sub>p mem' h' (\<L> E y)"
proof (cases \<alpha>)
  case (Assign x e)
  have "\<forall>y. eval\<^sub>p mem h (subst\<^sub>p (\<L> E y) (aexp_to_exp e) x) = eval\<^sub>p (mem(x := ev\<^sub>A mem e)) h (\<L> E y)"
    by (auto simp: aexp_to_exp_correct intro!: subst\<^sub>p_mem_upd)
  then show ?thesis using evl Assign assms by auto
qed (insert assms, auto)

text \<open>
Variables that are not modified by an action and low in the postcondition, either locally or
globally (\<L>\<Gamma>), should be low in the precondition, either locally or globally.
This is due to the fall proof obligation.
\<close>
lemma \<L>\<^sub>\<Gamma>_unmodified:
  assumes "wfE E"
  assumes tpe: "E \<turnstile>\<^sub>a P { \<alpha> } P'"
  assumes prd: "eval\<^sub>p mem h (Pred P)"
  assumes evl: "mem, h \<leadsto>\<^sub>\<alpha> mem',h'"
  shows "\<forall>x \<in> -writes \<alpha>. eval\<^sub>p mem' h' (\<L>\<^sub>\<Gamma> P' E x) \<longrightarrow> eval\<^sub>p mem h (\<L>\<^sub>\<Gamma> P E x)"
proof (intro ballI impI, elim \<L>\<^sub>\<Gamma>_elim, goal_cases)
  case (1 x) \<comment> \<open>Low globally, so its necessary to use the fall proof obligation \<close>
  have fal: "\<forall>y \<in> infl E \<alpha>. Pred P \<turnstile> PImp (wp (\<L> E y) \<alpha>) (\<L>\<^sub>\<Gamma> P E y)" 
    using tpe entail_def by auto
  thus ?case
  proof (cases "x \<in> infl E \<alpha>")
    case True \<comment> \<open>Influenced, show must have been low due to proof obligation\<close>
    thus ?thesis using wp_eval evl fal prd 1 assms(1) by (auto simp: entail_def)
  next
    case False \<comment> \<open>Not influenced, show global classification unmodified\<close>
    hence "writes \<alpha> \<inter> vars\<^sub>p (\<L> E x) = {}" by (auto simp: infl_def)
    hence "\<forall>x \<in> vars\<^sub>p (\<L> E x). mem x = mem' x" using evl eval_const by blast
    hence "eval\<^sub>p mem h (\<L> E x)" using 1(2) assms(1) lpred_eval_vars_det by blast
    thus ?thesis by (auto simp: \<L>\<^sub>\<Gamma>_def)
  qed
next
  case (2 x) \<comment> \<open>Low locally, which is a preserved property across changes to mem and h\<close>
  hence tdom: "x \<in> tdom P" using tpe tdom_eval_upd by blast
  moreover have "eval\<^sub>p mem h (type P x)" using 2 tpe evl type_eval_upd by auto
  ultimately show ?case by (auto simp: \<L>\<^sub>\<Gamma>_def)
qed

text \<open>
As a variable cannot influence its own classification, the Wtype of an action
will not be modified between the pre and post conditions.
Moreover, they are independent of the h state.
\<close>
lemma Wtype_unmodified:
  assumes "wfE E"
  assumes evl: "mem, h \<leadsto>\<^sub>\<alpha> mem',h'"
  shows "\<forall>x \<in> writes \<alpha>. eval\<^sub>p mem' h'' (\<L>\<^sub>G E x) = eval\<^sub>p mem h (\<L>\<^sub>G E x)"
proof (intro ballI impI)
  fix x assume wrt: "x \<in> writes \<alpha>"
  hence "x \<notin> vars\<^sub>p (\<L>\<^sub>G E x)" using no_self_control assms(1) by (auto simp: context_wf_def)
  hence "\<forall>y \<in> vars\<^sub>p (\<L>\<^sub>G E x). mem y = mem' y" using evl wrt by (cases \<alpha>; auto)
  hence "eval\<^sub>p mem' h'' (\<L>\<^sub>G E x) = eval\<^sub>p mem h'' (\<L>\<^sub>G E x)" by (simp add: lpred_eval_vars_det)
  also have "... = eval\<^sub>p mem h (\<L>\<^sub>G E x)" using assms(1) by (simp add: eval\<^sub>p_no_prime)
  finally show "eval\<^sub>p mem' h'' (\<L>\<^sub>G E x) = eval\<^sub>p mem h (\<L>\<^sub>G E x)" .
qed

text \<open>
Given at least one of the variables modified by an action is classified as low
due to the guaranteed global policy, then the expression must have been low,
due to the information flow proof obligation.
\<close>
lemma \<L>\<^sub>G_Rtype_low:
  assumes "wfE E"
  assumes tpe: "E \<turnstile>\<^sub>a P { \<alpha> } P'"
  assumes prd: "eval\<^sub>p mem h (Pred P)"
  assumes evl: "mem, h \<leadsto>\<^sub>\<alpha> mem',h'"
  shows "(\<exists>x \<in> writes \<alpha>. eval\<^sub>p mem' h'' (\<L>\<^sub>G E x)) \<longrightarrow> eval\<^sub>p mem h (Rtype P E \<alpha>)"
proof 
  assume "\<exists>x\<in>writes \<alpha>. eval\<^sub>p mem' h'' (\<L>\<^sub>G E x)"
  hence "\<exists>x\<in>writes \<alpha>. eval\<^sub>p mem h (\<L>\<^sub>G E x)" using Wtype_unmodified evl assms(1) by auto
  moreover have "Pred P \<turnstile> PImp (Wtype E \<alpha>) (Rtype P E \<alpha>)" using tpe by blast
  ultimately show "eval\<^sub>p mem h (Rtype P E \<alpha>)" 
    using prd by (cases \<alpha>; auto simp: entail_def Wtype_def)
qed

text \<open>
Given at least one of the variables modified by an action is classified as low
due to the local policy, then the expression must have been low,
due to the information flow proof obligation.
\<close>
lemma \<L>\<^sub>\<Gamma>_Rtype_low:
  assumes "wfE E"
  assumes tpe: "E \<turnstile>\<^sub>a P { \<alpha> } P'"
  assumes prd: "eval\<^sub>p mem h (Pred P)"
  assumes evl: "mem, h \<leadsto>\<^sub>\<alpha> mem',h'"
  shows "(\<exists>x \<in> writes \<alpha>. eval\<^sub>p mem' h' (\<L>\<^sub>\<Gamma> P' E x)) \<longrightarrow> eval\<^sub>p mem h (Rtype P E \<alpha>)"
proof 
  assume "\<exists>x\<in>writes \<alpha>. eval\<^sub>p mem' h' (\<L>\<^sub>\<Gamma> P' E x)"
  then obtain x where wr: "eval\<^sub>p mem' h' (\<L>\<^sub>\<Gamma> P' E x)" "x \<in> writes \<alpha>" by auto
  thus "eval\<^sub>p mem h (Rtype P E \<alpha>)"
  proof (cases rule: \<L>\<^sub>\<Gamma>_elim)
    case global
    then show ?thesis using wr(2) \<L>\<^sub>G_Rtype_low[OF assms(1) tpe prd evl] 
      by (auto simp: \<L>_def)
  next
    case local
    then show ?thesis using type_eval_upd tpe evl wr(2) by auto 
  qed
qed

text \<open>
Given an action \<alpha> that has been type checked from a precondition with low equivalence and 
progress on one memory, we show the other memory can take a step and maintains 
the low equivalence property.
Additionally, we show the stronger guarantee policy is established for modified variables.
\<close>
lemma maintain_leq_bisim:
  assumes wf: "wfE E"
  assumes tpe: "E \<turnstile>\<^sub>a P { \<alpha> } P'"
  assumes ev1: "mem\<^sub>1, h\<^sub>1 \<leadsto>\<^sub>\<alpha> mem\<^sub>1',h\<^sub>1'"
  assumes prd: "both mem\<^sub>1 h\<^sub>1 mem\<^sub>2 h\<^sub>2 (Pred P)"
  assumes leq: "mem\<^sub>1, h\<^sub>1 =\<^sup>l\<^bsub>\<L>\<^sub>\<Gamma> P E\<^esub> h\<^sub>2, mem\<^sub>2"
  obtains mem\<^sub>2' h\<^sub>2' where 
    "mem\<^sub>2, h\<^sub>2 \<leadsto>\<^sub>\<alpha> mem\<^sub>2',h\<^sub>2'" "mem\<^sub>1', h\<^sub>1' =\<^sup>l\<^bsub>\<L>\<^sub>\<Gamma> P' E\<^esub> h\<^sub>2', mem\<^sub>2'" "mem\<^sub>1' =\<^sup>g\<^bsub>\<L>\<^sub>G E,writes \<alpha>\<^esub> mem\<^sub>2'"
proof -
  \<comment> \<open>First, handle the action itself and show low equivalence on its writes globally\<close>
  obtain mem\<^sub>2' h\<^sub>2' where ev2: 
    "mem\<^sub>2, h\<^sub>2 \<leadsto>\<^sub>\<alpha> mem\<^sub>2',h\<^sub>2'" "mem\<^sub>1' =\<^sup>g\<^bsub>\<L>\<^sub>G E,writes \<alpha>\<^esub> mem\<^sub>2'" "mem\<^sub>1', h\<^sub>1' =\<^sup>l\<^bsub>\<L>\<^sub>\<Gamma> P' E,writes \<alpha>\<^esub> h\<^sub>2', mem\<^sub>2'" 
  proof (cases "writes \<alpha> = {}")
    case True \<comment> \<open>For guard, show low equivalence on expression to show equivalent outcome\<close>
    hence t: "Pred P \<turnstile> Rtype P E \<alpha>" using tpe 
      by (cases \<alpha>; auto elim!: action_has_type.cases simp: Wtype_def entail_def)
    hence "eval\<^sub>p mem\<^sub>1 h\<^sub>1 (Rtype P E \<alpha>)" using prd by (auto simp: entail_def pred_def)
    hence "\<forall>x \<in> reads \<alpha>. mem\<^sub>1 x = mem\<^sub>2 x" using read_type_all_low leq by (auto simp: low_eq_def)
    moreover have "\<forall>mem\<^sub>2'. mem\<^sub>1' =\<^sup>g\<^bsub>\<L>\<^sub>G E,writes \<alpha>\<^esub> mem\<^sub>2'" by (auto simp: low_eq_def True)
    moreover have "\<forall>mem\<^sub>2' h\<^sub>2'. mem\<^sub>1', h\<^sub>1' =\<^sup>l\<^bsub>\<L>\<^sub>\<Gamma> P' E,writes \<alpha>\<^esub> h\<^sub>2', mem\<^sub>2'" by (auto simp: low_eq_def True)
    ultimately show ?thesis using eval_step ev1 that True by metis
  next
    case False \<comment> \<open>For assign, show low equivalence on expression given guarantee policy\<close>
    then obtain x e where \<alpha>: "\<alpha> = x \<leftarrow> e" by (cases \<alpha>; auto)
    then obtain mem\<^sub>2' h\<^sub>2' where ev2: "mem\<^sub>2, h\<^sub>2 \<leadsto>\<^sub>\<alpha> mem\<^sub>2',h\<^sub>2'" by auto
    \<comment> \<open>Show the global and local policy separately, although the proofs are very similar\<close>
    \<comment> \<open>TODO: There should be a composition/decomposition property for secpolicy, use this to remove duplicated proof\<close>
    moreover have "mem\<^sub>1' =\<^sup>g\<^bsub>\<L>\<^sub>G E,writes \<alpha>\<^esub> mem\<^sub>2'"
      unfolding low_eq_def 
    proof (intro ballI allI impI)
      fix h\<^sub>1'' h\<^sub>2'' x assume a: "x \<in> writes \<alpha>" "either mem\<^sub>1' h\<^sub>1'' mem\<^sub>2' h\<^sub>2'' (\<L>\<^sub>G E x)" 
      hence "either mem\<^sub>1 h\<^sub>1 mem\<^sub>2 h\<^sub>2 (Rtype P E \<alpha>)" using tpe prd ev1 ev2 \<L>\<^sub>G_Rtype_low a(1) wf by blast
      hence "\<forall>x \<in> reads \<alpha>. mem\<^sub>1 x = mem\<^sub>2 x" using leq read_type_all_low by (auto simp: low_eq_def)
      thus "mem\<^sub>1' x = mem\<^sub>2' x" using \<alpha> ev1 ev2 eval_vars_det\<^sub>A a(1) by (cases \<alpha>; auto)
    qed
    moreover have "mem\<^sub>1', h\<^sub>1' =\<^sup>l\<^bsub>\<L>\<^sub>\<Gamma> P' E,writes \<alpha>\<^esub> h\<^sub>2', mem\<^sub>2'"
      unfolding low_eq_def
    proof (intro ballI allI impI)
      fix x assume a: "x \<in> writes \<alpha>" "either mem\<^sub>1' h\<^sub>1' mem\<^sub>2' h\<^sub>2' (\<L>\<^sub>\<Gamma> P' E x)" 
      hence "either mem\<^sub>1 h\<^sub>1 mem\<^sub>2 h\<^sub>2 (Rtype P E \<alpha>)" using tpe prd \<L>\<^sub>\<Gamma>_Rtype_low ev1 ev2 a(1) wf by blast
      hence "\<forall>x \<in> reads \<alpha>. mem\<^sub>1 x = mem\<^sub>2 x" using leq read_type_all_low by (auto simp: low_eq_def)
      thus "mem\<^sub>1' x = mem\<^sub>2' x" using \<alpha> ev1 ev2 eval_vars_det\<^sub>A a(1) by (cases \<alpha>; auto)
    qed
    ultimately show ?thesis using that by simp
  qed

  \<comment> \<open>Second, show low equivalence is maintained for all unmodified variables\<close>
  moreover have "mem\<^sub>1', h\<^sub>1' =\<^sup>l\<^bsub>\<L>\<^sub>\<Gamma> P' E,(-writes \<alpha>)\<^esub> h\<^sub>2', mem\<^sub>2'" 
    unfolding low_eq_def
  proof (intro ballI allI impI)
    fix x assume a: "x \<in> - writes \<alpha>" "either mem\<^sub>1' h\<^sub>1' mem\<^sub>2' h\<^sub>2' (\<L>\<^sub>\<Gamma> P' E x)" 
    hence "either mem\<^sub>1 h\<^sub>1 mem\<^sub>2 h\<^sub>2 (\<L>\<^sub>\<Gamma> P E x)" using \<L>\<^sub>\<Gamma>_unmodified tpe prd ev1 ev2 wf by auto
    moreover have "mem\<^sub>1' x = mem\<^sub>1 x \<and> mem\<^sub>2' x = mem\<^sub>2 x" using a(1) ev1 ev2 eval_const by metis
    ultimately show "mem\<^sub>1' x = mem\<^sub>2' x" using leq by (auto simp: low_eq_def)
  qed

  ultimately have "mem\<^sub>1', h\<^sub>1' =\<^sup>l\<^bsub>\<L>\<^sub>\<Gamma> P' E\<^esub> h\<^sub>2', mem\<^sub>2'" by (auto simp: low_eq_def)
  thus ?thesis using ev2 that by blast
qed

text \<open>
Combine all properties to show the bisimulation is maintained across an action.
\<close>
lemma maintain_bisim_act:
  assumes wf: "wfE E"
  assumes tpe: "E \<turnstile>\<^sub>a P {\<alpha>} P'"
  assumes ev1: "mem\<^sub>1, h\<^sub>1 \<leadsto>\<^sub>\<alpha> mem\<^sub>1',h\<^sub>1'"
  assumes prd: "both mem\<^sub>1 h\<^sub>1 mem\<^sub>2 h\<^sub>2 (Pred P)"
  assumes cwf: "context_wf P E"
  assumes leq: "mem\<^sub>1, h\<^sub>1 =\<^sup>l\<^bsub>\<L>\<^sub>\<Gamma> P E\<^esub> h\<^sub>2, mem\<^sub>2"
  obtains mem\<^sub>2' h\<^sub>2' where 
    "mem\<^sub>2, h\<^sub>2 \<leadsto>\<^sub>\<alpha> mem\<^sub>2',h\<^sub>2'" 
    "mem\<^sub>1', h\<^sub>1' =\<^sup>l\<^bsub>\<L>\<^sub>\<Gamma> P' E\<^esub> h\<^sub>2', mem\<^sub>2'" 
    "mem\<^sub>1' =\<^sup>g\<^bsub>\<L>\<^sub>G E,writes \<alpha>\<^esub> mem\<^sub>2'"
    "both mem\<^sub>1' h\<^sub>1' mem\<^sub>2' h\<^sub>2' (Pred P')" 
    "context_wf P' E"
proof -
  obtain mem\<^sub>2' h\<^sub>2' where e:
      "mem\<^sub>2, h\<^sub>2 \<leadsto>\<^sub>\<alpha> mem\<^sub>2',h\<^sub>2'"
      "mem\<^sub>1', h\<^sub>1' =\<^sup>l\<^bsub>\<L>\<^sub>\<Gamma> P' E\<^esub> h\<^sub>2', mem\<^sub>2'"  
      "mem\<^sub>1' =\<^sup>g\<^bsub>\<L>\<^sub>G E,writes \<alpha>\<^esub> mem\<^sub>2'"
    using assms maintain_leq_bisim by metis
  moreover have "both mem\<^sub>1' h\<^sub>1' mem\<^sub>2' h\<^sub>2' (Pred P')"
    using e prd ev1 pred_eval_upd[OF wf tpe] by metis
  moreover have "context_wf P' E" using wf cwf tpe context_wf_upd by fast
  ultimately show ?thesis using that by auto
qed

text \<open>
Combine all properties to show the bisimulation is maintained across a context rewrite.
\<close>
lemma maintain_bisim_ord:
  assumes ord: "P >\<^sub>E P'"
  assumes leq: "mem\<^sub>1, h\<^sub>1 =\<^sup>l\<^bsub>\<L>\<^sub>\<Gamma> P E\<^esub> h\<^sub>2, mem\<^sub>2"
  assumes prd: "both mem\<^sub>1 h\<^sub>1 mem\<^sub>2 h\<^sub>2 (Pred P)"
  assumes cwf: "context_wf P E"
  shows "mem\<^sub>1, h\<^sub>1 =\<^sup>l\<^bsub>\<L>\<^sub>\<Gamma> P' E\<^esub> h\<^sub>2, mem\<^sub>2 \<and> both mem\<^sub>1 h\<^sub>1 mem\<^sub>2 h\<^sub>2 (Pred P') \<and> context_wf P' E"
proof -
  have "both mem\<^sub>1 h\<^sub>1 mem\<^sub>2 h\<^sub>2 (Pred P')" 
    using ord prd by (auto simp: context_ord_def entail_def)
  moreover have "context_wf P' E"
    using ord cwf by (auto simp: context_ord_def)
  moreover have "mem\<^sub>1, h\<^sub>1 =\<^sup>l\<^bsub>\<L>\<^sub>\<Gamma> P' E\<^esub> h\<^sub>2, mem\<^sub>2" 
  proof -
    have "\<forall>x. PConj (Pred P) (\<L>\<^sub>\<Gamma> P' E x) \<turnstile> (\<L>\<^sub>\<Gamma> P E x)"
      using ord by (auto simp: context_ord_def entail_def)
    hence "\<forall>x. either mem\<^sub>1 h\<^sub>1 mem\<^sub>2 h\<^sub>2 (\<L>\<^sub>\<Gamma> P' E x) \<longrightarrow> either mem\<^sub>1 h\<^sub>1 mem\<^sub>2 h\<^sub>2 (\<L>\<^sub>\<Gamma> P E x)"
      using prd by (auto simp: entail_def)
    thus ?thesis using leq unfolding low_eq_def by auto
  qed
  ultimately show ?thesis by auto
qed

lemma global_sec_intro:
  assumes wf: "wfE E"
  assumes "mem\<^sub>1, h\<^sub>1 =\<^sup>l\<^bsub>\<L>\<^sub>\<Gamma> P E\<^esub> h\<^sub>2, mem\<^sub>2"
  shows "mem\<^sub>1 =\<^sup>g\<^bsub>\<L> E\<^esub> mem\<^sub>2"
  using assms by (auto simp: low_eq_def \<L>\<^sub>\<Gamma>_def)

lemma modified_writes:
  assumes "eval_action mem \<alpha> mem'"
  shows "modified mem mem' \<subseteq> writes \<alpha>"
  using assms by (cases \<alpha>; auto simp: modified_def)

text \<open>
  This is the main component of the proof.
  We show that given two programs in the bisimulation, where one has
  a valid evaluation step and speculation, the other must also have a valid
  evaluation step, using the same Action, and speculation. Additionally, their
  resultant states should be in the bisimulation.
\<close>
lemma \<R>_typed_step':
  assumes wf: "wfE E"
  assumes "\<langle>c\<^sub>1, det\<^sub>1, mem\<^sub>1\<rangle> \<leadsto>\<^sub>\<beta> \<langle>c\<^sub>1', det\<^sub>1', mem\<^sub>1'\<rangle>"
  assumes "E \<turnstile>\<^sub>s P { c\<^sub>1 } P'"
  assumes "context_wf P E"
  assumes "mem\<^sub>1, h\<^sub>1 =\<^sup>l\<^bsub>\<L>\<^sub>\<Gamma> P E\<^esub> h\<^sub>2, mem\<^sub>2"
  assumes "both mem\<^sub>1 h\<^sub>1 mem\<^sub>2 h\<^sub>2 (Pred P)"
  shows "\<exists>c\<^sub>2' det\<^sub>2' mem\<^sub>2'. 
    (eval\<^sub>a_abv (\<langle>c\<^sub>1, det\<^sub>1, mem\<^sub>2\<rangle>) \<beta> (\<langle>c\<^sub>2', det\<^sub>2', mem\<^sub>2'\<rangle>)) \<and>
    ((mem\<^sub>1, mem\<^sub>2), mem\<^sub>1', mem\<^sub>2') \<in> (G\<^sub>L E) \<and>
    (\<langle>c\<^sub>1', det\<^sub>1', mem\<^sub>1'\<rangle> \<R>\<^bsub>P',E\<^esub> \<langle>c\<^sub>2', det\<^sub>2', mem\<^sub>2'\<rangle>)"
  using assms(2)
proof (cases rule: eval\<^sub>a.cases)
  case evala
  show ?thesis using evala(2,1) assms(3,4,5,6)
  proof (induct arbitrary: mem\<^sub>2 P P' rule: next\<^sub>a.induct)
    case (act \<alpha> c det)
    \<comment> \<open>Split type checking into components\<close>
    then obtain P\<^sub>i P\<^sub>i' where split: "P >\<^sub>E P\<^sub>i" "E \<turnstile>\<^sub>a P\<^sub>i {\<alpha>} P\<^sub>i'" "E \<turnstile>\<^sub>s P\<^sub>i' {c} P'"
      by blast

    \<comment> \<open>Show properties hold on the weaker state Pi\<close>
    hence props1:
        "mem\<^sub>1, h\<^sub>1 =\<^sup>l\<^bsub>\<L>\<^sub>\<Gamma> P\<^sub>i E\<^esub> h\<^sub>2, mem\<^sub>2"
        "context_wf P\<^sub>i E"
        "both mem\<^sub>1 h\<^sub>1 mem\<^sub>2 h\<^sub>2 (Pred P\<^sub>i)"
      using maintain_bisim_ord act  by auto

    \<comment> \<open>Show properties are maintained across \<alpha>\<close>
    then obtain mem\<^sub>2' h\<^sub>1' h\<^sub>2' where props2: 
        "eval_action mem\<^sub>2 \<alpha> mem\<^sub>2'" 
        "mem\<^sub>1', h\<^sub>1' =\<^sup>l\<^bsub>\<L>\<^sub>\<Gamma> P\<^sub>i' E\<^esub> h\<^sub>2', mem\<^sub>2'"
        "mem\<^sub>1' =\<^sup>g\<^bsub>\<L>\<^sub>G E,writes \<alpha>\<^esub> mem\<^sub>2'"
        "context_wf P\<^sub>i' E" 
        "both mem\<^sub>1' h\<^sub>1' mem\<^sub>2' h\<^sub>2' (Pred P\<^sub>i')"
      using split(2) maintain_bisim_act act(1) wf by metis

    \<comment> \<open>Show \<alpha> conforms to the guarantees\<close>
    have g1: "mem\<^sub>1' =\<^sup>g\<^bsub>\<L> E\<^esub> mem\<^sub>2'" 
      using props2(2) by (auto intro!: global_sec_intro[OF wf])
    have g2: "mem\<^sub>1' =\<^sup>g\<^bsub>\<L>\<^sub>G E,(modified mem\<^sub>1 mem\<^sub>1' \<union> modified mem\<^sub>2 mem\<^sub>2')\<^esub> mem\<^sub>2'"
      using props2(1,3) act(1) modified_writes unfolding low_eq_def by blast
    have g3: "(mem\<^sub>1,mem\<^sub>1') \<in> G E \<and> (mem\<^sub>2,mem\<^sub>2') \<in> G E" 
    proof -
      have "guar (Pred P\<^sub>i) E \<alpha>" using split(2) by (elim action_has_type_elim)
      thus ?thesis using props1(3) by (meson act.prems(1) guar_def props2(1) pred_upd)
    qed

    \<comment> \<open>Its trivial to introduce the three properties from these results\<close>
    have "\<langle>\<alpha> ;;; c, L # det, mem\<^sub>2\<rangle> \<leadsto>\<^sub>\<alpha> \<langle>c, det, mem\<^sub>2'\<rangle>" 
      using props2 by (simp add: eval\<^sub>a.intros next\<^sub>a.act)
    moreover have "\<langle>c, det, mem\<^sub>1'\<rangle> \<R>\<^bsub>P',E\<^esub> \<langle>c, det, mem\<^sub>2'\<rangle>" 
      using props2 split(3) by blast
    moreover have "((mem\<^sub>1, mem\<^sub>2), mem\<^sub>1', mem\<^sub>2') \<in> G\<^sub>L E"
      unfolding G\<^sub>L_def using g1 g2 g3 by simp
    ultimately show ?case using act(2) by metis
  next
    case (choice1 c\<^sub>1 det \<alpha> c\<^sub>1' det' c\<^sub>2)
    thus ?case using next\<^sub>a.choice1 eval\<^sub>a.intros eval\<^sub>a_elim by fastforce 
  next
    case (choice2 c\<^sub>2 det \<alpha> c\<^sub>2' det' c\<^sub>1)
    thus ?case using next\<^sub>a.choice2 eval\<^sub>a.intros eval\<^sub>a_elim by fastforce
  next
    case (loop c\<^sub>1 n c\<^sub>2 det \<beta> c det')
    thus ?case using loop_elim_unroll next\<^sub>a.loop eval\<^sub>a.intros eval\<^sub>a_elim by fastforce
  qed
qed

text \<open>
  Use the prior result to demonstrate high level evaluation, ignoring
  Action choices and speculation, also remains in the bi-simulation.
\<close>
lemma \<R>_typed_step:
  assumes wf: "wfE E"
  assumes "E \<turnstile>\<^sub>s P { c\<^sub>1 } P'"
  assumes "context_wf P E"
  assumes "mem\<^sub>1, h\<^sub>1 =\<^sup>l\<^bsub>\<L>\<^sub>\<Gamma> P E\<^esub> h\<^sub>2, mem\<^sub>2"
  assumes "both mem\<^sub>1 h\<^sub>1 mem\<^sub>2 h\<^sub>2 (Pred P)"
  assumes "\<langle>c\<^sub>1, det\<^sub>1, mem\<^sub>1\<rangle> \<leadsto> \<langle>c\<^sub>1', det\<^sub>1', mem\<^sub>1'\<rangle>"
  shows "\<exists>c\<^sub>2' det\<^sub>2' mem\<^sub>2'. 
    (\<langle>c\<^sub>1, det\<^sub>1, mem\<^sub>2\<rangle> \<leadsto> \<langle>c\<^sub>2', det\<^sub>2', mem\<^sub>2'\<rangle>) \<and>
    ((mem\<^sub>1, mem\<^sub>2), mem\<^sub>1', mem\<^sub>2') \<in> (G\<^sub>L E) \<and>
    (\<langle>c\<^sub>1', det\<^sub>1', mem\<^sub>1'\<rangle> \<R>\<^bsub>P',E\<^esub> \<langle>c\<^sub>2', det\<^sub>2', mem\<^sub>2'\<rangle>)"
  using eval_iff assms \<R>_typed_step'
  by meson

subsection \<open>Bisimulation\<close>

text\<open>
All necessary properties have been shown to establish \<R> P E
is a strong low-bisimulation.
\<close>
lemma \<R>_bisim: "wfE E \<Longrightarrow> strong_bisim (\<R>_set P' E) (R\<^sub>L E) (G\<^sub>L E)"
  unfolding strong_bisim_def
proof (intro conjI)
  show "wfE E \<Longrightarrow> bisim (\<R>_set P' E) (G\<^sub>L E)" 
    by (auto elim!: \<R>.cases intro!: \<R>_typed_step simp: bisim_def)
next
  show "stable (\<R>_set P' E) (R\<^sub>L E)" using \<R>_stable by simp
next
  show "sym (\<R>_set P' E)" using \<R>_sym by simp
qed

definition P\<^sub>0 :: "(('Local,'Global) Var,'Val) Context"
  where "P\<^sub>0 = \<lparr> Pred = PTrue, Type = \<lambda>x. None \<rparr>"

inductive type_global ::
  "(((('Local,'Global) Var, 'AExp, 'BExp) Stmt \<times> DetChoice list) \<times> (('Local,'Global) Var, 'Val) Env) list \<Rightarrow> bool"
  ("\<turnstile> _" [120] 1000)
  where
  "\<lbrakk> \<forall>((c,d),env) \<in> set cs. \<exists>P'. wfE env \<and> (env \<turnstile>\<^sub>s P\<^sub>0 { c } P');
     \<forall>((c,d),env) \<in> set cs. \<forall>x. \<L>\<^sub>R env (Local x) =\<^sub>P PFalse;
     \<forall>((c,d),env) \<in> set cs. \<forall>m x. 
      (\<exists>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>R env (Global x))) \<longrightarrow>
      (\<forall>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>R env (Global x)));
    \<forall>((c,d),env) \<in> set cs. \<forall>m x. 
      (\<exists>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>G env (Global x))) \<longrightarrow>
      (\<forall>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>G env (Global x))) \<rbrakk> \<Longrightarrow>
    type_global cs"

lemma typed_secure:
  assumes wf: "wfE E"
  assumes typed: "E \<turnstile>\<^sub>s P\<^sub>0 { c } P'"
  shows "lsec (c,d) E"
  unfolding lsec_def
proof (clarsimp)
  fix mem\<^sub>1 mem\<^sub>2 assume "\<forall>h\<^sub>1 h\<^sub>2. mem\<^sub>1,h\<^sub>1 =\<^sup>l\<^bsub>\<L> E,UNIV\<^esub> h\<^sub>2,mem\<^sub>2"
  hence "\<forall>h\<^sub>1 h\<^sub>2. mem\<^sub>1,h\<^sub>1 =\<^sup>l\<^bsub>\<L>\<^sub>\<Gamma> P\<^sub>0 E,UNIV\<^esub> h\<^sub>2,mem\<^sub>2"
    unfolding P\<^sub>0_def low_eq_def \<L>\<^sub>\<Gamma>_def 
    by auto
  moreover have "context_wf P\<^sub>0 E"
    by (auto simp: context_wf_def stable_type_def P\<^sub>0_def)
  moreover have "\<forall>h\<^sub>1 h\<^sub>2. both mem\<^sub>1 h\<^sub>1 mem\<^sub>2 h\<^sub>2 (Pred P\<^sub>0)"
    by (auto simp: P\<^sub>0_def)
  ultimately have "\<langle>c, d, mem\<^sub>1\<rangle> \<R>\<^bsub>P',E\<^esub> \<langle>c, d, mem\<^sub>2\<rangle>"
    using typed by (auto)
  moreover have "strong_bisim (\<R>_set P' E) (R\<^sub>L E) (G\<^sub>L E)"
    using \<R>_bisim wf by auto
  ultimately show "\<exists>\<B>. strong_bisim \<B> (R\<^sub>L E) (G\<^sub>L E) \<and> (\<langle>c, d, mem\<^sub>1\<rangle>, \<langle>c, d, mem\<^sub>2\<rangle>) \<in> \<B>"
    by (auto)
qed

lemma list_all_set: "\<forall>x. x \<in> set xs \<longrightarrow> P x \<Longrightarrow> list_all P xs"
  by (metis (lifting) list_all_iff)

definition While ::
  "'BExp \<Rightarrow> ('Var, 'AExp, 'BExp) Stmt \<Rightarrow> ('Var, 'AExp, 'BExp) Stmt \<Rightarrow> 
      ('var, 'val) Context \<Rightarrow> ('Var, 'AExp, 'BExp) Stmt"
  where
  "While b c1 c2 Inv = Loop ([b?] ;;; c1) ( [(bexp_neg b)?] ;;; c2 )"

text \<open>Support for do-while syntax by merging seq and While rules\<close>
definition DoWhile ::
  "('Var, 'AExp, 'BExp) Stmt \<Rightarrow> 'BExp \<Rightarrow> ('var, 'val) Context \<Rightarrow> 
      ('Var, 'AExp, 'BExp) Stmt \<Rightarrow> ('Var, 'AExp, 'BExp) Stmt"
  where
  "DoWhile c1 b Inv c2 = c1 ;; Loop ([b?] ;;; c1) 
        ( [(bexp_neg b)?] ;;; c2 )"

lemma wtype_guard:
  "\<forall>E b. Wtype E ([b?]) = PTrue"
  using Wtype_def by simp

lemma rtype_neg:
  "Rtype Inv E ([bexp_neg b?]) = Rtype Inv E ([b?])"
  unfolding Rtype_def by (simp )
  
lemma loop_type_compute:
  assumes a0: "Pred Inv \<turnstile> (Rtype Inv E ([b?]))"
  assumes a1: "guar (Pred Inv) E ([b?])"
  assumes a2: "guar (Pred Inv) E ([bexp_neg b?])"
  assumes a5: "P >\<^sub>E Inv"
  assumes a6: "P\<^sub>2 >\<^sub>E Inv"
  assumes a3: "E \<turnstile>\<^sub>s (Inv ;[[b?]]\<^sub>E;[E]) {c1} P\<^sub>2"
  assumes a4: "E \<turnstile>\<^sub>s (Inv ;[[bexp_neg b?]]\<^sub>E ;[E]) {c2} P'"
  shows "E \<turnstile>\<^sub>s P { While b c1 c2 Inv } P'" 
  unfolding While_def
proof (rule rewrite [where ?P\<^sub>1=Inv and ?P\<^sub>1' = P'])
  show "E \<turnstile>\<^sub>s Inv {Loop ([b?] ;;; c1) ([bexp_neg b?] ;;; c2)} P'"
  proof (rule loop_type)
    show "E \<turnstile>\<^sub>s Inv {[b?] ;;; c1} Inv"
    proof(rule rewrite [where ?P\<^sub>1=Inv and ?P\<^sub>1' = P\<^sub>2])
      show b1: "E \<turnstile>\<^sub>s Inv {[b?] ;;; c1} P\<^sub>2"
      proof(rule act_type)
      show c1: "E \<turnstile>\<^sub>a Inv {[b?]} (Inv;[[b?]]\<^sub>E;[E])"
        apply(rule action)
        using a0 wtype_guard apply (simp add: entail_def)
         apply(simp add: infl_def)
        apply(simp add: a1)
        done
      show c2: "E \<turnstile>\<^sub>s (Inv;[[b?]]\<^sub>E;[E]) {c1} P\<^sub>2"
        apply(simp add:a3)
        done
    qed
    show b2: "Inv >\<^sub>E Inv" by auto
    show b3: "P\<^sub>2 >\<^sub>E Inv" using a6 by auto
  qed
  show "E \<turnstile>\<^sub>s Inv {[bexp_neg b?] ;;; c2} P'"
    proof (rule act_type)
      show "E \<turnstile>\<^sub>a Inv {[bexp_neg b?]} (Inv;[[(bexp_neg b)?]]\<^sub>E;[E])"
        apply(rule action)
        using a0 wtype_guard apply (simp add: entail_def rtype_neg)
         apply(simp add: infl_def)
        apply(simp add: a2)
        done
      show "E \<turnstile>\<^sub>s (Inv;[[bexp_neg b?]]\<^sub>E;[E]) {c2} P'"
        using a4 .
    qed
  qed
  show "P >\<^sub>E Inv" using a5 by auto
  show "P' >\<^sub>E P'" by auto
qed

lemma do_loop_type_compute:
  assumes a0: "Pred Inv \<turnstile> (Rtype Inv E ([b?]))"
  assumes a1: "guar (Pred Inv) E ([b?])"
  assumes a2: "guar (Pred Inv) E ([bexp_neg b?])"
  assumes a5: "P\<^sub>2 >\<^sub>E Inv"
  assumes a6: "P\<^sub>3 >\<^sub>E Inv"
  assumes a3: "E \<turnstile>\<^sub>s P { c1 } P\<^sub>2"
  assumes a3: "E \<turnstile>\<^sub>s (Inv ;[[b?]]\<^sub>E;[E]) {c1} P\<^sub>3"
  assumes a4: "E \<turnstile>\<^sub>s (Inv ;[[bexp_neg b?]]\<^sub>E ;[E]) {c2} P'"
  shows "E \<turnstile>\<^sub>s P { DoWhile c1 b Inv c2 } P'" 
  unfolding DoWhile_def 
proof -
  have "E \<turnstile>\<^sub>s P\<^sub>2 { While b c1 c2 Inv } P'" using loop_type_compute assms by blast
  thus "E \<turnstile>\<^sub>s P {c1 ;; Loop ([b?] ;;; c1) ([bexp_neg b?] ;;; c2)} P'"
    using assms unfolding While_def by auto
qed



theorem type_soundness_global:
  assumes typeable: "\<turnstile> cs"
  assumes global: "compat\<^sub>e (map toAbstract (map snd cs))"
  shows "\<forall>m. gsec (map (\<lambda>x. (fst x,m)) cs) (LAbs (merge\<^sub>e (map toAbstract (map snd cs))))"
proof (intro allI)
  fix m
  have props: "\<forall>((c,d),env) \<in> set cs. \<exists>P'. wfE env \<and> (env \<turnstile>\<^sub>s P\<^sub>0 { c } P')"
      "\<forall>((c,d),env) \<in> set cs. \<forall>x. \<L>\<^sub>R env (Local x) =\<^sub>P PFalse"
     "\<forall>((c,d),env) \<in> set cs. \<forall>m x. 
      (\<exists>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>R env (Global x))) \<longrightarrow>
      (\<forall>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>R env (Global x)))"
    "\<forall>((c,d),env) \<in> set cs. \<forall>m x. 
      (\<exists>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>G env (Global x))) \<longrightarrow>
      (\<forall>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>G env (Global x)))"
    using typeable type_global.cases by meson+
  hence l: "\<forall>((c,d),env) \<in> set cs. lsec (c,d) env"
    using typed_secure by auto

  show "gsec (map (\<lambda>x. (fst x,m)) cs) (LAbs (merge\<^sub>e (map toAbstract (map snd cs))))"
  proof (intro security)
    show "compat\<^sub>e (map toAbstract (map snd cs))" 
      using assms by auto
  next
    show "length (map snd cs) = length (map (\<lambda>x. (fst x, m)) cs)"
      by auto
  next
    show "\<forall>i<length (map (\<lambda>x. (fst x, m)) cs). lsec (fst (map (\<lambda>x. (fst x, m)) cs ! i)) (map snd cs ! i)"
      apply auto
    proof -
      fix i assume "i < length cs"
      hence "cs ! i \<in> set cs" by auto
      thus "lsec (fst (cs ! i)) (snd (cs ! i))" using l by auto
    qed
  next
    show "\<forall>i<length (map (\<lambda>x. (fst x, m)) cs). \<forall>x. \<L>\<^sub>R (map snd cs ! i) (Local x) =\<^sub>P PFalse"
      using props(2) nth_mem by fastforce
  next
    show "\<forall>i<length (map (\<lambda>x. (fst x, m)) cs).
       \<forall>m x. (\<exists>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>R (map snd cs ! i) (Global x))) \<longrightarrow>
             (\<forall>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>R (map snd cs ! i) (Global x)))"
      using props(3) nth_mem by fastforce
  next
    show "\<forall>i<length (map (\<lambda>x. (fst x, m)) cs).
       \<forall>m x. (\<exists>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>G (map snd cs ! i) (Global x))) \<longrightarrow>
             (\<forall>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>G (map snd cs ! i) (Global x)))"
      using props(4) nth_mem by fastforce
  qed
qed

end

end