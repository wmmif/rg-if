theory read
  imports ApplicationLanguage "HOL-Eisbach.Eisbach_Tools"
begin

abbreviation stable
  where "stable y \<equiv> PCmp (=) (Var y) (Var' y 0)"

definition R\<^sub>0 :: "((reg,addr)Var,nat) lpred"
  where
   "R\<^sub>0 \<equiv> PCmp (\<ge>) z\<^sup>o z` \<and>\<^sub>p stable (Local r1) \<and>\<^sub>p stable (Local r2)"

definition G\<^sub>0 :: "((reg,addr)Var,nat) lpred"
  where
   "G\<^sub>0 \<equiv> PCmp (=) z\<^sup>o z`"

definition SR\<^sub>0 :: "((reg,addr)Var,nat) SecPolicy"
  where
   "SR\<^sub>0 \<equiv> (\<lambda>y. case y of Global x \<Rightarrow> (PCmp (=) (aexp_to_exp(Mod (Load (Global z)) (Const 2))) (aexp_to_exp(Const 0))) |
                         Global z \<Rightarrow> PTrue |                         
                         Global public \<Rightarrow> PTrue |
                         _ \<Rightarrow> PFalse)"

definition SG\<^sub>0 :: "((reg,addr)Var,nat) SecPolicy"
  where
   "SG\<^sub>0 \<equiv> (\<lambda>y. case y of Global x \<Rightarrow> (PCmp (=) (aexp_to_exp(Mod (Load (Global z)) (Const 2))) (aexp_to_exp(Const 0))) |
                         Global z \<Rightarrow> PTrue |
                         Global public \<Rightarrow> PTrue |
                         _ \<Rightarrow> PFalse)"

definition E :: "((reg,addr)Var,nat) Env"
  where "E \<equiv> Env R\<^sub>0 G\<^sub>0 SR\<^sub>0 SG\<^sub>0"

definition P\<^sub>0' :: "((reg,addr)Var, nat) Context"
  where "P\<^sub>0' \<equiv> \<lparr> Pred = (PCmp (=) z\<^sup>o (lConst 0)), Type = \<lambda>x. None \<rparr>"

definition innerInv :: "((reg,addr)Var, nat) Context"
  where
  "innerInv  \<equiv>  \<lparr> Pred = (PCmp (\<le>) (LVar r1) (z\<^sup>o)), 
                  Type = \<lambda>y. case y of 
                          (Local r1) \<Rightarrow> Some PTrue |
                          _ \<Rightarrow> None\<rparr>"

definition outerInv :: "((reg,addr)Var, nat) Context"
  where
  "outerInv  \<equiv>  \<lparr> Pred = PCmp (\<le>) (LVar r1) (z\<^sup>o) \<and>\<^sub>p (PCmp (=) (aexp_to_exp(Mod (Load (Local r1)) (Const 2))) (aexp_to_exp(Const 0))), 
                  Type = \<lambda>y. case y of 
                          (Local r1) \<Rightarrow> Some PTrue |
                          (Local r2) \<Rightarrow> Some (PCmp (=) (LVar r1) z\<^sup>o) | 
                          _ \<Rightarrow> None\<rparr>"
(* May need to include r2 mapping *)

definition
  read :: "((reg,addr)Var, (reg,addr)Var aexp, (reg,addr)Var bexp) Stmt"
where
  "read \<equiv>                
    (DoWhile
        ((DoWhile
            ((Local r1) \<leftarrow> (Load (Global z)) ;;; Stop)
            (bexp_neg (Eq (Mod (Load (Local r1)) (Const 2)) (Const 0)))
            (innerInv))
        ((Local r2) \<leftarrow> (Load (Global x)) ;;; Stop))
        (bexp_neg (Eq (Load (Global z)) (Load (Local r1))))
        (outerInv))
    ((Global public) \<leftarrow> (Load (Local r2)) ;;; Stop)"

declare entail_def [simp]
declare guar_def [simp]
declare infl_def [simp]
declare Wtype_def [simp]
declare Rtype_def [simp]

(* Break a context ordering into its three components and defer them all *)
method defer_ord = (simp only: context_ord_def, intro conjI, defer_tac, defer_tac, defer_tac)

method step =
  (match conclusion in "has_type E P (DoWhile c\<^sub>1 b Inv c\<^sub>2) Q" for E P c\<^sub>1 b Inv c\<^sub>2 Q \<Rightarrow> 
    \<open>succeed\<close>, rule do_loop_type_compute, defer_tac, defer_tac, defer_tac, defer_ord, defer_ord) |
  (match conclusion in "has_type E P (\<alpha> ;;; c) Q" for E P \<alpha> c Q \<Rightarrow> 
    \<open>succeed\<close>, rule act_type, rule action, defer_tac, defer_tac, defer_tac) |
  (match conclusion in "has_type E P Stop Q" for E P Q \<Rightarrow> 
    \<open>succeed\<close>, rule stop_type)
  
method discharge_proof_obligations =
  (simp add: \<L>\<^sub>T_def \<L>_def \<L>\<^sub>R_def \<L>\<^sub>G_def \<L>\<^sub>\<Gamma>_def 
    context_act_def context_env_def assign_def context_ord_def
    G_def GtoRel_def GP_def eval\<^sub>g_def domIff innerInv_def outerInv_def
    R_def RtoRel_def eval\<^sub>r_def RP_def
    split:Var.splits addr.splits reg.splits)

(* Same as above, but use simp_all *)
method discharge_all_proof_obligations =
  (simp_all add: \<L>\<^sub>T_def \<L>_def \<L>\<^sub>R_def \<L>\<^sub>G_def \<L>\<^sub>\<Gamma>_def 
    context_act_def context_env_def assign_def context_ord_def
    G_def GtoRel_def GP_def eval\<^sub>g_def domIff innerInv_def outerInv_def
    R_def RtoRel_def eval\<^sub>r_def RP_def
    split:Var.splits addr.splits reg.splits)

named_theorems wf_invs

method context_wf_tac =
  (insert wf_invs, auto simp only: E_def SG\<^sub>0_def SR\<^sub>0_def R\<^sub>0_def G\<^sub>0_def)[1]

method unpack_\<Gamma> =
  (auto simp add: \<Gamma>_map_def \<Gamma>_filter_def noWrite_def R_def RtoRel_def eval\<^sub>r_def RP_def split: Env.splits)[1]

lemma innerInv_wf [wf_invs]:
  "context_wf innerInv E"
  by (auto simp: innerInv_def
      \<Gamma>_map_def \<Gamma>_filter_def noWrite_def R_def RtoRel_def eval\<^sub>r_def RP_def
      stable_type_def E_def R\<^sub>0_def context_wf_def stable_class_def R\<^sub>L_def
      split: Env.splits Var.splits addr.splits reg.splits)

lemma outerInv_wf [wf_invs]:
  "context_wf outerInv E"
  by (auto simp: outerInv_def
      \<Gamma>_map_def \<Gamma>_filter_def noWrite_def R_def RtoRel_def eval\<^sub>r_def RP_def
      stable_type_def E_def R\<^sub>0_def context_wf_def stable_class_def R\<^sub>L_def
      split: Env.splits Var.splits addr.splits reg.splits)

lemma read_typed:
  "\<exists>P'. has_type E P\<^sub>0' read P'"
  unfolding read_def
    E_def P\<^sub>0'_def SG\<^sub>0_def SR\<^sub>0_def R\<^sub>0_def G\<^sub>0_def
  apply(rule exI) (* Break into Actions and Stop *)
  apply step+
                      (* Simplify up to context_ord (4th goal), otherwise breaks application of context_wf_tac *)
                      (* This should be made more robust *)
                      apply discharge_all_proof_obligations[3]

                      (* context_ord *)
                      apply context_wf_tac
                      apply discharge_proof_obligations
                      apply (discharge_proof_obligations, unpack_\<Gamma>)
                      apply presburger
                      apply presburger

                      (* context_ord *)
                      apply context_wf_tac
                      apply discharge_proof_obligations
                      apply (discharge_proof_obligations, unpack_\<Gamma>)
                      apply presburger
                      apply presburger

                      apply discharge_all_proof_obligations[3]

                      (* context_ord *)
                      apply context_wf_tac
                      apply discharge_proof_obligations
                      apply (discharge_proof_obligations, unpack_\<Gamma>)
                      
                      (* context_ord *)
                      apply context_wf_tac
                      apply discharge_proof_obligations
                      apply (discharge_proof_obligations, unpack_\<Gamma>)

                      apply discharge_all_proof_obligations[12]
                      
                      (* context_ord *)
                      apply context_wf_tac
                      apply discharge_proof_obligations
                      apply (discharge_proof_obligations, unpack_\<Gamma>)

                      (* context_ord *)
                      apply context_wf_tac
                      apply discharge_proof_obligations
                      apply (discharge_proof_obligations, unpack_\<Gamma>)

                      apply (discharge_all_proof_obligations, unpack_\<Gamma>)
  done

end