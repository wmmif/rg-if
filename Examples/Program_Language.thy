theory Program_Language
  imports "../TypeSystem"
begin

(* Arithmetic expressions *)
datatype ('Addr) aexp = 
                Load "'Addr" | 
                Const "nat" | 
                Add "('Addr) aexp" "('Addr) aexp" | 
                Mult "('Addr) aexp" "('Addr) aexp"| 
                Mod "('Addr) aexp" "('Addr) aexp"

(* Boolean expressions *)
datatype ('Addr) bexp = 
                Eq "('Addr) aexp" "('Addr) aexp" |
                Neg "('Addr) bexp" |
                Or "('Addr) bexp" "('Addr) bexp" |
                And "('Addr) bexp" "('Addr) bexp" |
                Ex "nat \<Rightarrow> ('Addr) bexp" |
                TT | FF

locale nat_lang =
   fixes addr :: "('Addr ::{linorder, finite}) list"
   fixes reg :: "('Reg ::{linorder, finite}) list"

begin

print_locale sifum_types
thm sifum_types_def
thm sifum_lang_no_dma_def

(* Instruction set of language *)
fun
  ev\<^sub>A :: "(('Reg, 'Addr) Var \<Rightarrow> nat) \<Rightarrow> ('Reg, 'Addr) Var aexp \<Rightarrow> nat"
where
  "ev\<^sub>A mem (Load v) = mem v" |
  "ev\<^sub>A mem (Const c) = c" |
  "ev\<^sub>A mem (Add a b) = (ev\<^sub>A mem a + ev\<^sub>A mem b)" |
  "ev\<^sub>A mem (Mult a b) = (ev\<^sub>A mem a * ev\<^sub>A mem b)" |
  "ev\<^sub>A mem (Mod a b) = (ev\<^sub>A mem a mod ev\<^sub>A mem b)"


fun
  aexp_vars :: "('Reg, 'Addr) Var aexp \<Rightarrow> ('Reg, 'Addr) Var set"
where
  "aexp_vars (Load v) = {v}" |
  "aexp_vars (Add a b) = aexp_vars a \<union> aexp_vars b" |
  "aexp_vars (Mult a b) = aexp_vars a \<union> aexp_vars b" | 
  "aexp_vars (Mod a b) = aexp_vars a \<union> aexp_vars b" |
  "aexp_vars _ = {}"

lemma ev\<^sub>A_det:
  "\<forall>x\<in>aexp_vars e. mem\<^sub>1 x = mem\<^sub>2 x \<Longrightarrow> ev\<^sub>A mem\<^sub>1 e = ev\<^sub>A mem\<^sub>2 e"
  by (induct e ; auto) 

primrec
  aexp_to_exp :: "('Reg, 'Addr) Var aexp \<Rightarrow> (('Reg, 'Addr) Var, nat) lexp"
where
  "aexp_to_exp (Load v) = (Var v)" |
  "aexp_to_exp (Const c) = (lexp.Const c)" |
  "aexp_to_exp (Add a b) = (LBinOp (+) ((aexp_to_exp a)) ((aexp_to_exp b)))" |
  "aexp_to_exp (Mult a b) = (LBinOp (*) ((aexp_to_exp a)) ((aexp_to_exp b)))" |
  "aexp_to_exp (Mod a b) = (LBinOp (mod) ((aexp_to_exp a)) ((aexp_to_exp b)))"

fun
  aexp_replace :: "('Reg, 'Addr) Var aexp \<Rightarrow> ('Reg, 'Addr) Var \<Rightarrow> ('Reg, 'Addr) Var aexp \<Rightarrow> ('Reg, 'Addr) Var aexp"
where
  "aexp_replace (Load y) x c = (if (x = y) then c else (Load y))" |
  "aexp_replace (Const v) a c = Const v" |
  "aexp_replace (Add v v') a c = Add (aexp_replace v a c) (aexp_replace v' a c)" |
  "aexp_replace (Mult v v') a c = Mult (aexp_replace v a c) (aexp_replace v' a c)" |
  "aexp_replace (Mod v v') a c = Mod (aexp_replace v a c) (aexp_replace v' a c)"

text \<open> ki: in the first lemma  I replaced function lexp_vars with the new function \<close> 
lemma aexp_to_exp_vars' [simp]:
  "vars\<^sub>e ((aexp_to_exp e)) = aexp_vars e" 
  by (induct e, auto)

lemma aexp_to_exp_correct' [simp]:
  "eval\<^sub>e mem h (aexp_to_exp e) = ev\<^sub>A mem e"
  by (induct e, auto)

lemma aexp_replace_nop':
  "x \<notin> aexp_vars e \<Longrightarrow> aexp_replace e x f = e"
  by (induct e; simp)

primrec
  bexp_replace :: "('Reg, 'Addr) Var bexp \<Rightarrow> ('Reg, 'Addr) Var \<Rightarrow> ('Reg, 'Addr) Var aexp \<Rightarrow> ('Reg, 'Addr) Var bexp"
where
  "bexp_replace (Eq a\<^sub>1 a\<^sub>2) z c = Eq (aexp_replace a\<^sub>1 z c) (aexp_replace a\<^sub>2 z c)" |
  "bexp_replace (Neg a\<^sub>1) z c = Neg (bexp_replace a\<^sub>1 z c)" |
  "bexp_replace (Or b\<^sub>1 b\<^sub>2) z c = Or (bexp_replace b\<^sub>1 z c) (bexp_replace b\<^sub>2 z c)" |
  "bexp_replace (And b\<^sub>1 b\<^sub>2) z c = And (bexp_replace b\<^sub>1 z c) (bexp_replace b\<^sub>2 z c)" |
  "bexp_replace (bexp.Ex v) z c = bexp.Ex (\<lambda>x. (bexp_replace (v x) z c))" |
  "bexp_replace TT b c = TT" |
  "bexp_replace FF b c = FF"

primrec
  bexp_to_pred :: "(('Reg, 'Addr) Var) bexp \<Rightarrow> (('Reg, 'Addr) Var,nat) lpred"
where
  "bexp_to_pred (Eq a\<^sub>1 a\<^sub>2) = (PCmp (=) ((aexp_to_exp a\<^sub>1)) ((aexp_to_exp a\<^sub>2)))" |
  "bexp_to_pred (Neg a\<^sub>1) = (PNeg ((bexp_to_pred a\<^sub>1)))" |
  "bexp_to_pred (Or a b) = (PDisj ((bexp_to_pred a)) ((bexp_to_pred b)))" |
  "bexp_to_pred (And a b) = (PConj ((bexp_to_pred a)) ((bexp_to_pred b)))" |
  "bexp_to_pred (bexp.Ex v) = (PEx (\<lambda>x. ((bexp_to_pred (v x)))))" |
  "bexp_to_pred TT =  PTrue" |
  "bexp_to_pred FF =  PFalse"

primrec
  ev\<^sub>B :: "(('Reg, 'Addr) Var \<Rightarrow> nat) \<Rightarrow> (('Reg, 'Addr) Var) bexp \<Rightarrow> bool"
where
  "ev\<^sub>B mem (Eq a\<^sub>1 a\<^sub>2) = (ev\<^sub>A mem a\<^sub>1 = ev\<^sub>A mem a\<^sub>2)" |
  "ev\<^sub>B mem (Neg a\<^sub>1) = (\<not> (ev\<^sub>B mem a\<^sub>1))" |
  "ev\<^sub>B mem (Or x y) = (ev\<^sub>B mem x \<or> ev\<^sub>B mem y)" |
  "ev\<^sub>B mem (And x y) = (ev\<^sub>B mem x \<and> ev\<^sub>B mem y)" |
  "ev\<^sub>B mem (Ex fe) = (\<exists>v. ev\<^sub>B mem (fe v))" | 
  "ev\<^sub>B mem TT = True" |
  "ev\<^sub>B mem FF = False"

primrec
  bexp_vars :: "(('Reg, 'Addr) Var) bexp \<Rightarrow> ('Reg, 'Addr) Var set"
where
  "bexp_vars (Neg x) = bexp_vars x" |
  "bexp_vars (Eq x c) = aexp_vars x \<union> aexp_vars c"  |
  "bexp_vars (Or x y) = bexp_vars x \<union> bexp_vars y" |
  "bexp_vars (And x y) = bexp_vars x \<union> bexp_vars y" |
  "bexp_vars (Ex fe) = (\<Union>v. bexp_vars (fe v))" |
  "bexp_vars TT = {}" |
  "bexp_vars FF = {}"

lemma ev\<^sub>B_det:
  "\<forall> x\<in> bexp_vars e. mem\<^sub>1 x = mem\<^sub>2 x \<Longrightarrow> ev\<^sub>B mem\<^sub>1 e = ev\<^sub>B mem\<^sub>2 e"
proof (induct e)
  case (Eq x1 x2a)
  then show ?case by (metis (no_types, lifting) Un_iff bexp_vars.simps(2) ev\<^sub>A_det ev\<^sub>B.simps(1)) 
qed (auto)

lemma bexp_to_pred_vars' [simp]:
  "vars\<^sub>p (bexp_to_pred e) = bexp_vars e"
  by (induct e, auto)

lemma bexp_to_pred_correct' [simp]:
  "eval\<^sub>p mem h (bexp_to_pred e) = ev\<^sub>B mem e"
  by (induct e, auto)

lemma bexp_replace_nop':
  "x \<notin> bexp_vars b \<longrightarrow> bexp_replace b x f = b"
  by (induct b; simp add: aexp_replace_nop')

fun bexp_neg :: "(('Local, 'Global) Var) bexp \<Rightarrow> (('Local, 'Global) Var) bexp"
  where "bexp_neg b = Neg b"

lemma bexp_neg_negates':
  "ev\<^sub>B mem (bexp_neg e) = (\<not> ev\<^sub>B mem e)"
  by auto

lemma bexp_neg_vars':
  "bexp_vars (bexp_neg e) = bexp_vars e"
  by auto

end

declare Collect_conj_eq [simp]
declare Collect_disj_eq [simp]
declare pred_def [simp]
declare restrict_map_def [simp]

sublocale nat_lang \<subseteq> sifum_types ev\<^sub>A ev\<^sub>B aexp_to_exp bexp_to_pred bexp_neg
  by (unfold_locales, auto)

end