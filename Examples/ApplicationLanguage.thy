theory ApplicationLanguage
  imports "Program_Language"
begin

abbreviation lConst
  where "lConst n \<equiv> lexp.Const n" 

abbreviation aConst
  where "aConst n \<equiv> aexp.Const n"

abbreviation GVar ("_\<^sup>o" [1000] 1000)
  where "GVar y \<equiv> Var (Global y)"

abbreviation PVar ("_`" [1000] 1000)
  where "PVar v \<equiv> Var' (Global v) 0"

abbreviation LVar
  where "LVar y \<equiv> Var (Local y)"

datatype addr = x | z | secret | public
datatype reg = r1 | r2

type_synonym mem = "((reg,addr)Var, nat) Mem"

definition Addrs :: "addr list"
  where "Addrs \<equiv> [secret, public, x, z]"

definition Regs :: "reg list"
  where "Regs \<equiv> [r1, r2]"

lemma UNIV_addr:
  "set Addrs = UNIV"
  apply(clarsimp | safe)+ 
  by (metis (full_types) Addrs_def addr.exhaust list.set_intros(1) list.set_intros(2)) 


lemma UNIV_reg:
  "set Regs = UNIV"
  apply(clarsimp | safe)+ 
  by (metis (full_types) Regs_def reg.exhaust list.set_intros(1) list.set_intros(2)) 

lemma finite_Vars_addr:
  "finite {x::addr. True}"
  by (metis List.finite_set UNIV_addr UNIV_def)

lemma finite_Vars_reg:
  "finite {x::reg. True}"
  by (metis List.finite_set UNIV_reg UNIV_def)

instance addr :: finite
  apply standard
  using UNIV_addr by (metis List.finite_set)

instance reg :: finite
  apply standard
  using UNIV_reg by (metis List.finite_set)

primrec 
  index_of :: "'a \<Rightarrow> 'a list \<Rightarrow> nat option"
where
  "index_of a [] = None" |
  "index_of a (y#ys) = (if a = y then Some 0 else map_option Suc (index_of a ys))"

lemma Addr_complete:
  "\<exists>i. index_of a Addrs = Some i"
  apply(case_tac a, (auto simp: Addrs_def))
  done

lemma Reg_complete:
  "\<exists>i. index_of a Regs = Some i"
  apply(case_tac a, (auto simp: Regs_def))
  done

lemma index_of_inject:
  "index_of a xs = Some i \<Longrightarrow> index_of b xs = Some i \<Longrightarrow> a = b"
  apply(induct xs arbitrary: i, auto split: if_splits)
  done

instantiation addr :: linorder
begin

definition less_eq_addr :: "addr \<Rightarrow> addr \<Rightarrow> bool"
where
  "less_eq_addr a b \<equiv> a = b \<or> the (index_of a Addrs) < the (index_of b Addrs)"

definition less_addr :: "addr \<Rightarrow> addr \<Rightarrow> bool"
where
  "less_addr a b \<equiv> the (index_of a Addrs) < the (index_of b Addrs)"

instance 
  apply(intro_classes)
      apply(clarsimp simp: less_addr_def less_eq_addr_def | safe)+
  using Addr_complete index_of_inject
  by (metis option.sel)
end

instantiation reg :: linorder
begin

definition less_eq_reg :: "reg \<Rightarrow> reg \<Rightarrow> bool"
where
  "less_eq_reg a b \<equiv> a = b \<or> the (index_of a Regs) < the (index_of b Regs)"

definition less_reg :: "reg \<Rightarrow> reg \<Rightarrow> bool"
where
  "less_reg a b \<equiv> the (index_of a Regs) < the (index_of b Regs)"

instance 
  apply(intro_classes)
      apply(clarsimp simp: less_reg_def less_eq_reg_def | safe)+
  using Reg_complete index_of_inject
  by (metis option.sel)
end

interpretation nat_lang Addrs Regs
  by (unfold_locales)

end
