theory secret_write
  imports ApplicationLanguage "HOL-Eisbach.Eisbach_Tools" 
begin

definition R\<^sub>0 :: "nat \<Rightarrow> ((reg,addr)Var,nat) lpred"
  where
   "R\<^sub>0 \<equiv> (\<lambda>n. PCmp(=)(Var' (Global z) (n+1))(Var' (Global z) n))"

definition G\<^sub>0 :: "nat \<Rightarrow> ((reg,addr)Var,nat) lpred"
  where
   "G\<^sub>0 \<equiv> (\<lambda>n. PCmp(\<ge>)(Var' (Global z) (n+1))(Var' (Global z) n))"

definition SR\<^sub>0 :: "((reg,addr)Var,nat) SecPolicy"
  where
   "SR\<^sub>0 \<equiv> \<lambda>n. PTrue"

definition SG\<^sub>0 :: "((reg,addr)Var,nat) SecPolicy"
  where
    "SG\<^sub>0 \<equiv> (\<lambda>y. case y of Global x \<Rightarrow> PEx (\<lambda>n. PCmp (=) (aexp_to_exp(Const (2 * n))) (GVar z)) |
                          Global z \<Rightarrow> PTrue |
                          _ \<Rightarrow> PFalse)"

definition E :: "((reg,addr)Var,nat) Env"        
 where
  "E \<equiv> Env (R\<^sub>0 0) (G\<^sub>0 0) SR\<^sub>0 (SG\<^sub>0)" 

definition P\<^sub>0' :: "((reg,addr)Var, nat) Context"
  where
  "P\<^sub>0'  \<equiv>  \<lparr> Pred = (PCmp (=) (GVar z) (lConst 0)), Type = \<lambda>x. None \<rparr>"

definition
  secret_write :: "((reg,addr)Var, (reg,addr)Var aexp, (reg,addr)Var bexp) Stmt"
where
  "secret_write \<equiv>                
    (Global z) \<leftarrow> (Add (Load (Global z)) (Const 1)) ;;;   
    Fence ;;;
    (Global x) \<leftarrow> (Load (Global secret)) ;;;
    (Global x) \<leftarrow> (Const 0) ;;;
    Fence ;;;
    (Global z) \<leftarrow> (Add (Load (Global z)) (Const 1)) ;;;   
    Stop"

declare entail_def [simp]
declare guar_def [simp]
declare infl_def [simp]
declare Wtype_def [simp]
declare Rtype_def [simp]

method step =
  (match conclusion in "has_type E P (\<alpha> ;;; c) Q" for E P \<alpha> c Q \<Rightarrow> \<open>succeed\<close>, rule act_type, rule action, defer_tac, defer_tac, defer_tac) |
  (match conclusion in "has_type E P Stop Q" for E P Q \<Rightarrow> \<open>succeed\<close>, rule stop_type)

method discharge_proof_obligations =
  (simp add: \<L>\<^sub>T_def \<L>_def \<L>\<^sub>R_def \<L>\<^sub>G_def \<L>\<^sub>\<Gamma>_def 
    context_act_def context_env_def RP_def assign_def 
    G_def GtoRel_def GP_def eval\<^sub>g_def 
    split:Var.splits addr.splits; linarith)

lemma secret_write_typed:
  "\<exists>P'. has_type E P\<^sub>0' secret_write P'"
  unfolding secret_write_def E_def P\<^sub>0'_def SG\<^sub>0_def SR\<^sub>0_def R\<^sub>0_def G\<^sub>0_def
  apply (rule exI)
  apply step+
                   apply discharge_proof_obligations+
  done

end
