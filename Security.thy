theory Security
  imports Preliminaries
begin

text \<open>Security policy type with restriction to prevent self references\<close>

text \<open>defines the type of any Security policy \<L>_i: x \<mapsto> pred, and its wellformedness condition; 
      the typedef comes with a predicate Rep_SecPolicy to access the predicate to which a variable 
      is mapped\<close>

type_synonym ('var, 'val) SecPolicy = " 'var \<Rightarrow> ('var, 'val) lpred"

datatype ('var,'val) Env = 
  Env "('var,'val) lpred" "('var,'val) lpred" "('var,'val) SecPolicy" "('var,'val) SecPolicy"

context sifum_security begin

subsection \<open>Evaluation of Global Program\<close>

abbreviation eval_abv :: "('Com, 'Local, 'Global, 'Val) Conf \<Rightarrow> (_, _, _, _) Conf \<Rightarrow> bool"
  (infixl "\<leadsto>" 70)
where
  "x \<leadsto> y \<equiv> (x, y) \<in> eval"

definition conf :: "('Com, 'Local, 'Val) LocalConf \<Rightarrow> ('Global, 'Val) Mem \<Rightarrow> ('Com,'Local,'Global,'Val) Conf"
  ("\<langle>_, _\<rangle>" [100, 100] 100)
where
  "\<langle> c, mem \<rangle> \<equiv> (fst c, merge (snd c) mem)"

lemma det:
  assumes "\<langle> c, mem \<rangle> \<leadsto> \<langle> c', mem' \<rangle>"
  assumes "\<langle> c, mem \<rangle> \<leadsto> \<langle> c'', mem'' \<rangle>"
  shows "c' = c'' \<and> mem' = mem''"
proof -
  have "merge (snd c') mem' = merge (snd c'') mem'' \<longrightarrow> (\<forall>x. (snd c') x = (snd c'') x) \<and> (\<forall>x. mem' x = mem'' x)"
    by (metis merge_def Var.simps(5,6))
  hence "merge (snd c') mem' = merge (snd c'') mem'' \<longrightarrow> snd c' = snd c'' \<and> mem' = mem''" by blast
  moreover have "\<langle> c', mem' \<rangle> = \<langle> c'', mem'' \<rangle>"
    using assms deterministic by auto
  ultimately show ?thesis by (cases c'; cases c''; auto simp: conf_def)
qed

inductive_set geval :: "(('Com,'Local,'Global,'Val) GlobalConf \<times> nat \<times> (_,_,_,_) GlobalConf) set"
  and geval_abv :: "('Com,'Local,'Global,'Val) GlobalConf \<Rightarrow> nat \<Rightarrow> (_,_,_,_) GlobalConf \<Rightarrow> bool" 
  ("_ \<leadsto>\<^bsub>_\<^esub> _" 70)
where
  "con \<leadsto>\<^bsub>k\<^esub> con' \<equiv> (con, k, con') \<in> geval" |
  intro: "\<lbrakk> \<langle> cms ! n, mem \<rangle> \<leadsto> \<langle> cm', mem' \<rangle>; n < length cms \<rbrakk> \<Longrightarrow>
  ((cms, mem), n, (cms [n := cm'], mem')) \<in> geval"

inductive_cases geval_elim [elim!]: "((cms, mem), k, (cms', mem')) \<in> geval"

lemma geval_det:
  "gc \<leadsto>\<^bsub>n\<^esub> gc' \<Longrightarrow> gc \<leadsto>\<^bsub>n\<^esub> gc'' \<Longrightarrow> gc' = gc''"
  using det by (cases gc; cases gc'; cases gc''; blast)

fun geval_sch :: "('Com,'Local,'Global,'Val) GlobalConf \<Rightarrow> nat list \<Rightarrow> (_,_,_,_) GlobalConf \<Rightarrow> bool"
  ("_ \<rightarrow>\<^bsub>_\<^esub> _" 70)
  where
    "geval_sch c [] c' = (c = c')" |
    "geval_sch c (n#ns) c' = (\<exists> c''.  (c \<leadsto>\<^bsub>n\<^esub> c'') \<and> geval_sch c'' ns c')"

lemma geval_sch_det:
  "geval_sch c ns c' \<Longrightarrow> geval_sch c ns c'' \<Longrightarrow> c' = c''"
proof (induct arbitrary: c rule: geval_sch.induct)
  case (1 c c')
  then show ?case by auto
next
  case (2 d n ns c')
  then obtain a b where e: 
      "(c \<leadsto>\<^bsub>n\<^esub> a) \<and> (a \<rightarrow>\<^bsub>ns\<^esub> c')" "(c \<leadsto>\<^bsub>n\<^esub> b) \<and> (b \<rightarrow>\<^bsub>ns\<^esub> d)"
    by auto
  then show ?case using 2 geval_det by blast
qed

subsection \<open>Rely/Guarantee Local Bisimulation\<close>

type_synonym ('var, 'val) PairMemRel = "(('var,'val) Mem \<times> ('var,'val) Mem) rel"
type_synonym ('var, 'val) PairMem = "(('var,'val) Mem \<times> ('var,'val) Mem) set"

text \<open>Bisimulation property with guarantee restrictions\<close>
definition bisim\<^sub>L :: "('Com,'Local,'Global,'Val) Conf rel \<Rightarrow> (_,_) PairMemRel \<Rightarrow> bool"
  where
    "bisim\<^sub>L \<B> G \<equiv>
      \<forall>c\<^sub>1 mem\<^sub>1 c\<^sub>2 mem\<^sub>2 c\<^sub>1' mem\<^sub>1'.
        (\<langle> c\<^sub>1, mem\<^sub>1 \<rangle>, \<langle> c\<^sub>2, mem\<^sub>2 \<rangle>) \<in> \<B> \<longrightarrow>
        \<langle> c\<^sub>1, mem\<^sub>1 \<rangle> \<leadsto> \<langle> c\<^sub>1', mem\<^sub>1' \<rangle> \<longrightarrow>
        (\<exists>c\<^sub>2' mem\<^sub>2'. 
          \<langle> c\<^sub>2, mem\<^sub>2 \<rangle> \<leadsto> \<langle> c\<^sub>2', mem\<^sub>2' \<rangle> \<and>
          ((mem\<^sub>1,mem\<^sub>2),(mem\<^sub>1',mem\<^sub>2')) \<in> G \<and>
          (\<langle> c\<^sub>1', mem\<^sub>1' \<rangle>, \<langle> c\<^sub>2', mem\<^sub>2' \<rangle>) \<in> \<B>)"

text \<open>Stability property for a local bisimulation\<close>
definition stable\<^sub>L :: "('Com,'Local,'Global,'Val) Conf rel \<Rightarrow> (_,_) PairMemRel \<Rightarrow> bool"
  where
    "stable\<^sub>L \<B> R \<equiv>
      \<forall>c\<^sub>1 mem\<^sub>1 c\<^sub>2 mem\<^sub>2 mem\<^sub>1' mem\<^sub>2'. 
        (\<langle> c\<^sub>1, mem\<^sub>1 \<rangle>, \<langle> c\<^sub>2, mem\<^sub>2 \<rangle>) \<in> \<B> \<longrightarrow>
        ((mem\<^sub>1,mem\<^sub>2),(mem\<^sub>1',mem\<^sub>2')) \<in> R \<longrightarrow>
        (\<langle> c\<^sub>1, mem\<^sub>1' \<rangle>, \<langle> c\<^sub>2, mem\<^sub>2' \<rangle>) \<in> \<B>"

text \<open>Merge for a strong local bisimulation\<close>
definition strong_bisim\<^sub>L :: "('Com,'Local,'Global,'Val) Conf rel \<Rightarrow> (_,_) PairMemRel \<Rightarrow> (_,_) PairMemRel \<Rightarrow> bool"
  where
    "strong_bisim\<^sub>L \<B> R G \<equiv> bisim\<^sub>L \<B> G \<and> stable\<^sub>L \<B> R \<and> sym \<B>"

text \<open>Introduction rule for strong local bisimulation\<close>
definition sb_intro\<^sub>L :: "('Com,'Local,'Val) LocalConf \<Rightarrow> ('Global,_) PairMem \<Rightarrow> (_,_) PairMemRel \<Rightarrow> (_,_) PairMemRel \<Rightarrow> bool"
  where
    "sb_intro\<^sub>L c P R G \<equiv> 
      \<forall>mem\<^sub>1 mem\<^sub>2. (mem\<^sub>1, mem\<^sub>2) \<in> P \<longrightarrow> (\<exists>\<B>. strong_bisim\<^sub>L \<B> R G \<and> (\<langle> c, mem\<^sub>1 \<rangle>, \<langle> c, mem\<^sub>2 \<rangle>) \<in> \<B>)"

subsection \<open>Rely/Guarantee Global Bisimulation\<close>

text \<open>Bisimulation property with guarantee restrictions\<close>
definition bisim\<^sub>G :: "('Com,'Local,'Global,'Val) GlobalConf rel \<Rightarrow> (_,_) PairMemRel \<Rightarrow> bool"
  where
    "bisim\<^sub>G \<B> G \<equiv>
      \<forall>n c\<^sub>1 mem\<^sub>1 c\<^sub>2 mem\<^sub>2 c\<^sub>1' mem\<^sub>1'.
        ((c\<^sub>1, mem\<^sub>1), (c\<^sub>2, mem\<^sub>2)) \<in> \<B> \<longrightarrow>
        ((c\<^sub>1, mem\<^sub>1) \<leadsto>\<^bsub>n\<^esub> (c\<^sub>1', mem\<^sub>1')) \<longrightarrow>
        (\<exists>c\<^sub>2' mem\<^sub>2'. 
          (( c\<^sub>2, mem\<^sub>2 ) \<leadsto>\<^bsub>n\<^esub> ( c\<^sub>2', mem\<^sub>2' )) \<and>
          ((mem\<^sub>1,mem\<^sub>2),(mem\<^sub>1',mem\<^sub>2')) \<in> G \<and>
          (( c\<^sub>1', mem\<^sub>1' ), ( c\<^sub>2', mem\<^sub>2' )) \<in> \<B>)"

text \<open>Stability property for a global bisimulation\<close>
definition stable\<^sub>G :: "('Com,'Local,'Global,'Val) GlobalConf rel \<Rightarrow> (_,_) PairMemRel \<Rightarrow> bool"
  where
    "stable\<^sub>G \<B> R \<equiv>
      \<forall>c\<^sub>1 mem\<^sub>1 c\<^sub>2 mem\<^sub>2 mem\<^sub>1' mem\<^sub>2'. 
        (( c\<^sub>1, mem\<^sub>1 ), ( c\<^sub>2, mem\<^sub>2 )) \<in> \<B> \<longrightarrow>
        ((mem\<^sub>1,mem\<^sub>2), (mem\<^sub>1',mem\<^sub>2')) \<in> R \<longrightarrow>
        (( c\<^sub>1, mem\<^sub>1' ), ( c\<^sub>2, mem\<^sub>2')) \<in> \<B>"

text \<open>Merge for a strong global bisimulation\<close>
definition strong_bisim\<^sub>G :: "('Com,'Local,'Global,'Val) GlobalConf rel \<Rightarrow> (_,_) PairMemRel \<Rightarrow> (_,_) PairMemRel \<Rightarrow> bool"
  where
    "strong_bisim\<^sub>G \<B> R G \<equiv> bisim\<^sub>G \<B> G \<and> stable\<^sub>G \<B> R \<and> sym \<B>"

text \<open>Introduce strong global bisimulation under initial conditions\<close>
definition sb_intro\<^sub>G :: "('Com,'Local,'Val) LocalConf list \<Rightarrow> ('Global,_) PairMem \<Rightarrow> (_,_) PairMemRel \<Rightarrow> (_,_) PairMemRel \<Rightarrow> bool"
  where
    "sb_intro\<^sub>G c P R G \<equiv> 
      \<forall>mem\<^sub>1 mem\<^sub>2. (mem\<^sub>1, mem\<^sub>2) \<in> P \<longrightarrow> (\<exists>\<B>. strong_bisim\<^sub>G \<B> R G \<and> ((c, mem\<^sub>1), (c, mem\<^sub>2)) \<in> \<B>)"

text \<open>Compatible rely/guarantee list\<close>
definition compat :: "('Global,'Val) PairMemRel list \<Rightarrow> (_,_) PairMemRel list \<Rightarrow> bool"
  where
    "compat Rs Gs \<equiv> \<forall>i < length Rs. (\<forall>j < length Gs. j \<noteq> i \<longrightarrow> (Rs ! i) \<supseteq> (Gs ! j))"

text \<open>Merge a list of relies\<close>
definition merge\<^sub>R :: "('Global,'Val) PairMemRel list \<Rightarrow> (_,_) PairMemRel"
  where
    "merge\<^sub>R Rs \<equiv> {x. \<forall>y \<in> set Rs. x \<in> y}"

text \<open>Merge a list of guarantees\<close>
definition merge\<^sub>G :: "('Global,'Val) PairMemRel list \<Rightarrow> (_,_) PairMemRel"
  where
    "merge\<^sub>G Rs \<equiv> {x. \<exists>y \<in> set Rs. x \<in> y}"

text \<open>Given compatible R/G conditions and local bisimulations, establish a global bisimulation\<close>
lemma parallel_bisim:
  assumes "compat Rs Gs"
  assumes "\<forall>i < length c. sb_intro\<^sub>L (c ! i) P (Rs ! i) (Gs ! i)"
  assumes "length c = length Gs" and "length c = length Rs"
  shows "sb_intro\<^sub>G c P (merge\<^sub>R Rs) (merge\<^sub>G Gs)"
  unfolding sb_intro\<^sub>G_def
proof (clarsimp)
  fix mem\<^sub>1 mem\<^sub>2 
  let ?L = "length Gs"

  \<comment> \<open>Obtain a list of local bisimulations\<close>
  assume a: "(mem\<^sub>1, mem\<^sub>2) \<in> P"
  hence "\<forall>i < length c. \<exists>\<B>. bisim\<^sub>L \<B> (Gs ! i) \<and> stable\<^sub>L \<B> (Rs ! i) \<and> sym \<B> \<and> (\<langle>c ! i, mem\<^sub>1\<rangle>, \<langle>c ! i, mem\<^sub>2\<rangle>) \<in> \<B>"
    using assms(2) by (auto simp: sb_intro\<^sub>L_def strong_bisim\<^sub>L_def)
  then obtain \<B>s where \<B>s:
    "\<forall>i < ?L. bisim\<^sub>L (\<B>s i) (Gs ! i) \<and> stable\<^sub>L (\<B>s i) (Rs ! i) \<and> sym (\<B>s i) \<and> (\<langle>c ! i, mem\<^sub>1\<rangle>, \<langle>c ! i, mem\<^sub>2\<rangle>) \<in> (\<B>s i)"
    using assms(3) by (metis)
  let ?B = "{ ((c,m),(c',m')). length c = length c' \<and> length c = ?L \<and> (\<forall>i < ?L. (\<langle>c ! i,m\<rangle>,\<langle>c' ! i,m'\<rangle>) \<in> \<B>s i) }"

  \<comment> \<open>First show a global bisim property\<close>
  have "bisim\<^sub>G ?B (merge\<^sub>G Gs)"
    unfolding bisim\<^sub>G_def 
  proof (intro allI impI)
    fix n c\<^sub>1 mem\<^sub>1 c\<^sub>2 mem\<^sub>2 mem\<^sub>1' c\<^sub>1'
    assume asm: "((c\<^sub>1, mem\<^sub>1), (c\<^sub>2, mem\<^sub>2)) \<in> ?B" "(c\<^sub>1, mem\<^sub>1) \<leadsto>\<^bsub>n\<^esub> (c\<^sub>1', mem\<^sub>1')"
    hence props: 
        "n < length c\<^sub>1" "length c\<^sub>1' = length c\<^sub>1" "length c\<^sub>2 = length c\<^sub>1" "?L = length c\<^sub>1"
        "\<langle>c\<^sub>1 ! n, mem\<^sub>1\<rangle> \<leadsto> \<langle>c\<^sub>1' ! n, mem\<^sub>1'\<rangle>"  "c\<^sub>1[n := c\<^sub>1' ! n] = c\<^sub>1'"
        "bisim\<^sub>L (\<B>s n) (Gs ! n)" 
        "\<forall>i<?L. (\<langle>c\<^sub>1 ! i, mem\<^sub>1\<rangle>, \<langle>c\<^sub>2 ! i, mem\<^sub>2\<rangle>) \<in> \<B>s i"
      using \<B>s by auto

    \<comment> \<open>Use the local bisimulation property to show execution on state 2\<close>
    obtain c\<^sub>2' mem\<^sub>2' where local\<^sub>2:
        "\<langle> c\<^sub>2 ! n, mem\<^sub>2 \<rangle> \<leadsto> \<langle> c\<^sub>2', mem\<^sub>2' \<rangle>"
        "((mem\<^sub>1,mem\<^sub>2),(mem\<^sub>1',mem\<^sub>2')) \<in> (Gs ! n)"
        "(\<langle>c\<^sub>1' ! n, mem\<^sub>1'\<rangle>, \<langle> c\<^sub>2', mem\<^sub>2' \<rangle>) \<in> \<B>s n"
      using props unfolding bisim\<^sub>L_def by metis

    \<comment> \<open>Preserve others through the stability property\<close>
    have other: "\<forall>i < ?L. i \<noteq> n \<longrightarrow> (\<langle>c\<^sub>1[n := c\<^sub>1' ! n] ! i, mem\<^sub>1'\<rangle>, \<langle>c\<^sub>2[n := c\<^sub>2'] ! i, mem\<^sub>2'\<rangle>) \<in> \<B>s i"
    proof (clarsimp)
      fix i assume asm: "i \<noteq> n" "i < ?L"
      hence "(\<langle>c\<^sub>1 ! i, mem\<^sub>1\<rangle>, \<langle>c\<^sub>2 ! i, mem\<^sub>2\<rangle>) \<in> \<B>s i" using props by auto
      moreover have "stable\<^sub>L (\<B>s i) (Rs ! i)" using \<B>s asm by auto
      moreover have "((mem\<^sub>1, mem\<^sub>2), mem\<^sub>1', mem\<^sub>2') \<in> Rs ! i" 
        using asm assms(1,3,4) local\<^sub>2(2) props by (auto simp: compat_def)
      ultimately show "(\<langle>c\<^sub>1 ! i, mem\<^sub>1'\<rangle>, \<langle>c\<^sub>2 ! i, mem\<^sub>2'\<rangle>) \<in> \<B>s i"
        unfolding stable\<^sub>L_def by fast
    qed

    \<comment> \<open>Move this to a global evaluation\<close>
    have merge: "((mem\<^sub>1, mem\<^sub>2), mem\<^sub>1', mem\<^sub>2') \<in> merge\<^sub>G Gs"
      using local\<^sub>2(2) nth_mem props by (auto simp: merge\<^sub>G_def)
    moreover have eval: "((c\<^sub>2, mem\<^sub>2) \<leadsto>\<^bsub>n\<^esub> (c\<^sub>2[n := c\<^sub>2'], mem\<^sub>2'))"
      using intro[OF local\<^sub>2(1)] props by auto
    moreover have "((c\<^sub>1', mem\<^sub>1'), c\<^sub>2[n := c\<^sub>2'], mem\<^sub>2') \<in> ?B"
    proof (clarsimp, intro conjI allI impI)
      show "length c\<^sub>1' = length c\<^sub>2" using props by auto
    next
      show "length c\<^sub>1' = length Gs" using props by auto
    next
      fix i assume asm: "i < length Gs"
      show "(\<langle>c\<^sub>1' ! i, mem\<^sub>1'\<rangle>, \<langle>c\<^sub>2[n := c\<^sub>2'] ! i, mem\<^sub>2'\<rangle>) \<in> \<B>s i"
      proof (cases "i = n")
        case True
        thus ?thesis using local\<^sub>2 props by auto
      next
        case False
        thus ?thesis using other asm props by auto
      qed
    qed

    ultimately show "\<exists>c\<^sub>2' mem\<^sub>2'.
          ((c\<^sub>2, mem\<^sub>2) \<leadsto>\<^bsub>n\<^esub> (c\<^sub>2', mem\<^sub>2')) \<and>
          ((mem\<^sub>1, mem\<^sub>2), mem\<^sub>1', mem\<^sub>2') \<in> merge\<^sub>G Gs \<and> 
          ((c\<^sub>1', mem\<^sub>1'), c\<^sub>2', mem\<^sub>2') \<in> ?B"
      by meson 
  qed

  \<comment> \<open>Show a global stability property\<close>
  moreover have "stable\<^sub>G ?B (merge\<^sub>R Rs)"
    unfolding stable\<^sub>G_def
  proof (clarsimp)
    fix c\<^sub>1 mem\<^sub>1 c\<^sub>2 mem\<^sub>2 mem\<^sub>1' mem\<^sub>2' i
    assume asm: 
      "\<forall>i<length c\<^sub>2. (\<langle>c\<^sub>1 ! i, mem\<^sub>1\<rangle>, \<langle>c\<^sub>2 ! i, mem\<^sub>2\<rangle>) \<in> \<B>s i" "i < length c\<^sub>2" 
      "length Gs = length c\<^sub>2" "((mem\<^sub>1, mem\<^sub>2), mem\<^sub>1', mem\<^sub>2') \<in> merge\<^sub>R Rs"
    moreover have "stable\<^sub>L (\<B>s i) (Rs ! i)" using \<B>s asm by auto
    moreover have "((mem\<^sub>1, mem\<^sub>2), mem\<^sub>1', mem\<^sub>2') \<in> Rs ! i"
      using assms(3,4) \<B>s asm by (auto simp: merge\<^sub>R_def)
    ultimately show "(\<langle>c\<^sub>1 ! i, mem\<^sub>1'\<rangle>,\<langle>c\<^sub>2 ! i, mem\<^sub>2'\<rangle>) \<in> \<B>s i" 
      unfolding stable\<^sub>L_def by fast 
  qed

  \<comment> \<open>Show symmetry of global bisimulation\<close>
  moreover have "sym ?B"
    unfolding sym_def
  proof (clarsimp)
    fix c\<^sub>1 mem\<^sub>1 c\<^sub>2 mem\<^sub>2 i
    assume asm: "\<forall>i<length c\<^sub>2. (\<langle>c\<^sub>1 ! i, mem\<^sub>1\<rangle>, \<langle>c\<^sub>2 ! i, mem\<^sub>2\<rangle>) \<in> \<B>s i" "i < length c\<^sub>2" "length Gs = length c\<^sub>2"
    hence props: "(\<langle>c\<^sub>1 ! i, mem\<^sub>1\<rangle>, \<langle>c\<^sub>2 ! i, mem\<^sub>2\<rangle>) \<in> \<B>s i" "sym (\<B>s i)" using \<B>s by auto
    thus "(\<langle>c\<^sub>2 ! i, mem\<^sub>2\<rangle>, \<langle>c\<^sub>1 ! i, mem\<^sub>1\<rangle>) \<in> \<B>s i" unfolding sym_def by blast
  qed

  \<comment> \<open>Compose these properties to show a strong global bisimulation\<close>
  moreover have "((c, mem\<^sub>1), c, mem\<^sub>2) \<in> ?B" using \<B>s assms by auto
  ultimately show "\<exists>\<B>. strong_bisim\<^sub>G \<B> (merge\<^sub>R Rs) (merge\<^sub>G Gs) \<and> ((c, mem\<^sub>1), c, mem\<^sub>2) \<in> \<B>" 
    by (auto simp: strong_bisim\<^sub>G_def)
qed

subsection \<open> Dependent SIFUM-Security \<close>

type_synonym ('var, 'val) SPAbs = "'var \<Rightarrow> ('var, 'val) Mem set" 

definition low_eq_abs :: "('var,'val) Mem \<Rightarrow> (_,_) SPAbs \<Rightarrow> _ set \<Rightarrow> (_,_) Mem \<Rightarrow> bool"
  ("_ =\<^bsub>_,_\<^esub> _" [100, 100, 100] 100)
  where "low_eq_abs m\<^sub>1 P V m\<^sub>2 \<equiv>  \<forall>x \<in> V. m\<^sub>1 \<in> P x \<or> m\<^sub>2 \<in> P x \<longrightarrow> m\<^sub>1 x = m\<^sub>2 x"

abbreviation low_eq_abs_all :: "('var,'val) Mem \<Rightarrow> (_,_) SPAbs \<Rightarrow> (_,_) Mem \<Rightarrow> bool"
  ("_ =\<^bsub>_\<^esub> _" [100, 100, 100] 100)
  where "low_eq_abs_all m\<^sub>1 P m\<^sub>2 \<equiv> low_eq_abs m\<^sub>1 P UNIV m\<^sub>2"

datatype ('var,'val) EnvAbs = 
  EnvAbs "('var,'val) Mem rel" "('var,'val) Mem rel" "('var, 'val) SPAbs" "('var, 'val) SPAbs"

definition modified :: "('var,'val) Mem \<Rightarrow> (_,_) Mem \<Rightarrow> _ set"
  where "modified m m' \<equiv> {x. m x \<noteq> m' x}"

definition RAbs :: "('var,'val) EnvAbs \<Rightarrow> (_,_) Mem rel"
  where "RAbs env \<equiv> case env of EnvAbs R _ _ _ \<Rightarrow> R"

definition GAbs :: "('var,'val) EnvAbs \<Rightarrow> (_,_) Mem rel"
  where "GAbs env \<equiv> case env of EnvAbs _ G _ _ \<Rightarrow> G"

definition LRAbs :: "('var,'val) EnvAbs \<Rightarrow> (_,_) SPAbs"
  where "LRAbs env \<equiv> case env of EnvAbs _ _ R _ \<Rightarrow> R"

definition LGAbs :: "('var,'val) EnvAbs \<Rightarrow> (_,_) SPAbs"
  where "LGAbs env \<equiv> case env of EnvAbs _ _ _ G \<Rightarrow> G"

definition LAbs :: "('var,'val) EnvAbs \<Rightarrow> (_,_) SPAbs"
  where "LAbs env \<equiv> \<lambda>x. (LRAbs env x) \<inter> (LGAbs env x)"

definition R\<^sub>pAbs :: "('Var,'Val) EnvAbs \<Rightarrow> (('Var,'Val) Mem \<times> ('Var,'Val) Mem) rel"
  where "R\<^sub>pAbs E \<equiv> { ((m\<^sub>1,m\<^sub>2),(m\<^sub>1',m\<^sub>2')). 
    (m\<^sub>1,m\<^sub>1') \<in> RAbs E \<and> 
    (m\<^sub>2,m\<^sub>2') \<in> RAbs E \<and>
    (m\<^sub>1' =\<^bsub>LRAbs E,(modified m\<^sub>1 m\<^sub>1' \<union> modified m\<^sub>2 m\<^sub>2')\<^esub> m\<^sub>2') \<and>
    (m\<^sub>1' =\<^bsub>LAbs E\<^esub> m\<^sub>2') }"

definition G\<^sub>pAbs :: "('Var,'Val) EnvAbs \<Rightarrow> (('Var,'Val) Mem \<times> ('Var,'Val) Mem) rel"
  where "G\<^sub>pAbs E \<equiv> { ((m\<^sub>1,m\<^sub>2),(m\<^sub>1',m\<^sub>2')). 
    (m\<^sub>1,m\<^sub>1') \<in> GAbs E \<and> 
    (m\<^sub>2,m\<^sub>2') \<in> GAbs E \<and>
    (m\<^sub>1' =\<^bsub>LGAbs E,(modified m\<^sub>1 m\<^sub>1' \<union> modified m\<^sub>2 m\<^sub>2')\<^esub> m\<^sub>2') \<and>
    (m\<^sub>1' =\<^bsub>LAbs E\<^esub> m\<^sub>2') }"





text \<open>Security policy without the restriction\<close>
type_synonym ('var,'val) SP = "'var \<Rightarrow> ('var,'val) lpred"

text \<open>Enforce security policies\<close>
definition low_eq :: "('var,'val) Mem \<Rightarrow> (_,_) Mem \<Rightarrow> (_,_) SP \<Rightarrow> _ set \<Rightarrow> (_,_) Mem \<Rightarrow> (_,_) Mem \<Rightarrow> bool"
  ("_,_ =\<^sup>l\<^bsub>_,_\<^esub> _,_" [100, 100, 100, 100, 100] 100)
  where "low_eq m\<^sub>1 h\<^sub>1 P V h\<^sub>2 m\<^sub>2 \<equiv> \<forall>x \<in> V. eval\<^sub>p m\<^sub>1 h\<^sub>1 (P x) \<or> eval\<^sub>p m\<^sub>2 h\<^sub>2 (P x) \<longrightarrow> m\<^sub>1 x = m\<^sub>2 x"

abbreviation low_eq1 :: "('var,'val) Mem \<Rightarrow> (_,_) Mem \<Rightarrow> (_,_) SP \<Rightarrow> (_,_) Mem \<Rightarrow> (_,_) Mem \<Rightarrow> bool"
  ("_,_ =\<^sup>l\<^bsub>_\<^esub> _,_" [100, 100, 100, 100, 100] 100)
  where "low_eq1 m\<^sub>1 h\<^sub>1 P h\<^sub>2 m\<^sub>2 \<equiv> low_eq m\<^sub>1 h\<^sub>1 P UNIV h\<^sub>2 m\<^sub>2"

abbreviation low_eq2 :: "('var,'val) Mem \<Rightarrow> (_,_) SP \<Rightarrow> _ set \<Rightarrow> (_,_) Mem \<Rightarrow> bool"
  ("_ =\<^sup>g\<^bsub>_,_\<^esub> _" [100, 100, 100] 100)
  where "low_eq2 m\<^sub>1 P V m\<^sub>2 \<equiv> \<forall>h\<^sub>1 h\<^sub>2. low_eq m\<^sub>1 h\<^sub>1 P V h\<^sub>2 m\<^sub>2"

abbreviation low_eq3 :: "('var,'val) Mem \<Rightarrow> (_,_) SP \<Rightarrow> (_,_) Mem \<Rightarrow> bool"
  ("_ =\<^sup>g\<^bsub>_\<^esub> _" [100, 100, 100] 100)
  where "low_eq3 m\<^sub>1 P m\<^sub>2 \<equiv> low_eq2 m\<^sub>1 P UNIV m\<^sub>2"

text \<open>Merge typical rely/guarantee with security policies for a component's full annotations\<close>

definition RtoRel :: "('var,'val) lpred \<Rightarrow> (_,_) Mem rel"
  where "RtoRel RG \<equiv> {(mem, mem')|mem mem'. eval\<^sub>r mem mem' RG}"

definition GtoRel :: "('var,'val) lpred \<Rightarrow> (_,_) Mem rel"
  where "GtoRel RG \<equiv> {(mem, mem')|mem mem'. eval\<^sub>g mem mem' RG}"

definition \<L>\<^sub>R :: "('var,'val) Env \<Rightarrow> 'var \<Rightarrow> ('var,'val) lpred"
  where "\<L>\<^sub>R E \<equiv> \<lambda>x. case E of Env R _ LR _ \<Rightarrow> ( LR x)"

definition \<L>\<^sub>G :: "('var,'val) Env \<Rightarrow> 'var \<Rightarrow> ('var,'val) lpred"
  where "\<L>\<^sub>G E \<equiv> \<lambda>x. case E of Env R _ _ LG \<Rightarrow> ( LG x)"

definition \<L> :: "('var,'val) Env \<Rightarrow> (_,_) SP"
  where "\<L> P \<equiv> \<lambda>x. PConj (\<L>\<^sub>R P x) (\<L>\<^sub>G P x)"

definition RP :: "('var,'val) Env \<Rightarrow> (_,_) lpred"
  where "RP env \<equiv> case env of Env r g _ _ \<Rightarrow> r"

definition GP :: "('var,'val) Env \<Rightarrow> (_,_) lpred"
  where "GP env \<equiv> case env of Env r g _ _ \<Rightarrow> g"

definition R :: "('var,'val) Env \<Rightarrow> (_,_) Mem rel"
  where "R env \<equiv> RtoRel (RP env)"

definition G :: "('var,'val) Env \<Rightarrow> (_,_) Mem rel"
  where "G env \<equiv> GtoRel (GP env)"

definition R\<^sub>p :: "('Var,'Val) Env \<Rightarrow> (('Var,'Val) Mem \<times> ('Var,'Val) Mem) rel"
  where "R\<^sub>p E \<equiv> { ((m\<^sub>1,m\<^sub>2),(m\<^sub>1',m\<^sub>2')). 
    (m\<^sub>1,m\<^sub>1') \<in> R E \<and> 
    (m\<^sub>2,m\<^sub>2') \<in> R E \<and>
    (m\<^sub>1' =\<^sup>g\<^bsub>\<L>\<^sub>R E,(modified m\<^sub>1 m\<^sub>1' \<union> modified m\<^sub>2 m\<^sub>2')\<^esub> m\<^sub>2') \<and>
    (m\<^sub>1' =\<^sup>g\<^bsub>\<L> E\<^esub> m\<^sub>2') }"

definition G\<^sub>p :: "('Var,'Val) Env \<Rightarrow> (('Var,'Val) Mem \<times> ('Var,'Val) Mem) rel"
  where "G\<^sub>p E \<equiv> { ((m\<^sub>1,m\<^sub>2),(m\<^sub>1',m\<^sub>2')). 
    (m\<^sub>1,m\<^sub>1') \<in> G E \<and> 
    (m\<^sub>2,m\<^sub>2') \<in> G E \<and>
    (m\<^sub>1' =\<^sup>g\<^bsub>\<L>\<^sub>G E,(modified m\<^sub>1 m\<^sub>1' \<union> modified m\<^sub>2 m\<^sub>2')\<^esub> m\<^sub>2') \<and>
    (m\<^sub>1' =\<^sup>g\<^bsub>\<L> E\<^esub> m\<^sub>2') }"


text \<open>First show a suitable global bisimulation enforces the security policy\<close>
lemma global_gsec_step:
  assumes gua: "bisim\<^sub>G \<B> (G\<^sub>pAbs E)"
  assumes bsm: "((c\<^sub>1, mem\<^sub>1), (c\<^sub>2, mem\<^sub>2)) \<in> \<B>"
  assumes leq: "mem\<^sub>1 =\<^bsub>LAbs E\<^esub> mem\<^sub>2"
  assumes evl: "(c\<^sub>1, mem\<^sub>1) \<rightarrow>\<^bsub>t\<^esub> (c\<^sub>1', mem\<^sub>1')"
  shows "\<exists>c\<^sub>2' mem\<^sub>2'. ((c\<^sub>2, mem\<^sub>2) \<rightarrow>\<^bsub>t\<^esub> (c\<^sub>2', mem\<^sub>2')) \<and> mem\<^sub>1' =\<^bsub>LAbs E\<^esub> mem\<^sub>2'"
  using evl bsm leq
proof (induct "(c\<^sub>1, mem\<^sub>1)" t "(c\<^sub>1', mem\<^sub>1')" arbitrary: c\<^sub>1 c\<^sub>2 mem\<^sub>1 mem\<^sub>2 rule: geval_sch.induct)
  case 1
  then show ?case by auto
next
  case (2 n ns)
  obtain c\<^sub>i mem\<^sub>i where evl\<^sub>i: "(c\<^sub>1, mem\<^sub>1) \<leadsto>\<^bsub>n\<^esub> (c\<^sub>i, mem\<^sub>i)" "(c\<^sub>i, mem\<^sub>i) \<rightarrow>\<^bsub>ns\<^esub> (c\<^sub>1', mem\<^sub>1')"
    using 2(2) by (metis geval_sch.simps(2) surj_pair)
  obtain c\<^sub>j mem\<^sub>j where evl\<^sub>j: "(c\<^sub>2, mem\<^sub>2) \<leadsto>\<^bsub>n\<^esub> (c\<^sub>j, mem\<^sub>j)" "((mem\<^sub>1, mem\<^sub>2), mem\<^sub>i, mem\<^sub>j) \<in> (G\<^sub>pAbs E)" "((c\<^sub>i, mem\<^sub>i), c\<^sub>j, mem\<^sub>j) \<in> \<B>"
    using 2(3) assms(1) evl\<^sub>i(1) unfolding bisim\<^sub>G_def by metis
  have "mem\<^sub>i =\<^bsub>LAbs E\<^esub> mem\<^sub>j" using evl\<^sub>j(2) by (auto simp: low_eq_abs_def G\<^sub>pAbs_def)
  hence "\<exists>c\<^sub>2' mem\<^sub>2'. ((c\<^sub>j, mem\<^sub>j) \<rightarrow>\<^bsub>ns\<^esub> (c\<^sub>2', mem\<^sub>2')) \<and> mem\<^sub>1' =\<^bsub>LAbs E\<^esub> mem\<^sub>2'"
    using 2(1)[OF evl\<^sub>i(2) evl\<^sub>j(3)] by auto
  then show ?case using evl\<^sub>j(1) by (meson geval_sch.simps)
qed

definition compat\<^sub>e :: "('Global,'Val) EnvAbs list \<Rightarrow> bool"
  where "compat\<^sub>e Es \<equiv> \<forall>i < length Es. (\<forall>j < length Es. j \<noteq> i \<longrightarrow>
    (RAbs (Es ! i)) \<supseteq> (GAbs (Es ! j)) \<and> 
    (\<forall>x. (LAbs (Es ! i) x) = (LAbs (Es ! j) x)) \<and>
    (\<forall>x. (LRAbs (Es ! i) x) \<subseteq> (LGAbs (Es ! j) x)))"

definition merge\<^sub>e :: "('Global,'Val) EnvAbs list \<Rightarrow> ('Global,'Val) EnvAbs"
  where "merge\<^sub>e Es \<equiv> EnvAbs
    ({x. \<forall>y \<in> set Es. x \<in> RAbs y})
    ({x. \<exists>y \<in> set Es. x \<in> GAbs y})
    (\<lambda>v. {x. \<exists>y \<in> set Es. x \<in> LRAbs y v})
    (\<lambda>v. {x. \<forall>y \<in> set Es. x \<in> LGAbs y v})"

lemma stable_rewrite:
  assumes "stable\<^sub>G \<B> R'"
  assumes "R'' \<subseteq> R'"
  shows "stable\<^sub>G \<B> R''"
  using assms unfolding stable\<^sub>G_def
  by auto

lemma bisim_rewrite:
  assumes "bisim\<^sub>G \<B> R'"
  assumes "R'' \<supseteq> R'"
  shows "bisim\<^sub>G \<B> R''"
  using assms unfolding bisim\<^sub>G_def
  by (meson subset_eq)

lemma strong_bisim_rewrite:
  assumes "strong_bisim\<^sub>G \<B> R' G'"
  assumes "R' \<supseteq> R''"
  assumes "G'' \<supseteq> G'"
  shows "strong_bisim\<^sub>G \<B> R'' G''"
  using assms stable_rewrite bisim_rewrite 
  unfolding strong_bisim\<^sub>G_def by auto

text \<open>Proofs after this point fall apart quickly. They have been somewhat rushed.\<close>

text \<open>Next, show a series of local bisimiulations can be composed given compatible annotations\<close>
lemma lsec:
  assumes "compat\<^sub>e Es"
  assumes "length Es = length c"
  assumes "\<forall>i < length c. sb_intro\<^sub>L (c ! i) P (R\<^sub>pAbs (Es ! i)) (G\<^sub>pAbs (Es ! i))"
  shows "sb_intro\<^sub>G c P (R\<^sub>pAbs (merge\<^sub>e Es)) (G\<^sub>pAbs (merge\<^sub>e Es))"
proof -
  let ?Rs = "map R\<^sub>pAbs Es" and ?Gs = "map G\<^sub>pAbs Es"

  have "sb_intro\<^sub>G c P (merge\<^sub>R ?Rs) (merge\<^sub>G ?Gs)"
  proof (intro parallel_bisim)
    show "compat (map R\<^sub>pAbs Es) (map G\<^sub>pAbs Es)"
      using assms(1) unfolding compat_def compat\<^sub>e_def G\<^sub>pAbs_def R\<^sub>pAbs_def
      apply auto
         apply blast
        apply blast
      unfolding low_eq_abs_def by blast+
  next
    show "\<forall>i<length c. sb_intro\<^sub>L (c ! i) P (map R\<^sub>pAbs Es ! i) (map G\<^sub>pAbs Es ! i)" 
      using assms by simp
  next
    show "length c = length (map G\<^sub>pAbs Es)" using assms by simp
  next
    show "length c = length (map R\<^sub>pAbs Es)" using assms by simp
  qed

  moreover have "merge\<^sub>R (map R\<^sub>pAbs Es) \<supseteq> R\<^sub>pAbs (merge\<^sub>e Es)"
    unfolding merge\<^sub>R_def
    unfolding merge\<^sub>e_def
    unfolding R\<^sub>pAbs_def
    unfolding LAbs_def LGAbs_def LRAbs_def RAbs_def low_eq_abs_def
    apply auto 
  proof (goal_cases)
    case (1 a b aa ba x xa) 
    have "\<forall>y\<in>set Es. aa \<in> ((case y of EnvAbs x xa xb G' \<Rightarrow> G') xa)"
      using 1(1,6,7) assms(1) unfolding compat\<^sub>e_def LGAbs_def LRAbs_def
      by (smt in_set_conv_nth subsetD)
    then show ?case using 1 by blast
  next
    case (2 a b aa ba x xa)
    have "\<forall>y\<in>set Es. ba \<in> ((case y of EnvAbs x xa xb G' \<Rightarrow> G') xa)"
      using 2(1,6,7) assms(1) unfolding compat\<^sub>e_def LGAbs_def LRAbs_def
      by (smt in_set_conv_nth subsetD)
    then show ?case using 2 by blast
  qed

  moreover have "merge\<^sub>G (map G\<^sub>pAbs Es) \<subseteq> G\<^sub>pAbs (merge\<^sub>e Es)"
    unfolding merge\<^sub>G_def
    unfolding merge\<^sub>e_def
    unfolding G\<^sub>pAbs_def
    unfolding LAbs_def LGAbs_def LRAbs_def GAbs_def low_eq_abs_def
    apply auto 
  proof (goal_cases)
    case (1 a b aa ba x xa y)
    have "aa \<in> (case x of EnvAbs x xa R' xb \<Rightarrow> R') xa"
      using assms(1) 1(1,6,7,8) unfolding compat\<^sub>e_def LGAbs_def LRAbs_def LAbs_def
      by (smt Int_iff in_set_conv_nth)
    then show ?case using 1 by blast 
  next
    case (2 a b aa ba x xa y)
    hence "ba \<in> (case x of EnvAbs x xa R' xb \<Rightarrow> R') xa"
    using assms(1) 2(1,6,7,8) unfolding compat\<^sub>e_def LGAbs_def LRAbs_def LAbs_def
      by (smt Int_iff in_set_conv_nth)
    then show ?case using 2 by blast 
  qed

  ultimately show ?thesis using strong_bisim_rewrite
    unfolding sb_intro\<^sub>G_def by meson
qed


definition R\<^sub>L :: "(('Local,'Global) Var,'Val) Env \<Rightarrow> ((('Local,'Global) Var,'Val) Mem \<times> (('Local,'Global) Var,'Val) Mem) rel"
  where "R\<^sub>L E \<equiv> { ((m\<^sub>1,m\<^sub>2),(m\<^sub>1',m\<^sub>2')). 
    (m\<^sub>1,m\<^sub>1') \<in> R E \<and> 
    (m\<^sub>2,m\<^sub>2') \<in> R E \<and>
    (m\<^sub>1' =\<^sup>g\<^bsub>\<L>\<^sub>R E,(modified m\<^sub>1 m\<^sub>1' \<union> modified m\<^sub>2 m\<^sub>2')\<^esub> m\<^sub>2') \<and>
    (m\<^sub>1' =\<^sup>g\<^bsub>\<L> E\<^esub> m\<^sub>2') }"

definition G\<^sub>L :: "(('Local,'Global) Var,'Val) Env \<Rightarrow> ((('Local,'Global) Var,'Val) Mem \<times> (('Local,'Global) Var,'Val) Mem) rel"
  where "G\<^sub>L E \<equiv> { ((m\<^sub>1,m\<^sub>2),(m\<^sub>1',m\<^sub>2')). 
    (m\<^sub>1,m\<^sub>1') \<in> G E \<and> 
    (m\<^sub>2,m\<^sub>2') \<in> G E \<and>
    (m\<^sub>1' =\<^sup>g\<^bsub>\<L>\<^sub>G E,(modified m\<^sub>1 m\<^sub>1' \<union> modified m\<^sub>2 m\<^sub>2')\<^esub> m\<^sub>2') \<and>
    (m\<^sub>1' =\<^sup>g\<^bsub>\<L> E\<^esub> m\<^sub>2') }"

definition stable :: "('Com,'Local,'Global,'Val) Conf rel \<Rightarrow> (('Local,'Global) Var,'Val) PairMemRel \<Rightarrow> bool"
  where
    "stable \<B> C \<equiv>
      \<forall>c\<^sub>1 mem\<^sub>1 c\<^sub>2 mem\<^sub>2 mem\<^sub>1' mem\<^sub>2'. 
        ((c\<^sub>1, mem\<^sub>1), (c\<^sub>2, mem\<^sub>2 )) \<in> \<B> \<longrightarrow>
        ((mem\<^sub>1,mem\<^sub>2),(mem\<^sub>1',mem\<^sub>2')) \<in> C \<longrightarrow>
        (( c\<^sub>1, mem\<^sub>1' ), ( c\<^sub>2, mem\<^sub>2' )) \<in> \<B>"

definition bisim :: "('Com,'Local,'Global,'Val) Conf rel \<Rightarrow> (('Local,'Global) Var,'Val) PairMemRel \<Rightarrow> bool"
  where
    "bisim \<B> C \<equiv>
      \<forall>c\<^sub>1 mem\<^sub>1 c\<^sub>2 mem\<^sub>2 c\<^sub>1' mem\<^sub>1'.
        (( c\<^sub>1, mem\<^sub>1 ), ( c\<^sub>2, mem\<^sub>2 )) \<in> \<B> \<longrightarrow>
        ( c\<^sub>1, mem\<^sub>1 ) \<leadsto> ( c\<^sub>1', mem\<^sub>1' ) \<longrightarrow>
        (\<exists>c\<^sub>2' mem\<^sub>2'. 
          ( c\<^sub>2, mem\<^sub>2 ) \<leadsto> ( c\<^sub>2', mem\<^sub>2' ) \<and>
          ((mem\<^sub>1,mem\<^sub>2),(mem\<^sub>1',mem\<^sub>2')) \<in> C \<and>
          (( c\<^sub>1', mem\<^sub>1' ), ( c\<^sub>2', mem\<^sub>2' )) \<in> \<B>)"

definition strong_bisim :: "('Com,'Local,'Global,'Val) Conf rel \<Rightarrow> (_,_) PairMemRel \<Rightarrow> (_,_) PairMemRel \<Rightarrow> bool"
  where
    "strong_bisim \<B> C C' \<equiv> bisim \<B> C' \<and> stable \<B> C \<and> sym \<B>"

definition lsec :: "'Com \<Rightarrow> (('Local,'Global) Var,'Val) Env \<Rightarrow> bool"
  where
    "lsec c E \<equiv> 
      \<forall>mem\<^sub>1 mem\<^sub>2. mem\<^sub>1 =\<^sup>g\<^bsub>\<L> E\<^esub> mem\<^sub>2 \<longrightarrow>
        (\<exists>\<B>. strong_bisim \<B> (R\<^sub>L E) (G\<^sub>L E) \<and> (( c, mem\<^sub>1 ), ( c, mem\<^sub>2 )) \<in> \<B>)"

text \<open>The global security property preserves a security policy across any schedule\<close>
definition gsec :: "('Com,'Local,'Val) LocalConf list \<Rightarrow> (_,_) SPAbs \<Rightarrow> bool"
  where "gsec c SP \<equiv>
     (\<forall>mem\<^sub>1 mem\<^sub>2. mem\<^sub>1 =\<^bsub>SP\<^esub> mem\<^sub>2 \<longrightarrow>
      (\<forall>t c\<^sub>1' mem\<^sub>1'. ((c, mem\<^sub>1) \<rightarrow>\<^bsub>t\<^esub> (c\<^sub>1', mem\<^sub>1')) \<longrightarrow>
        (\<exists>c\<^sub>2' mem\<^sub>2'. ((c, mem\<^sub>2) \<rightarrow>\<^bsub>t\<^esub> (c\<^sub>2', mem\<^sub>2')) \<and> 
          mem\<^sub>1' =\<^bsub>SP\<^esub> mem\<^sub>2')))"

lemma global_gsec:
  assumes "sb_intro\<^sub>G c {(m1,m2). m1 =\<^bsub>LAbs E\<^esub> m2} Q (G\<^sub>pAbs E)"
  shows "gsec c (LAbs E)"
  unfolding gsec_def
proof (clarsimp)
  fix mem\<^sub>1 mem\<^sub>2 t c\<^sub>1' mem\<^sub>1'
  assume asm: "mem\<^sub>1 =\<^bsub>LAbs E\<^esub> mem\<^sub>2" "(c, mem\<^sub>1) \<rightarrow>\<^bsub>t\<^esub> (c\<^sub>1', mem\<^sub>1')"
  then obtain \<B> where \<B>: "bisim\<^sub>G \<B> (G\<^sub>pAbs E)" "((c, mem\<^sub>1), (c, mem\<^sub>2)) \<in> \<B>"
    using assms unfolding sb_intro\<^sub>G_def strong_bisim\<^sub>G_def by blast
  thus "\<exists>c\<^sub>2' mem\<^sub>2'. ((c, mem\<^sub>2) \<rightarrow>\<^bsub>t\<^esub> (c\<^sub>2', mem\<^sub>2')) \<and> mem\<^sub>1' =\<^bsub>LAbs E\<^esub> mem\<^sub>2'"
    using asm by (auto intro!: global_gsec_step)
qed

definition toAbstract :: "(('Local, 'Global) Var, 'Val) Env \<Rightarrow> ('Global, 'Val) EnvAbs"
  where
    "toAbstract E = EnvAbs 
      {(m, m'). \<forall>ml. eval\<^sub>r (merge ml m) (merge ml m') (RP E)} 
      {(m, m'). \<exists>ml ml'. eval\<^sub>g (merge ml m) (merge ml' m') (GP E)} 
      (\<lambda>x. {mem. \<forall>h ml. eval\<^sub>p (merge ml mem) h (\<L>\<^sub>R E (Global x))})
      (\<lambda>x. {mem. \<forall>h ml. eval\<^sub>p (merge ml mem) h (\<L>\<^sub>G E (Global x))})"

lemma RAbs:
  shows "(m,m') \<in> RAbs (toAbstract E) = (\<forall>ml. ((merge ml m, merge ml m')) \<in> R E)"
  unfolding toAbstract_def RAbs_def R_def RtoRel_def
  by auto

lemma GAbs:
  assumes "(merge ml1 m, merge ml3 m') \<in> G E"
  shows "(m,m') \<in> GAbs (toAbstract E)"
  using assms
  unfolding toAbstract_def GAbs_def G_def GtoRel_def
  by auto

lemma merge_modified':
  assumes "Global x \<in> modified (merge ml m1) (merge ml' m3)"
  shows "x \<in> modified m1 m3"
  using assms unfolding modified_def merge_def by (auto)

lemma LRAbs:
  assumes "\<forall>m x. (\<exists>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>R E (Global x))) \<longrightarrow> (\<forall>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>R E (Global x)))"
  assumes "eval\<^sub>p (merge ml m3) h\<^sub>1 (\<L>\<^sub>R E (Global x))"
  shows "m3 \<in> LRAbs (toAbstract E) x"
  using assms unfolding toAbstract_def LRAbs_def 
  by auto

lemma LGAbs:
  assumes "m3 \<in> LGAbs (toAbstract E) x"
  shows "\<forall>ml. \<exists>h\<^sub>1. eval\<^sub>p (merge ml m3) h\<^sub>1 (\<L>\<^sub>G E (Global x))"
  using assms unfolding toAbstract_def LGAbs_def 
  by auto

lemma LAbs:
  assumes "\<forall>m x. (\<exists>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>R E (Global x))) \<longrightarrow> (\<forall>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>R E (Global x)))"
  assumes "\<forall>m x. (\<exists>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>G E (Global x))) \<longrightarrow> (\<forall>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>G E (Global x)))"
  assumes "eval\<^sub>p (merge ml m3) h\<^sub>1 (\<L> E (Global x))"
  shows "m3 \<in> LAbs (toAbstract E) x"
  using assms 
  unfolding toAbstract_def LAbs_def \<L>_def LRAbs_def \<L>\<^sub>R_def LGAbs_def \<L>\<^sub>G_def
  by fastforce

lemma LAbs':
  assumes "m3 \<in> LAbs (toAbstract E) x"
  shows "\<forall>ml h\<^sub>1. eval\<^sub>p (merge ml m3) h\<^sub>1 (\<L> E (Global x))"
  using assms 
  unfolding toAbstract_def LAbs_def \<L>_def LRAbs_def \<L>\<^sub>R_def LGAbs_def \<L>\<^sub>G_def
  by auto

lemma LRAbs_low:
  assumes "m3 =\<^bsub>LRAbs (toAbstract E),(modified m1 m3 \<union> modified m2 m4)\<^esub> m4"
  assumes "\<forall>x. \<L>\<^sub>R E (Local x) =\<^sub>P PFalse"
  assumes "\<forall>m x. (\<exists>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>R E (Global x))) \<longrightarrow> (\<forall>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>R E (Global x)))"
  shows "merge ml m3,h\<^sub>1 =\<^sup>l\<^bsub>\<L>\<^sub>R E,(modified (merge ml m1) (merge ml m3) \<union> modified (merge ml' m2) (merge ml' m4))\<^esub> h\<^sub>2,merge ml' m4"
  using assms(1)  unfolding low_eq_def low_eq_abs_def 
  apply (intro ballI)
  apply (case_tac "x")
   using assms(2) apply (auto simp: merge_def equiv_def)[1]
  using LRAbs[OF assms(3), of ml m3 h\<^sub>1] LRAbs[OF assms(3), of ml' m4 h\<^sub>2] merge_modified'[of _ ml] merge_modified'[of _ ml' m2] 
  unfolding merge_def
  by (simp add: LRAbs_def Un_def modified_def)

lemma merge_mod:
  assumes "x \<in> modified m1 m3 \<union> modified m2 m4"
  shows "Global x\<in>modified (merge ml1 m1) (merge ml3 m3) \<union> modified (merge ml2 m2) (merge ml4 m4)"
  using assms unfolding merge_def modified_def
  by auto

lemma LGAbs_low:
  assumes "\<forall>h\<^sub>1 h\<^sub>2. merge ml3 m3,h\<^sub>1 =\<^sup>l\<^bsub>\<L>\<^sub>G E,(modified (merge ml1 m1) (merge ml3 m3) \<union> modified (merge ml2 m2) (merge ml4 m4))\<^esub> h\<^sub>2,merge ml4 m4"
  shows "m3 =\<^bsub>LGAbs (toAbstract E),(modified m1 m3 \<union> modified m2 m4)\<^esub> m4"
  using assms  unfolding low_eq_def low_eq_abs_def equiv_def
  apply (intro ballI)
  using LGAbs[of m3 E] LGAbs[of m4 E] merge_mod[of _ m1 m3 m2 m4]
  by (smt Var.case(2) merge_def)

lemma LAbs_low:
  assumes "m3 =\<^bsub>LAbs (toAbstract E),UNIV\<^esub> m4" 
  assumes "\<forall>m x. (\<exists>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>R E (Global x))) \<longrightarrow> (\<forall>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>R E (Global x)))"
  assumes "\<forall>m x. (\<exists>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>G E (Global x))) \<longrightarrow> (\<forall>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>G E (Global x)))"
  assumes "\<forall>x. \<L> E (Local x)  =\<^sub>P PFalse"
  shows "merge ml m3,h\<^sub>1 =\<^sup>l\<^bsub>\<L> E,UNIV\<^esub> h\<^sub>2,merge ml' m4"
  using assms(1,4) LAbs[OF assms(2,3)] unfolding low_eq_def low_eq_abs_def  merge_def equiv_def
  apply (intro ballI)
    apply (case_tac "x")
   apply (auto simp: merge_def)[1]
  by (simp)

lemma LAbs_low':
  assumes "\<forall>h\<^sub>1 h\<^sub>2. merge ml3 m3,h\<^sub>1 =\<^sup>l\<^bsub>\<L> E,UNIV\<^esub> h\<^sub>2,merge ml4 m4"
  shows "m3 =\<^bsub>LAbs (toAbstract E),UNIV\<^esub> m4" 
  using assms 
  unfolding low_eq_def low_eq_abs_def 
  apply (intro ballI)
  apply auto
   apply (drule LAbs')
  apply (simp add: merge_def)
   apply force
  apply (drule LAbs')
    apply (simp add: merge_def)
  by force

lemma R\<^sub>pAbs:
  assumes "\<forall>x. \<L> E (Local x)  =\<^sub>P PFalse"
  assumes "\<forall>x. \<L>\<^sub>R E (Local x)  =\<^sub>P PFalse"
  assumes "\<forall>m x. (\<exists>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>R E (Global x))) \<longrightarrow> (\<forall>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>R E (Global x)))"
  assumes "\<forall>m x. (\<exists>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>G E (Global x))) \<longrightarrow> (\<forall>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>G E (Global x)))"
  assumes "((m1,m2),m3,m4) \<in> R\<^sub>pAbs (toAbstract E)"
  shows "\<forall>ml ml'. ((merge ml m1, merge ml' m2),merge ml m3, merge ml' m4) \<in> R\<^sub>L E"
  using assms RAbs LRAbs_low LAbs_low  
  unfolding R\<^sub>pAbs_def R\<^sub>L_def
  by auto

lemma G\<^sub>pAbs:
  assumes "(((merge ml1 m1, merge ml2 m2),merge ml3 m3, merge ml4 m4) \<in> G\<^sub>L E)"
  shows "((m1,m2),m3,m4) \<in> G\<^sub>pAbs (toAbstract E)"
  using assms GAbs LAbs_low' LGAbs_low
  unfolding G\<^sub>pAbs_def G\<^sub>L_def
  by auto

lemma merge_split:
  shows "merge (local mem\<^sub>2'') (global mem\<^sub>2'') = mem\<^sub>2''"
proof -
  have "\<forall>x. merge (local mem\<^sub>2'') (global mem\<^sub>2'') x = mem\<^sub>2'' x"
    apply (intro allI, case_tac x)
    unfolding merge_def local_def global_def
    by auto
  thus ?thesis by blast
qed

lemma merge_global[simp]:
  shows "global (merge ml mem) = mem"
proof -
  have "\<forall>x. global (merge ml mem) x = mem x"
    apply (intro allI)
    unfolding merge_def local_def global_def
    by auto
  thus ?thesis by blast
qed

lemma test:
  assumes rg: "\<forall>i < length c. \<forall>m x. (\<exists>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>R (Es ! i) (Global x))) \<longrightarrow> (\<forall>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>R (Es ! i) (Global x)))"
  assumes gg: "\<forall>i < length c. \<forall>m x. (\<exists>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>G (Es ! i) (Global x))) \<longrightarrow> (\<forall>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>G (Es ! i) (Global x)))"
  assumes "length Es = length c"
  assumes "i < length Es"
  assumes "eval\<^sub>p mem h (\<L> (Es ! i) (Global x))"
  assumes "\<forall>j \<in> set Es. \<forall> x. \<L> (Es ! i) x \<turnstile> \<L>\<^sub>G j x"
  shows "global mem \<in> LAbs (merge\<^sub>e (map toAbstract Es)) x"
  unfolding merge\<^sub>e_def LAbs_def LRAbs_def LGAbs_def
proof (auto, goal_cases)
  case 1
  have a: "eval\<^sub>p (merge (local mem) (global mem)) h (\<L>\<^sub>R (Es ! i) (Global x))"
    using assms by (auto simp:  merge_split \<L>_def)
  have b: "Es ! i \<in> set Es" using assms by auto
  show ?case
    unfolding toAbstract_def
    apply auto
    using a b assms(3,4) rg by metis
next
  case (2 y)
  then obtain j where j: "j < length Es" "Es ! j = y"
    by (meson in_set_conv_nth)
  have "eval\<^sub>p (merge (local mem) (global mem)) h (\<L> (Es ! i) (Global x))"
    using assms by (auto simp: merge_split \<L>_def)
  hence a: "eval\<^sub>p (merge (local mem) (global mem)) h (\<L>\<^sub>G y (Global x))"
    using 2 assms(6) by (auto simp: entail_def)
  then show ?case
    unfolding toAbstract_def
    apply auto using a gg j assms(3)
    by metis
qed

theorem security:
  (* Require all locals to have False classifications *)
  assumes hl: "\<forall>i < length c. \<forall> x. \<L>\<^sub>R (Es ! i) (Local x) =\<^sub>P PFalse"
  (* Require no references to temporary variables or locals in classifications *)
  assumes rg: "\<forall>i < length c. \<forall>m x. (\<exists>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>R (Es ! i) (Global x))) \<longrightarrow> (\<forall>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>R (Es ! i) (Global x)))"
  assumes gg: "\<forall>i < length c. \<forall>m x. (\<exists>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>G (Es ! i) (Global x))) \<longrightarrow> (\<forall>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>G (Es ! i) (Global x)))"
  (* Require compatibility on environments, compat R G *)
  assumes cmp: "compat\<^sub>e (map toAbstract Es)"
  (* Require an environment for each component *)
  assumes len: "length Es = length c"
  (* Local security property, regardless of initial local memory, secure_L *)
  assumes sec: "\<forall>i < length c. lsec (fst (c ! i)) (Es ! i)"
  shows "gsec c (LAbs (merge\<^sub>e (map toAbstract Es)))"
proof -
  let ?Es = "map toAbstract Es" and ?E = "merge\<^sub>e (map toAbstract Es)"
  
  have "sb_intro\<^sub>G c {(m1,m2). m1 =\<^bsub>LAbs ?E\<^esub> m2} (R\<^sub>pAbs (merge\<^sub>e ?Es)) (G\<^sub>pAbs (merge\<^sub>e ?Es))"
  proof (intro lsec)
    show "compat\<^sub>e (map toAbstract Es)" using cmp by auto
  next
    show "length (map toAbstract Es) = length c" using len by auto
  next
    have "\<forall>i<length c.
       sb_intro\<^sub>L 
        (c ! i) 
        {(m1, m2). m1 =\<^bsub>LAbs (merge\<^sub>e (map toAbstract Es)),UNIV\<^esub> m2} 
        (R\<^sub>pAbs ( toAbstract (Es ! i)))
        (G\<^sub>pAbs ( toAbstract (Es ! i)))"
      unfolding sb_intro\<^sub>L_def  
    proof auto
      fix i mem\<^sub>1 mem\<^sub>2 assume a: "i < length c" "mem\<^sub>1 =\<^bsub>LAbs (merge\<^sub>e (map toAbstract Es)),UNIV\<^esub> mem\<^sub>2"
      have l: "\<forall>x. \<L> (Es ! i) (Local x) =\<^sub>P PFalse" "\<forall>x. \<L>\<^sub>R (Es ! i) (Local x) =\<^sub>P PFalse"
          "\<forall>m x. (\<exists>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>R (Es ! i) (Global x))) \<longrightarrow> (\<forall>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>R (Es ! i) (Global x)))"
          "\<forall>m x. (\<exists>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>G (Es ! i) (Global x))) \<longrightarrow> (\<forall>l h. eval\<^sub>p (merge l m) h (\<L>\<^sub>G (Es ! i) (Global x)))"
        using a hl rg gg by (auto simp: \<L>_def equiv_def)

      have b: "\<forall>j \<in> set Es. \<forall>x. \<L> (Es ! i) x \<turnstile> \<L>\<^sub>G (j) x"
      proof (intro ballI allI)
        fix y x assume "y \<in> set Es"
        then obtain j where j: "j < length Es" "Es ! j = y"
          by (meson in_set_conv_nth)

        show "\<L> (Es ! i) x \<turnstile> \<L>\<^sub>G y x"
        proof (cases "j = i")
          case True
          then show ?thesis using j by (auto simp: \<L>_def entail_def)
        next
          case False
          have i: "i < length Es" using a(1) assms by auto
          have "(\<forall>x. LRAbs (map toAbstract Es ! i) x \<subseteq> LGAbs (map toAbstract Es ! j) x)"
            using False j i cmp unfolding compat\<^sub>e_def by fastforce
          hence p: "(\<forall>x. LRAbs (toAbstract (Es ! i)) x \<subseteq> LGAbs (toAbstract (Es ! j)) x)"
            using i j by auto

          hence "\<L>\<^sub>R (Es ! i) x \<turnstile> \<L>\<^sub>G y x"
            unfolding  \<L>\<^sub>R_def \<L>\<^sub>G_def LGAbs_def LRAbs_def entail_def
            apply auto
            unfolding toAbstract_def \<L>\<^sub>R_def \<L>\<^sub>G_def
            apply auto
            apply (case_tac x)
            using hl a(1) apply (auto simp: \<L>\<^sub>R_def equiv_def)[1]
            using j apply auto
          proof -
            fix mem h x2
            assume "eval\<^sub>p mem h (case Es ! i of Env x xa LR xb \<Rightarrow>  LR (Global x2))"
            hence "eval\<^sub>p (merge (local mem) (global mem)) h (case Es ! i of Env x xa LR xb \<Rightarrow>  LR (Global x2))"
              by (auto simp: merge_split)
            hence "\<forall>h ml. eval\<^sub>p (merge ml (global mem)) h (case Es ! i of Env x xa LR xb \<Rightarrow>  LR (Global x2))"
              using rg a(1) unfolding \<L>\<^sub>R_def by blast
            hence "global mem \<in> {mem. \<forall>h ml. eval\<^sub>p (merge ml mem) h (case Es ! i of Env x xa LR xb \<Rightarrow>  LR (Global x2))}"
              by auto
            hence "global mem \<in> {mem. \<forall>h ml. eval\<^sub>p (merge ml mem) h (case Es ! j of Env x xa xb LG \<Rightarrow>  LG (Global x2))}"
              using p unfolding LGAbs_def LRAbs_def toAbstract_def
              apply (auto simp: \<L>\<^sub>R_def \<L>\<^sub>G_def) by blast
            hence "eval\<^sub>p (merge (local mem) (global mem)) h (case Es ! j of Env x xa xb LG \<Rightarrow>  LG (Global x2))"
              by auto
            thus "eval\<^sub>p mem h (case Es ! j of Env x xa xb LG \<Rightarrow>  LG (Global x2))"
              by (auto simp: merge_split)
          qed

          then show ?thesis by (auto simp: \<L>_def entail_def)
        qed
      qed

      hence "merge (snd (c ! i)) mem\<^sub>1 =\<^sup>g\<^bsub>\<L> (Es ! i)\<^esub> merge (snd (c ! i)) mem\<^sub>2"
        using a(2) unfolding low_eq_def low_eq_abs_def
        apply (intro allI ballI)
        apply (case_tac x)
         apply (simp add: merge_def)
        using a len test[OF rg gg len, of i]
        by (metis UNIV_I global_def merge_global)        

      hence "
          (\<exists>\<B>. strong_bisim \<B> (R\<^sub>L (Es ! i)) (G\<^sub>L (Es ! i)) \<and>
                ((fst (c ! i), merge (snd (c ! i)) mem\<^sub>1), fst (c ! i), merge (snd (c ! i)) mem\<^sub>2) \<in> \<B>)"
        using a(1) sec unfolding lsec_def low_eq_def by auto

      then obtain \<B> where "strong_bisim \<B> (R\<^sub>L (Es ! i)) (G\<^sub>L (Es ! i)) \<and> (( (fst (c ! i)), merge (snd (c ! i)) mem\<^sub>1 ), ( (fst (c ! i)), merge (snd (c ! i)) mem\<^sub>2 )) \<in> \<B>"
        using assms(3) a(1)
        unfolding lsec_def 
        by auto

      hence "strong_bisim\<^sub>L \<B> (R\<^sub>pAbs (toAbstract (Es ! i))) (G\<^sub>pAbs (toAbstract (Es ! i))) \<and> (\<langle>c ! i, mem\<^sub>1\<rangle>, \<langle>c ! i, mem\<^sub>2\<rangle>) \<in> \<B>"
      
          unfolding conf_def strong_bisim\<^sub>L_def  strong_bisim_def
        apply (clarsimp, intro conjI allI impI)
         prefer 2
        apply (simp add: stable\<^sub>L_def stable_def conf_def)
         apply (intro allI impI)
         apply (drule R\<^sub>pAbs[OF l])
         apply blast
        apply (simp add: bisim\<^sub>L_def conf_def)
         apply (elim conjE, intro allI impI conjI)
      proof -
        fix a b mem\<^sub>1' aa ba mem\<^sub>2' ab bb mem\<^sub>1''
        assume a: "((a, merge b mem\<^sub>1'), aa, merge ba mem\<^sub>2') \<in> \<B>"
        assume b: "(a, merge b mem\<^sub>1') \<leadsto> (ab, merge bb mem\<^sub>1'')"
        assume c: "bisim \<B> (G\<^sub>L (Es ! i))"

        hence t: "\<And>c\<^sub>1 mem\<^sub>1 c\<^sub>2 mem\<^sub>2 c\<^sub>1' mem\<^sub>1'.
        (( c\<^sub>1, mem\<^sub>1 ), ( c\<^sub>2, mem\<^sub>2 )) \<in> \<B> \<Longrightarrow>
        ( c\<^sub>1, mem\<^sub>1 ) \<leadsto> ( c\<^sub>1', mem\<^sub>1' ) \<Longrightarrow>
        (\<exists>c\<^sub>2' mem\<^sub>2'. 
          ( c\<^sub>2, mem\<^sub>2 ) \<leadsto> ( c\<^sub>2', mem\<^sub>2' ) \<and>
          ((mem\<^sub>1,mem\<^sub>2),(mem\<^sub>1',mem\<^sub>2')) \<in> (G\<^sub>L (Es ! i)) \<and>
          (( c\<^sub>1', mem\<^sub>1' ), ( c\<^sub>2', mem\<^sub>2' )) \<in> \<B>)"
          by (auto simp: bisim_def)

        then obtain c\<^sub>2' mem\<^sub>2'' where "
      (aa, merge ba mem\<^sub>2') \<leadsto> (c\<^sub>2', mem\<^sub>2'') \<and>
      ((merge b mem\<^sub>1', merge ba mem\<^sub>2'), merge bb mem\<^sub>1'', mem\<^sub>2'') \<in> G\<^sub>L (Es ! i) \<and>
      ((ab, merge bb mem\<^sub>1''), c\<^sub>2', mem\<^sub>2'') \<in> \<B>"
          using t[OF a b]
          by (auto simp: bisim_def)

        hence "
        (aa, merge ba mem\<^sub>2') \<leadsto> (c\<^sub>2', merge (local mem\<^sub>2'') (global mem\<^sub>2'')) \<and>
        ((mem\<^sub>1', mem\<^sub>2'), mem\<^sub>1'', (global mem\<^sub>2'')) \<in> G\<^sub>pAbs (toAbstract (Es ! i)) \<and>
        ((ab, merge bb mem\<^sub>1''), c\<^sub>2', merge (local mem\<^sub>2'') (global mem\<^sub>2'')) \<in> \<B>"
          using G\<^sub>pAbs[of b mem\<^sub>1' ba mem\<^sub>2' bb mem\<^sub>1'' "local mem\<^sub>2''" ]
          by (auto simp: merge_split)

        thus "\<exists>a b mem\<^sub>2''.
          (aa, merge ba mem\<^sub>2') \<leadsto> (a, merge b mem\<^sub>2'') \<and>
          ((mem\<^sub>1', mem\<^sub>2'), mem\<^sub>1'', mem\<^sub>2'') \<in> G\<^sub>pAbs (toAbstract (Es ! i)) \<and>
          ((ab, merge bb mem\<^sub>1''), a, merge b mem\<^sub>2'') \<in> \<B>"
          by blast
      qed
      
      thus "\<exists>\<B>. strong_bisim\<^sub>L \<B> (R\<^sub>pAbs (toAbstract (Es ! i))) (G\<^sub>pAbs (toAbstract (Es ! i))) \<and> (\<langle>c ! i, mem\<^sub>1\<rangle>, \<langle>c ! i, mem\<^sub>2\<rangle>) \<in> \<B>" 
        by auto
    qed
    thus " \<forall>i<length c.
       sb_intro\<^sub>L (c ! i) {(m1, m2). m1 =\<^bsub>LAbs (merge\<^sub>e (map toAbstract Es)),UNIV\<^esub> m2} (R\<^sub>pAbs (map toAbstract Es ! i))
        (G\<^sub>pAbs (map toAbstract Es ! i))"
      by (simp add: len)
  qed

  thus ?thesis by (auto intro: global_gsec)
qed

end

end
