theory Preliminaries
  imports PredicateLanguage
begin

datatype Sec = High | Low

instantiation Sec :: complete_lattice
begin

definition top_Sec_def: "top = High"
definition sup_Sec_def: "sup d1 d2 = (if (d1 = High \<or> d2 = High) then High else Low)"
definition inf_Sec_def: "inf d1 d2 = (if (d1 = Low \<or> d2 = Low) then Low else High)"
definition bot_Sec_def: "bot = Low"
definition less_eq_Sec_def: "d1 \<le> d2 = (d1 = d2 \<or> d1 = Low)"
definition less_Sec_def: "d1 < d2 = (d1 = Low \<and> d2 = High)"
definition Sup_Sec_def: "Sup S = (if (High \<in> S) then High else Low)"
definition Inf_Sec_def: "Inf S = (if (Low \<in> S) then Low else High)"

instance
  apply (intro_classes)
                 using Sec.exhaust less_Sec_def less_eq_Sec_def inf_Sec_def sup_Sec_def apply auto[10]
       apply (auto simp: Inf_Sec_def Sec.exhaust less_eq_Sec_def)
      using Inf_Sec_def Sec.exhaust less_eq_Sec_def apply (metis)
     using Sec.exhaust less_Sec_def less_eq_Sec_def inf_Sec_def sup_Sec_def Inf_Sec_def Sup_Sec_def top_Sec_def bot_Sec_def 
     by auto
end

text \<open>Memory is broken into a local and global context\<close>
datatype ('local, 'global) Var = Local "'local" | Global "'global" 

text \<open>Full memory definition with both local and global variables\<close>
type_synonym ('local, 'global, 'val) FullMem = "(('local, 'global) Var, 'val) Mem"

text \<open>Local configuration with full memory\<close>
type_synonym ('com, 'local, 'global, 'val) Conf = "('com \<times> ('local, 'global, 'val) FullMem)"

text \<open>Local configurations with only local memory\<close>
type_synonym ('com, 'local, 'val) LocalConf = "('com \<times> ('local, 'val) Mem)"

text \<open>Global configurations with list of local configurations\<close>
type_synonym ('com, 'local, 'global, 'val) GlobalConf = 
  "('com, 'local, 'val) LocalConf list \<times> ('global, 'val) Mem"

definition merge :: "('local, 'val) Mem \<Rightarrow> ('global, 'val) Mem \<Rightarrow> ('local, 'global, 'val) FullMem"
  where "merge l g \<equiv> \<lambda>v. case v of Local n \<Rightarrow> l n | Global n \<Rightarrow> g n"

text \<open>TASK:Transition over local configurations\<close>
definition local :: "('local, 'global, 'val) FullMem \<Rightarrow> ('local, 'val) Mem"
  where "local m \<equiv> \<lambda>v. m (Local v)"

text \<open>TASK:Transition over global configurations\<close>
definition global :: "('local, 'global, 'val) FullMem \<Rightarrow> ('global, 'val) Mem"
  where "global m \<equiv> \<lambda>v. m (Global v)"

locale sifum_security_no_det =
  fixes eval :: "('Com, 'Local, 'Global, 'Val) Conf rel"

locale sifum_security = sifum_security_no_det eval
  for eval :: "('Com, 'Local, 'Global, 'Val) Conf rel" +
  assumes deterministic: "\<lbrakk> (lc, lc') \<in> eval; (lc, lc'') \<in> eval \<rbrakk> \<Longrightarrow> lc' = lc''"

end