theory Language
  imports Main Security
begin

subsection \<open> Syntax \<close>

datatype ('var, 'aexp, 'bexp) Action =
  Assign "'var" "'aexp"  (infix "\<leftarrow>" 999)
  | Fence
  | Guard "'bexp" ("[_?]" [0] 999)

datatype ('var, 'aexp, 'bexp) Stmt =
  Stop
  | Act "('var, 'aexp, 'bexp) Action" "('var, 'aexp, 'bexp) Stmt" (infixr ";;;" 150)
  | Choice "('var, 'aexp, 'bexp) Stmt" "('var, 'aexp, 'bexp) Stmt" (infixr "?" 150)
  | Loop "('var, 'aexp, 'bexp) Stmt" "('var, 'aexp, 'bexp) Stmt"

datatype DetChoice = L | R | U "nat"

locale sifum_lang_no_dma = predicate_lang "undefined::('Local, 'Global) Var \<times> 'Val::type" +
  fixes ev\<^sub>A :: "(('Local, 'Global) Var, 'Val) Mem \<Rightarrow> 'AExp \<Rightarrow> 'Val"
  fixes ev\<^sub>B :: "(('Local, 'Global) Var, 'Val) Mem \<Rightarrow> 'BExp \<Rightarrow> bool"
  fixes aexp_to_exp :: "'AExp \<Rightarrow> (('Local, 'Global) Var,'Val) lexp"
  fixes bexp_to_pred :: "'BExp \<Rightarrow> (('Local, 'Global) Var,'Val) lpred"
  fixes bexp_neg :: "'BExp \<Rightarrow> 'BExp"
  assumes aexp_to_exp_correct: "\<And>e mem h. eval\<^sub>e mem h (aexp_to_exp e) = ev\<^sub>A mem e"
  assumes bexp_to_pred_correct: "\<And>e mem h. eval\<^sub>p mem h (bexp_to_pred e) = ev\<^sub>B mem e"
  assumes neg_vars [simp]: "\<And>b. vars\<^sub>p (bexp_to_pred (bexp_neg b)) = vars\<^sub>p (bexp_to_pred ( b))"

context sifum_lang_no_dma
begin

lemma eval_vars_det\<^sub>A:
  assumes "\<forall>x \<in> vars\<^sub>e (aexp_to_exp e). mem\<^sub>1 x = mem\<^sub>2 x" (is "\<forall>x \<in> vars\<^sub>e ?e. ?eq x")
  shows "ev\<^sub>A mem\<^sub>1 e = ev\<^sub>A mem\<^sub>2 e"
proof -
  have "\<forall>h. ev\<^sub>A mem\<^sub>1 e = eval\<^sub>e mem\<^sub>1 h ?e" "\<forall>h. ev\<^sub>A mem\<^sub>2 e = eval\<^sub>e mem\<^sub>2 h ?e"
    using aexp_to_exp_correct by auto
  moreover have "\<forall>h. eval\<^sub>e mem\<^sub>1 h ?e = eval\<^sub>e mem\<^sub>2 h ?e"
    by (simp add: assms leval_vars_det)
  ultimately show ?thesis by auto
qed

lemma eval_vars_det\<^sub>B:
  assumes "\<forall>x \<in> vars\<^sub>p (bexp_to_pred e). mem\<^sub>1 x = mem\<^sub>2 x" (is "\<forall>x \<in> vars\<^sub>p ?e. ?eq x")
  shows "ev\<^sub>B mem\<^sub>1 e = ev\<^sub>B mem\<^sub>2 e"
proof -
  have "\<forall>h. ev\<^sub>B mem\<^sub>1 e = eval\<^sub>p mem\<^sub>1 h ?e" "\<forall>h. ev\<^sub>B mem\<^sub>2 e = eval\<^sub>p mem\<^sub>2 h ?e"
    using bexp_to_pred_correct by auto
  moreover have "\<forall>h. eval\<^sub>p mem\<^sub>1 h ?e = eval\<^sub>p mem\<^sub>2 h ?e"
    by (simp add: assms lpred_eval_vars_det)
  ultimately show ?thesis by auto
qed

type_synonym ('local, 'global, 'val, 'aexp, 'bexp) LC =
  "((('local, 'global) Var, 'aexp, 'bexp) Stmt \<times> DetChoice list, 'local, 'global, 'val) Conf"

text \<open> Abbreviation for program state \<close>

abbreviation conf\<^sub>w_abv :: "(('local, 'global) Var, 'AExp, 'BExp) Stmt  \<Rightarrow> DetChoice list \<Rightarrow> (('local, 'global) Var , 'Val) Mem  \<Rightarrow> (_,_,_,_) Conf"
  ("\<langle>_, _, _\<rangle>\<^sub>w" [0, 60, 120] 70)
  where
  "\<langle> c, det, mem \<rangle>\<^sub>w \<equiv> (((c, det)), mem)"

subsection \<open> Stmt Functions \<close>

text \<open> Define paper's Seq, If and While in terms of existing Stmts \<close>
fun Seq :: "('var, 'aexp, 'bexp) Stmt \<Rightarrow> ('var, 'aexp, 'bexp) Stmt \<Rightarrow> ('var, 'aexp, 'bexp) Stmt"
  (infixr ";;" 190)
  where
  "Seq Stop c = c" |
  "Seq (a ;;; c\<^sub>1) c\<^sub>2 = (a ;;; (c\<^sub>1 ;; c\<^sub>2))" |
  "Seq (Choice c\<^sub>1 c\<^sub>2) c\<^sub>3 = (Choice (c\<^sub>1 ;; c\<^sub>3) (c\<^sub>2 ;; c\<^sub>3))" |
  "Seq (Loop c\<^sub>1 c\<^sub>2) c\<^sub>3 = Loop c\<^sub>1 (c\<^sub>2 ;; c\<^sub>3)"

fun unroll :: "(('Local, 'Global) Var, 'AExp, 'BExp) Stmt \<Rightarrow> nat \<Rightarrow> (('Local, 'Global) Var, 'AExp, 'BExp) Stmt"
  where
  "unroll c 0 = Stop" |
  "unroll c (Suc n) = c ;; (unroll c n)"

subsection \<open> Reordering Semantics \<close>

text \<open> Sets for variables used in Actions \<close>
fun reads :: "(('Local, 'Global) Var, 'AExp, 'BExp) Action \<Rightarrow> ('Local, 'Global) Var set"
  where
  "reads ([b?]) = (vars\<^sub>p (bexp_to_pred b))" |
  "reads (x \<leftarrow> e) = (vars\<^sub>e (aexp_to_exp e))" |
  "reads _ = {}"

fun writes :: "(('Local, 'Global) Var, 'AExp, 'BExp) Action \<Rightarrow> ('Local, 'Global) Var set"
  where
  "writes (x \<leftarrow> e) = {x}" |
  "writes _ = {}"

abbreviation vars :: "(('Local, 'Global) Var, 'AExp, 'BExp) Action \<Rightarrow> ('Local, 'Global) Var set"
  where
  "vars a \<equiv> reads a \<union> writes a"

subsection \<open> Semantics \<close>

text \<open>
  Evaluate an Action, relating the pre and post memory states.
\<close>
fun eval_action :: "(('Local, 'Global) Var, 'Val) Mem \<Rightarrow> (('Local, 'Global) Var, 'AExp, 'BExp) Action \<Rightarrow> (('Local, 'Global) Var , 'Val) Mem \<Rightarrow> bool"
  where
  assign: "eval_action mem (x \<leftarrow> e) mem' = (mem' = mem (x := (ev\<^sub>A mem e)))" |
  guard:  "eval_action mem ([b?]) mem' = (mem = mem' \<and> ev\<^sub>B mem b)" |
  other:  "eval_action mem a mem' = (mem = mem')"

inductive_set next\<^sub>a ::
  "(((('Local, 'Global) Var, 'AExp, 'BExp) Stmt \<times> DetChoice list) \<times> (('Local, 'Global) Var, 'AExp, 'BExp) Action \<times> ((('Local, 'Global) Var, 'AExp, 'BExp) Stmt \<times> DetChoice list)) set"
and next\<^sub>a_abv :: "((('Local, 'Global) Var, 'AExp, 'BExp) Stmt \<times> DetChoice list) \<Rightarrow> (('Local, 'Global) Var, 'AExp, 'BExp) Action \<Rightarrow> ((('Local, 'Global) Var, 'AExp, 'BExp) Stmt \<times> DetChoice list) \<Rightarrow> bool"
  ("_ \<rightarrow>\<^bsub>_\<^esub> _" [11,0,0] 70)
  where
  "c \<rightarrow>\<^bsub>\<alpha>\<^esub> c' \<equiv> (c, \<alpha>, c') \<in> next\<^sub>a" |
  act: "(\<alpha> ;;; c, L#det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c, det)" |
  choice1: "(c\<^sub>1, det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c\<^sub>1', det') \<Longrightarrow> (c\<^sub>1 ? c\<^sub>2, L#det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c\<^sub>1', det')" |
  choice2: "(c\<^sub>2, det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c\<^sub>2', det') \<Longrightarrow> (c\<^sub>1 ? c\<^sub>2, R#det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c\<^sub>2', det')" |
  loop: "(unroll c\<^sub>1 n ;; c\<^sub>2, det) \<rightarrow>\<^bsub>\<beta>\<^esub> (c, det') \<Longrightarrow> (Loop c\<^sub>1 c\<^sub>2, U n#det) \<rightarrow>\<^bsub>\<beta>\<^esub> (c, det')"

text \<open>
  Evaluation of a statement, specifying the atomic action that has been carried out.
\<close>
inductive_set eval\<^sub>a ::
  "(('Local, 'Global, 'Val, 'AExp, 'BExp) LC \<times> (('Local, 'Global) Var, 'AExp, 'BExp) Action \<times> ('Local, 'Global, 'Val, 'AExp, 'BExp) LC) set"
and eval\<^sub>a_abv :: "('Local, 'Global, 'Val, 'AExp, 'BExp) LC \<Rightarrow> (('Local, 'Global) Var, 'AExp, 'BExp) Action \<Rightarrow> ('Local, 'Global, 'Val, 'AExp, 'BExp) LC \<Rightarrow> bool"
  ("_ \<leadsto>\<^bsub>_\<^esub> _" [11,0,0] 70)
  where
  "c \<leadsto>\<^bsub>\<alpha>\<^esub> c' \<equiv> (c, \<alpha>, c') \<in> eval\<^sub>a" |
  evala[intro]: "\<lbrakk> eval_action mem \<alpha> mem' ; (c, det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c', det') \<rbrakk> \<Longrightarrow> \<langle>c, det, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det', mem'\<rangle>\<^sub>w"

lemma eval\<^sub>a_elim [elim]:
  assumes "\<langle>c, det, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det', mem'\<rangle>\<^sub>w"
  obtains "(c, det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c', det')" "eval_action mem \<alpha> mem'"
  using assms by (blast elim: eval\<^sub>a.cases)

subsection \<open> Speculation \<close>

text \<open>
  Build up a predicate to express valid speculation for a sequence of actions
\<close>

inductive_set eval\<^sub>w :: "('Local, 'Global, 'Val, 'AExp, 'BExp) LC rel"
and eval\<^sub>w_abv :: "('Local, 'Global, 'Val, 'AExp, 'BExp) LC \<Rightarrow> ('Local, 'Global, 'Val, 'AExp, 'BExp) LC \<Rightarrow> bool"
  ("_ \<leadsto>\<^sub>w _" 70)
  where
  "c \<leadsto>\<^sub>w c' \<equiv> (c, c') \<in> eval\<^sub>w" |
  eval_a: "\<lbrakk> \<langle>c, det, mem\<rangle>\<^sub>w \<leadsto>\<^bsub>a\<^esub> \<langle>c', det', mem'\<rangle>\<^sub>w  \<rbrakk>
            \<Longrightarrow> \<langle>c, det, mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', det', mem'\<rangle>\<^sub>w"

subsection \<open> Semantic Properties \<close>

lemma eval_iff: 
  "(\<exists>\<alpha>. (\<langle>c, det,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w) ) =
   (\<langle>c, det,  mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', det',  mem'\<rangle>\<^sub>w)"
  by (rule iffI, auto simp: eval_a eval\<^sub>w.simps)

lemma stop_no_next_a: "\<not> ((Stop, det) \<rightarrow>\<^bsub>a\<^esub> (c, det'))"
  by (blast elim: next\<^sub>a.cases)

lemma nil_next_a: "\<not> ((c, []) \<rightarrow>\<^bsub>a\<^esub> (c', det'))"
  by (blast elim: next\<^sub>a.cases)

lemma stop_no_eval_a: "\<not> (\<langle>Stop, det,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>a\<^esub> \<langle>c, det',  mem'\<rangle>\<^sub>w)"
  using stop_no_next_a by blast

lemma nil_eval_a: "\<not> (\<langle>c, [],  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>a\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w)"
  using nil_next_a by blast

lemma stop_no_eval: "\<not> (\<langle>Stop, det,  mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c, det',  mem'\<rangle>\<^sub>w)"
  using eval_iff stop_no_eval_a by metis

lemma nil_eval: "\<not> (\<langle>c, [],  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>a\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w)"
  using eval_iff nil_eval_a by metis

lemma actD_a [dest]:
  assumes "\<langle>\<alpha> ;;; c, L#det,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<beta>\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w"
  shows "\<beta> = \<alpha> \<and> c' = c \<and> det' = det \<and> eval_action mem \<alpha> mem'"
  using assms next\<^sub>a.cases[of "\<alpha> ;;; c" "L # det" \<beta>] by auto

lemma assignD_a [dest]:
  assumes "\<langle>x \<leftarrow> e ;;; c, L#det,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w"
  shows "\<alpha> = x \<leftarrow> e \<and> c' = c \<and>  mem' = mem (x := (ev\<^sub>A mem e) ) \<and> det' = det"
  using assms by auto

lemma fenceD_a [dest]:
  assumes "\<langle>Fence ;;; c, L#det,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w"
  shows "\<alpha> = Fence \<and> c' = c \<and> det' = det \<and> mem' = mem"
  using assms by auto

lemma guardD_a [dest]:
  assumes "\<langle>[b?] ;;; c, L#det,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w"
  shows "\<alpha> = [b?] \<and> c' = c \<and>  det' = det \<and> mem' = mem \<and> ev\<^sub>B mem b"
  using assms by auto

lemma choice1D_a [dest]:
  assumes "\<langle>c1 ? c2, L#det,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w"
  shows "\<langle>c1, det,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w"
  using assms 
proof (elim eval\<^sub>a_elim)
  assume a: "(c1 ? c2, L # det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c', det')" "eval_action mem \<alpha> mem'" 
  hence "(c1, det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c', det')" by (blast elim: next\<^sub>a.cases)
  thus "\<langle>c1, det,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w" using a by (simp add: eval\<^sub>a.intros)
qed

lemma choice2D_a [dest]:
  assumes "\<langle>c1 ? c2, R#det,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w"
  shows "\<langle>c2, det,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w"
  using assms
proof (elim eval\<^sub>a_elim)
  assume a: "(c1 ? c2, R # det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c', det')" "eval_action mem \<alpha> mem'" 
  hence "(c2, det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c', det')" by (blast elim: next\<^sub>a.cases)
  thus "\<langle>c2, det,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w" using a by (simp add: eval\<^sub>a.intros)
qed

lemma loopD_a [dest]:
  assumes "\<langle>Loop c1 c2, U n#det,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w"
  shows "\<langle>unroll c1 n ;; c2, det,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w"
  using assms
proof (elim eval\<^sub>a_elim)
  assume a: "(Loop c1 c2, U n#det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c', det')" "eval_action mem \<alpha> mem'" 
  hence "(unroll c1 n ;; c2, det) \<rightarrow>\<^bsub>\<alpha>\<^esub> (c', det')" by (blast elim: next\<^sub>a.cases)
  thus "\<langle>unroll c1 n ;; c2, det,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w" using a by (simp add: eval\<^sub>a.intros)
qed

lemma assignD [dest]:
  assumes "\<langle>x \<leftarrow> e ;;; c, L#det,  mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', det',  mem'\<rangle>\<^sub>w"
  shows "c' = c \<and> mem' = mem ( x := (ev\<^sub>A mem e))  \<and> det' = det"
  using assms eval_iff actD_a  by blast

lemma actD [dest]:
  assumes "\<langle>a ;;; c, L#det,  mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', det',  mem'\<rangle>\<^sub>w"
  shows "c' = c \<and> det' = det \<and> eval_action mem a mem'"
  using assms eval_iff actD_a by blast

lemma eval_mem:
  assumes "\<langle>c, det,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w"
  shows "eval_action mem \<alpha> mem'"
  using assms by (induct rule: eval\<^sub>a.induct, auto)

lemma seq_assoc:
  "(c1 ;; c2) ;; c3 = c1 ;; (c2 ;; c3)"
  by (induct arbitrary: c3 rule: Seq.induct, auto)

lemma actE_a [elim]:
  assumes "\<langle>\<alpha> ;;; c, det,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<beta>\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w"
  obtains (1) "\<beta> = \<alpha> \<and> c' = c \<and> det = L#det' \<and> eval_action mem \<alpha> mem'"
  using assms 
proof (cases rule: eval\<^sub>a.cases)
  case evala
  hence "(\<alpha> ;;; c, det) \<rightarrow>\<^bsub>\<beta>\<^esub> (c', det')" by auto
  then show ?thesis
  proof (cases rule: next\<^sub>a.cases)
    case act
    then show ?thesis using 1 evala by auto
  qed
qed

lemma actE [elim]:
  assumes "\<langle>\<alpha> ;;; c, det,  mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', det',  mem'\<rangle>\<^sub>w"
  obtains (prefix)  "c' = c" "det = L#det'" "eval_action mem \<alpha> mem'" 
  using assms actE_a
proof -
  obtain \<beta> where "\<langle>\<alpha> ;;; c, det,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<beta>\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w"
    using assms eval_iff by blast 
  thus ?thesis
  proof (cases rule: actE_a)
    case 1
    then show ?thesis using prefix eval_iff by blast
  qed
qed 

lemma choiceE_a [elim]:
  assumes "\<langle>c1 ? c2, det,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<beta>\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w"
  obtains (1) "\<exists>d. (\<langle>c1, d,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<beta>\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w) \<and> det = L#d" |
          (2) "\<exists>d. (\<langle>c2, d,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<beta>\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w) \<and> det = R#d"
  using assms
proof (cases rule: eval\<^sub>a_elim)
  case 1
  then show ?thesis using that by (cases rule: next\<^sub>a.cases, auto)
qed

lemma choiceE [elim]:
  assumes "\<langle>c1 ? c2, det,  mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', det',  mem'\<rangle>\<^sub>w"
  obtains (left) "\<exists>d. (\<langle>c1, d,  mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', det',  mem'\<rangle>\<^sub>w) \<and> det = L#d" |
          (right) "\<exists>d. (\<langle>c2, d,  mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', det',  mem'\<rangle>\<^sub>w) \<and> det = R#d"
proof -
  obtain \<beta> where eval:
      "\<langle>c1 ? c2, det,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<beta>\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w"
    using assms eval_iff by blast
  thus ?thesis using left right
  proof (cases rule: choiceE_a)
    case 1
    then show ?thesis using eval left eval_iff by metis
  next
    case 2
    then show ?thesis using eval right eval_iff by metis
  qed
qed 

lemma loopE_a [elim]:
  assumes "\<langle>Loop c1 c2, det,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w"
  obtains d n where "\<langle>unroll c1 n ;; c2, d,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w" "det = U n#d" 
  using assms 
proof (cases rule: eval\<^sub>a_elim)
  case 1
  then show ?thesis using that by (cases rule: next\<^sub>a.cases, auto)
qed

lemma loopE [elim]:
  assumes "\<langle>Loop c1 c2, det,  mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', det',  mem'\<rangle>\<^sub>w"
  obtains d n where "(\<langle>unroll c1 n ;; c2, d,  mem\<rangle>\<^sub>w \<leadsto>\<^sub>w \<langle>c', det',  mem'\<rangle>\<^sub>w)" "det = U n#d" 
  by (metis assms eval_iff loopE_a)

subsection \<open> Lemmas for Program Transforms \<close>

fun flat :: "(('Local, 'Global) Var, 'AExp, 'BExp) Stmt \<Rightarrow> DetChoice list \<Rightarrow> (('Local, 'Global) Var, 'AExp, 'BExp) Stmt"
  where
  "flat (c1 ? c2) (L#d) = flat c1 d" |
  "flat (c1 ? c2) (R#d) = flat c2 d" |
  "flat (a ;;; c) (R#d) = a ;;; flat c d" |
  "flat (Loop c1 c2) (U n#d) = flat (unroll c1 n ;; c2) d" |
  "flat c d = c"

lemma eval_step:
  assumes read: "\<forall>x \<in> reads \<alpha>. mem\<^sub>1 x = mem\<^sub>2 x"
  assumes eval: "eval_action mem\<^sub>1 \<alpha> mem\<^sub>1'"
  shows "\<exists>mem\<^sub>2'. eval_action mem\<^sub>2 \<alpha> mem\<^sub>2' \<and> (\<forall>x \<in> writes \<alpha>. mem\<^sub>1' x = mem\<^sub>2' x)"
  using assms eval_vars_det\<^sub>B eval_vars_det\<^sub>A  by (cases \<alpha>; auto)

lemma eval_const:
  assumes "eval_action mem \<alpha> mem'"
  shows "\<forall>x \<in> -writes \<alpha>. mem x = mem' x"
  using assms by (metis Compl_iff eval_action.elims(2) fun_upd_apply insertI1 writes.simps(1))

lemma prog_split':
  assumes eval: "(c, det) \<rightarrow>\<^bsub>\<beta>\<^esub> (c', det')"
  obtains \<beta>' c\<^sub>1 c\<^sub>2 where "flat c det = c\<^sub>1 ;; (\<beta>' ;;; c\<^sub>2)" "c' = c\<^sub>1 ;; c\<^sub>2"
  using eval
proof (induct rule: next\<^sub>a.induct)
  case (act \<alpha> c det)
  thus ?case using flat.simps Seq.simps(1) by metis
next
  case (choice1 c\<^sub>1 det \<alpha> c\<^sub>1' det' c\<^sub>2)
  thus ?case using flat.simps by metis
next
  case (choice2 c\<^sub>2 det \<alpha> c\<^sub>2' det' c\<^sub>1)
  thus ?case using flat.simps by metis
next
  case (loop c\<^sub>1 n c\<^sub>2 det \<beta> c det')
  thus ?case using flat.simps by metis
qed

lemma prog_split:
  assumes eval: "\<langle>c, det,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<beta>\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w"
  obtains \<beta>' c\<^sub>1 c\<^sub>2 where "flat c det = c\<^sub>1 ;; (\<beta>' ;;; c\<^sub>2)" "c' = c\<^sub>1 ;; c\<^sub>2"
  by (meson eval eval\<^sub>a_elim prog_split')

subsection \<open> Lemmas for Deterministic Behaviour \<close>

lemma eval_action_det:
  assumes "eval_action mem \<alpha> mem'"
  assumes "eval_action mem \<alpha> mem''"
  shows "mem'' = mem'" using assms
  by (cases \<alpha>, auto)

lemma next\<^sub>a_det:
  assumes "(c, det) \<rightarrow>\<^bsub>\<alpha>1\<^esub> (c1, det1)"
  assumes "(c, det) \<rightarrow>\<^bsub>\<alpha>2\<^esub> (c2, det2)"
  shows "c1 = c2 \<and> det1 = det2 \<and> \<alpha>1 = \<alpha>2"
  using assms
proof (induct arbitrary: c2 det2 \<alpha>2 rule: next\<^sub>a.induct)
  case (act \<alpha> c det)
  then show ?case by (blast elim: next\<^sub>a.cases)
next
  case (choice1 c\<^sub>1 det \<alpha> c\<^sub>1' det' c\<^sub>2)
  then show ?case by (blast elim: next\<^sub>a.cases)
next
  case (choice2 c\<^sub>2 det \<alpha> c\<^sub>2' det' c\<^sub>1)
  then show ?case by (blast elim: next\<^sub>a.cases)
next
  case (loop c\<^sub>1 n c\<^sub>2 det \<beta> c det')
  then show ?case by (blast elim: next\<^sub>a.cases)
qed

lemma eval\<^sub>a_det:
  "\<langle>c, det,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<beta>1\<^esub> \<langle>c1, det1, mem1\<rangle>\<^sub>w \<Longrightarrow>
   \<langle>c, det,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<beta>2\<^esub> \<langle>c2, det2, mem2\<rangle>\<^sub>w \<Longrightarrow>
   c1 = c2 \<and> mem1 = mem2 \<and> det1 = det2 \<and> \<beta>1 = \<beta>2"
  by (metis eval\<^sub>a_elim eval_action_det next\<^sub>a_det)

lemma lc_elim:
  obtains c det env mem where "lc = \<langle>c, det,  mem\<rangle>\<^sub>w"
  by (case_tac lc; auto)

lemma eval\<^sub>w_det:
  "lc \<leadsto>\<^sub>w lc1 \<Longrightarrow> lc \<leadsto>\<^sub>w lc2 \<Longrightarrow> lc1 = lc2"
  apply (cases lc rule: lc_elim; cases lc1 rule: lc_elim ; cases lc2 rule: lc_elim)
  using eval\<^sub>a_det eval_iff by (metis (no_types, lifting))

lemma parallel_eval:
  assumes "\<langle>c, det,  mem\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det',  mem'\<rangle>\<^sub>w"
  assumes "eval_action mem\<^sub>2 \<alpha> mem\<^sub>2'"
  shows "\<langle>c, det,  mem\<^sub>2\<rangle>\<^sub>w \<leadsto>\<^bsub>\<alpha>\<^esub> \<langle>c', det',  mem\<^sub>2'\<rangle>\<^sub>w"
  using assms(1)
proof (cases rule: eval\<^sub>a.cases)
  case evala
  then show ?thesis using assms(2) by (simp add: eval\<^sub>a.intros)
qed

end

end