# Rely/Guarantee Reasoning for Noninterference in Non-Blocking Algorithms

This project contains the Isabelle/HOL formalisation and soundness proof for [Rely/Guarantee Reasoning for Noninterference in Non-Blocking Algorithms](https://www.semanticscholar.org/paper/Rely%2FGuarantee-Reasoning-for-Noninterference-in-Coughlin-Smith/adbddc422a936cc9acc2d6cb28eefee4581357ed).
This work is loosely based on a formalisation of [Covern](http://covern.org).

## Formalisation Details

The formalisation defines an information flow logic capable of handling arbitrary rely/guarantee specifications for each thread.
Consequently, it is capable of demonstrating noninterference in the presence of data races, as seen in the Linux kernel's seqlock mechanism.
Rules are defined over a basic While language, with a distinction drawn between a global and local context to simplify stability proofs.
The logic's assertions are defined via a deeply embedded predicate language, capable of tracking the historic state of the program to simplify updates due to the rely specification.
See the paper for the full details.

## Project Structure  
* Preliminaries.thy: Security classification definitions, inherited from **Covern**
* Security.thy: Security properties demonstrated by the logic and their composition, based on the rely/guarantee parallel rule
* PredicateLanguage.thy: Deeply embedded predicate language used for the logic's assertions
* Language.thy: Basic While language definition
* TypeSystem.thy: Logic rules and soundness proof
