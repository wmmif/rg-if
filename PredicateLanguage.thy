theory PredicateLanguage
  imports Main
begin

subsection \<open> Syntax \<close>

type_synonym ('var, 'val) Mem = "'var \<Rightarrow> 'val"
type_synonym ('var, 'val) His = "nat \<Rightarrow> 'var \<Rightarrow> 'val"

datatype ('var,'val) lexp =
  Var "'var"
  | Var' "'var" "nat"
  | Const "'val"
  | LBinOp "('val \<Rightarrow> 'val \<Rightarrow> 'val)" "('var,'val) lexp" "('var,'val) lexp"

datatype ('var,'val) lpred = 
  PTrue 
  | PFalse 
  | PDisj "('var,'val) lpred" "('var,'val) lpred" (infixr "\<or>\<^sub>p" 30)
  | PConj "('var,'val) lpred" "('var,'val) lpred" (infixr "\<and>\<^sub>p" 35)
  | PNeg "('var,'val) lpred"
  | PImp "('var,'val) lpred" "('var,'val) lpred"
  | PCmp "('val \<Rightarrow> 'val \<Rightarrow> bool)" "('var,'val) lexp" "('var,'val) lexp"
  | PEx "('val \<Rightarrow> ('var,'val) lpred)"
  | PAll "('val \<Rightarrow> ('var,'val) lpred)"

subsection \<open> Evaluation Semantics \<close>

primrec
  eval\<^sub>e :: "('var,'val) Mem \<Rightarrow> ('var,'val) His \<Rightarrow> ('var,'val) lexp \<Rightarrow> 'val"
where
  "eval\<^sub>e mem h (Var v) = mem v" |
  "eval\<^sub>e mem h (Const c) = c" |
  "eval\<^sub>e mem h (LBinOp bop e f) = (bop (eval\<^sub>e mem h e) (eval\<^sub>e mem h f))" |
  "eval\<^sub>e mem h (Var' v n) = h n v"

primrec 
  eval\<^sub>p :: "('var,'val) Mem \<Rightarrow> ('var,'val) His \<Rightarrow> ('var,'val) lpred  \<Rightarrow> bool"
where
  "eval\<^sub>p mem h PTrue = True" |
  "eval\<^sub>p mem h PFalse = False" |
  "eval\<^sub>p mem h (PDisj p q) = ((eval\<^sub>p mem h p) \<or> (eval\<^sub>p mem h q))" |
  "eval\<^sub>p mem h (PConj p q) = ((eval\<^sub>p mem h p) \<and> (eval\<^sub>p mem h q))" |
  "eval\<^sub>p mem h (PNeg p) = (\<not> (eval\<^sub>p mem h p))" |
  "eval\<^sub>p mem h (PImp p q) = ((eval\<^sub>p mem h p) \<longrightarrow> (eval\<^sub>p mem h q))" |
  "eval\<^sub>p mem h (PCmp cmp e f) = (cmp (eval\<^sub>e mem h e) (eval\<^sub>e mem h f))" |
  "eval\<^sub>p mem h (PEx fe) = (\<exists>v. eval\<^sub>p mem h (fe v))" |
  "eval\<^sub>p mem h (PAll fe) = (\<forall>v. eval\<^sub>p mem h (fe v))"

subsection \<open> Evaluation Semantics \<close>

primrec
  vars\<^sub>e :: "('var,'val) lexp \<Rightarrow> 'var set"
where
  "vars\<^sub>e (Var v) = { v }" |
  "vars\<^sub>e (Var' v n) = {}" |
  "vars\<^sub>e (Const c) = {}" |
  "vars\<^sub>e (LBinOp bop e f) = (vars\<^sub>e e \<union> vars\<^sub>e f)"

primrec
  vars'\<^sub>e :: "('var,'val) lexp \<Rightarrow> ('var\<times>nat) set"
where
  "vars'\<^sub>e (Var v) = {}" |
  "vars'\<^sub>e (Var' v n) = {(v,n)}" |
  "vars'\<^sub>e (Const c) = {}" |
  "vars'\<^sub>e (LBinOp bop e f) = (vars'\<^sub>e e \<union> vars'\<^sub>e f)"

primrec
  vars\<^sub>p :: "('var,'val) lpred \<Rightarrow> 'var set"
where
  "vars\<^sub>p PTrue = {}" |
  "vars\<^sub>p PFalse = {}"  |
  "vars\<^sub>p (PNeg p) = vars\<^sub>p p" |
  "vars\<^sub>p (PImp p q) = (vars\<^sub>p p \<union> vars\<^sub>p q)" |
  "vars\<^sub>p (PConj p q) = (vars\<^sub>p p \<union> vars\<^sub>p q)" |
  "vars\<^sub>p (PDisj p q) = (vars\<^sub>p p \<union> vars\<^sub>p q)" |
  "vars\<^sub>p (PCmp cmp e f) = (vars\<^sub>e e \<union> vars\<^sub>e f)" |
  "vars\<^sub>p (PEx fe) = (\<Union>v. vars\<^sub>p (fe v))" |
  "vars\<^sub>p (PAll fe) = (\<Union>v. vars\<^sub>p (fe v))"

primrec
  vars'\<^sub>p :: "('var,'val) lpred \<Rightarrow> ('var\<times>nat) set"
where
  "vars'\<^sub>p PTrue = {}" |
  "vars'\<^sub>p PFalse = {}"  |
  "vars'\<^sub>p (PNeg p) = vars'\<^sub>p p" |
  "vars'\<^sub>p (PImp p q) = (vars'\<^sub>p p \<union> vars'\<^sub>p q)" |
  "vars'\<^sub>p (PConj p q) = (vars'\<^sub>p p \<union> vars'\<^sub>p q)" |
  "vars'\<^sub>p (PDisj p q) = (vars'\<^sub>p p \<union> vars'\<^sub>p q)" |
  "vars'\<^sub>p (PCmp cmp e f) = (vars'\<^sub>e e \<union> vars'\<^sub>e f)" |
  "vars'\<^sub>p (PEx fe) = (\<Union>v. vars'\<^sub>p (fe v))" |
  "vars'\<^sub>p (PAll fe) = (\<Union>v. vars'\<^sub>p (fe v))"

text \<open> substitution as in y[x\<mapsto>e] \<close> 
primrec 
  subst\<^sub>e :: "('Var,'Val) lexp \<Rightarrow> ('Var,'Val) lexp \<Rightarrow> 'Var \<Rightarrow> ('Var,'Val) lexp"
where
  "subst\<^sub>e (Var y) e x = (if x = y then e else Var y)" |   
  "subst\<^sub>e (Const c) e x = Const c" |
  "subst\<^sub>e (Var' v n) e x = Var' v n" |
  "subst\<^sub>e (LBinOp opf f f') e x = (LBinOp opf (subst\<^sub>e f e x) (subst\<^sub>e f' e x))"

primrec 
  subst\<^sub>p :: "('Var,'Val) lpred \<Rightarrow> ('Var,'Val) lexp \<Rightarrow> 'Var \<Rightarrow> ('Var,'Val) lpred"
where
  "subst\<^sub>p PTrue e x = PTrue" |
  "subst\<^sub>p PFalse e x = PFalse" |
  "subst\<^sub>p (PConj P Q) e x = (PConj (subst\<^sub>p P e x) (subst\<^sub>p Q e x))" |
  "subst\<^sub>p (PDisj P Q) e x = (PDisj (subst\<^sub>p P e x) (subst\<^sub>p Q e x))" |
  "subst\<^sub>p (PImp P Q) e x = (PImp (subst\<^sub>p P e x) (subst\<^sub>p Q e x))" |
  "subst\<^sub>p (PNeg P) e x = (PNeg (subst\<^sub>p P e x))" |
  "subst\<^sub>p (PCmp cmp f f') e x = (PCmp cmp (subst\<^sub>e f e x) (subst\<^sub>e f' e x))" |
  "subst\<^sub>p (PAll fP) e x = (PAll (\<lambda>x'. subst\<^sub>p (fP x') e x))" |
  "subst\<^sub>p (PEx fP) e x = (PEx (\<lambda>x'. subst\<^sub>p (fP x') e x))"


text \<open> prime variables in expr e that are contained in set V, ie., give those a subscript like v_n \<close>
primrec 
  prime\<^sub>e :: "('Var,'Val) lexp \<Rightarrow> 'Var set \<Rightarrow> ('Var,'Val) lexp"
where
  "prime\<^sub>e (Var y) V = (if y \<in> V then (Var' y 0) else (Var y))" |
  "prime\<^sub>e (Const c) V = (Const c)" |
  "prime\<^sub>e (Var' y n) V = (if y \<in> V then (Var' y (Suc n)) else (Var' y n))" |
  "prime\<^sub>e (LBinOp op f f') V = (LBinOp op (prime\<^sub>e f V) (prime\<^sub>e f' V))"

primrec 
  prime\<^sub>p :: "('Var,'Val) lpred \<Rightarrow> 'Var set \<Rightarrow> ('Var,'Val) lpred"
where
  "prime\<^sub>p PTrue V = PTrue" |
  "prime\<^sub>p PFalse V = PFalse" |
  "prime\<^sub>p (PConj P Q) x = PConj (prime\<^sub>p P x) (prime\<^sub>p Q x)" |
  "prime\<^sub>p (PDisj P Q) x = PDisj (prime\<^sub>p P x) (prime\<^sub>p Q x)" |
  "prime\<^sub>p (PImp P Q) x = PImp (prime\<^sub>p P x) (prime\<^sub>p Q x)" |
  "prime\<^sub>p (PNeg P) x = PNeg (prime\<^sub>p P x)" |
  "prime\<^sub>p (PCmp cmp f f') x = PCmp cmp (prime\<^sub>e f x) (prime\<^sub>e f' x)" |
  "prime\<^sub>p (PAll f) x = PAll (\<lambda>v. (prime\<^sub>p (f v) x))" |
  "prime\<^sub>p (PEx f) x = PEx (\<lambda>v. (prime\<^sub>p (f v) x))"

text \<open> exp declaration \<close> 

typedef ('Var,'Val) exp = "{P. vars'\<^sub>e (P:: ('Var,'Val) lexp) = {}}"
proof -
  have "\<exists>c. vars'\<^sub>e (Const c) = {}" by auto
  thus ?thesis by fast
qed

text \<open> lifted semantics \<close> 

setup_lifting type_definition_exp

lift_definition lift_Const :: "'Val \<Rightarrow> ('Var, 'Val) exp"
is "\<lambda>c. Const c" by simp

lift_definition lift_Var :: "'Var \<Rightarrow> ('Var, 'Val) exp"
is "\<lambda>c. Var c" by simp

lift_definition lift_LBinOp :: "('val \<Rightarrow> 'val \<Rightarrow> 'val) \<Rightarrow> ('var,'val) exp \<Rightarrow> ('var,'val) exp \<Rightarrow> ('var,'val) exp"
is "\<lambda>opf f f'. LBinOp opf f f'" by simp

lift_definition evale :: "('Var, 'Val) Mem \<Rightarrow> ('Var, 'Val) His \<Rightarrow> ('Var, 'Val) exp \<Rightarrow> 'Val" 
is "\<lambda>m h e. eval\<^sub>e m h e"
  done

lift_definition varse :: "('Var, 'Val) exp \<Rightarrow> 'Var set" is "\<lambda>e. vars\<^sub>e e" 
  done

lift_definition varse' :: "('Var,'Val) exp \<Rightarrow> ('Var\<times>nat) set" is "\<lambda>e. vars'\<^sub>e e"
  done


lemma subst\<^sub>e_vars' [simp]:
  shows "vars'\<^sub>e (subst\<^sub>e f v x) = vars'\<^sub>e f \<union> (if x \<in> vars\<^sub>e f then vars'\<^sub>e v else {})"
  by (induct f, auto)

lift_definition subste :: "('Var,'Val) exp \<Rightarrow> ('Var,'Val) exp \<Rightarrow> 'Var \<Rightarrow> ('Var,'Val) exp" 
is "\<lambda>e1 e2 Var. subst\<^sub>e e1 e2 Var" by simp


text \<open> pred declaration \<close> 

typedef ('Var,'Val) pred = "{P. vars'\<^sub>p (P:: ('Var,'Val) lpred) = {}}"
proof -
  have "vars'\<^sub>p PTrue = {}" by auto
  thus ?thesis by blast
qed

text \<open> lifted semantics \<close> 

setup_lifting type_definition_pred

lift_definition lift_PConj :: "('Var, 'Val) pred \<Rightarrow> ('Var, 'Val) pred \<Rightarrow> ('Var, 'Val) pred" 
is "\<lambda>P Q. (PConj P Q)" by simp

lift_definition lift_PEx :: "('Val \<Rightarrow> ('Var,'Val) pred) \<Rightarrow> ('Var, 'Val) pred" 
is "\<lambda>P. (PEx P)" by simp

lift_definition lift_PNeg :: "('Var,'Val) pred \<Rightarrow> ('Var, 'Val) pred" 
is "\<lambda>P. (PNeg P)" by simp

lift_definition lift_PDisj :: "('Var,'Val) pred \<Rightarrow> ('Var,'Val) pred \<Rightarrow> ('Var,'Val) pred"
is "\<lambda>P Q. (PDisj P Q)" by simp

lift_definition lift_PCmp :: "('Val \<Rightarrow> 'Val \<Rightarrow> bool) \<Rightarrow> ('Var,'Val) exp \<Rightarrow> ('Var,'Val) exp \<Rightarrow> ('Var,'Val) pred"
is "\<lambda>x E1 E2. (PCmp x E1 E2)" by simp

lift_definition lift_PImp :: "('var,'val) pred \<Rightarrow> ('var,'val) pred \<Rightarrow> ('var,'val) pred"
is "\<lambda>P Q. (PImp P Q)" by simp 

lift_definition evalp :: "('Var, 'Val) Mem \<Rightarrow> ('Var, 'Val) His \<Rightarrow> ('Var, 'Val) pred \<Rightarrow> bool" 
is "\<lambda>m h P. eval\<^sub>p m h P"
  done

lift_definition varsp :: "('Var, 'Val) pred \<Rightarrow> 'Var set" is "\<lambda>e. vars\<^sub>p e"
  done

lift_definition varsp' :: "('var,'val) pred \<Rightarrow> ('var\<times>nat) set" is "\<lambda>e. vars'\<^sub>p e"
  done


lemma subst\<^sub>p_vars' [simp]:
  shows "vars'\<^sub>p (subst\<^sub>p P v x) = vars'\<^sub>p P \<union> (if x \<in> vars\<^sub>p P then vars'\<^sub>e v else {})"
  by (induct P, auto split: if_splits)

lift_definition substp :: "('Var,'Val) pred \<Rightarrow> ('Var,'Val) exp \<Rightarrow> 'Var \<Rightarrow> ('Var,'Val) pred" 
is "\<lambda>p e Var. subst\<^sub>p p e Var" by simp


lemma varsp'_true: 
  "varsp' (Abs_pred PTrue) = {}"
  using Rep_pred varsp'.rep_eq by auto

lemma evalp_current:
  "\<forall>mem. evalp mem (\<lambda>n. mem) (Abs_pred PTrue)" 
  by (simp add: eq_onp_same_args evalp.abs_eq)

lemma evalp_history:
  "\<forall>mem mem'. evalp mem mem' (Abs_pred PTrue)" 
  by (simp add: eq_onp_same_args evalp.abs_eq)

lemma liftAbs [simp]:  
  assumes "vars'\<^sub>p(p) = {}"
  shows "Rep_pred(Abs_pred(p)) = p" 
  by (simp add: Abs_pred_inverse Rep_exp assms) 

lemma vars\<^sub>p_empty:
  "vars'\<^sub>p(PCmp x1 x2 x3) = {} \<longleftrightarrow> (vars'\<^sub>e(x2) = {} \<and> vars'\<^sub>e(x3) = {})" by simp

lemma varsp_empty:
  "varsp'(lift_PCmp x1 x2 x3) = {} \<longleftrightarrow> (varse'(x2) = {} \<and> varse'(x3) = {})" 
  by (simp add: varse'_def varsp'_def lift_PCmp.rep_eq)


text \<open> R as a relational predicate which is transitive and reflexive \<close>
text \<open> HOL typedefs require a non-emptyness proof \<close>
typedef ('var,'val) rpred = "{P. (\<forall>mem mem' mem''. eval\<^sub>p mem (\<lambda>n. mem') P \<and> eval\<^sub>p mem' mem'' P \<longrightarrow> eval\<^sub>p mem mem'' P) 
                     \<and> (\<forall>mem. eval\<^sub>p mem (\<lambda>n. mem) P) \<and> vars'\<^sub>p (P :: ('var,'val) lpred) \<subseteq> {(x,0)|x. True}}"
proof -
  have "vars'\<^sub>p PTrue = {}" by auto
  moreover have "\<forall>mem. eval\<^sub>p mem (\<lambda>n. mem) PTrue" by auto
  moreover have "\<forall>mem mem'. eval\<^sub>p mem mem' PTrue" by auto
  ultimately show ?thesis by blast
qed

setup_lifting type_definition_rpred


text \<open> G as a relational predicate which is reflexive \<close>
typedef ('Var,'Val) gpred = "{P. vars'\<^sub>p (P:: ('Var,'Val) lpred) \<subseteq> {(x,0)|x. True}}"
proof -
  have "vars'\<^sub>p PTrue = {}" by auto
  thus ?thesis by blast
qed

setup_lifting type_definition_gpred


definition eval\<^sub>r :: "('Var,'Val) Mem \<Rightarrow> ('Var,'Val) Mem \<Rightarrow> ('Var,'Val) lpred \<Rightarrow> bool" 
  where "eval\<^sub>r mem mem' P \<equiv> eval\<^sub>p mem' (\<lambda>n. mem) (P)"

definition eval\<^sub>g :: "('Var,'Val) Mem \<Rightarrow> ('Var,'Val) Mem \<Rightarrow> ('Var,'Val) lpred \<Rightarrow> bool" 
  where "eval\<^sub>g mem mem' P \<equiv> eval\<^sub>p mem' (\<lambda>n. mem) (P)"

lift_definition evalr :: "('Var,'Val) Mem \<Rightarrow> ('Var,'Val) Mem \<Rightarrow> ('Var,'Val) rpred \<Rightarrow> bool" 
is "\<lambda>m h P. eval\<^sub>p h (\<lambda>n. m) (Rep_rpred P)"
  done

lift_definition evalg :: "('Var,'Val) Mem \<Rightarrow> ('Var,'Val) Mem \<Rightarrow> ('Var,'Val) gpred \<Rightarrow> bool" 
is "\<lambda>m h P. eval\<^sub>p h (\<lambda>n. m) (Rep_gpred P)"
  done


subsubsection \<open> Entailment Definition \<close>

definition entail :: "('Var,'Val) lpred \<Rightarrow> ('Var,'Val) lpred \<Rightarrow> bool"
  (infix "\<turnstile>" 50)
  where "entail P P' \<equiv> \<forall>mem h. eval\<^sub>p mem h P \<longrightarrow> eval\<^sub>p mem h P'"

subsubsection \<open> Equivalent Definition \<close>

definition equiv :: "('Var,'Val) lpred \<Rightarrow> ('Var,'Val) lpred \<Rightarrow> bool"
  (infix "=\<^sub>P" 50)
  where "equiv P P' \<equiv> \<forall>mem h. eval\<^sub>p mem h P = eval\<^sub>p mem h P'"


subsection \<open> Locale \<close>

locale predicate_lang =
  fixes local_type_constraint:: "'Var::type \<times> 'Val::type"

context predicate_lang
begin

lemma leval_vars_det:
  "\<forall>v \<in> vars\<^sub>e e. mem v = mem' v \<Longrightarrow> \<forall>h. eval\<^sub>e mem h e = eval\<^sub>e mem' h e"
  by (induct e, auto)

lemma leval_varse_det:
  "\<forall>v \<in> varse e. mem v = mem' v \<Longrightarrow> \<forall>h. evale mem h e = evale mem' h e"
  by (simp add: leval_vars_det evale.rep_eq varse.rep_eq)

lemma lpred_eval_vars_det:
  "\<forall>v \<in> vars\<^sub>p P. mem v = mem' v \<Longrightarrow>  \<forall>h. eval\<^sub>p mem h P = eval\<^sub>p mem' h P"
  by (induct P, (auto | metis leval_vars_det UnCI)+)

lemma lpred_eval_varsp_det:
  "\<forall>v \<in> varsp P. mem v = mem' v \<Longrightarrow>  \<forall>h. evalp mem h P = evalp mem' h P"
  by (simp add: evalp.rep_eq lpred_eval_vars_det varsp.rep_eq)


subsubsection \<open> Entailment Definitions \<close>

abbreviation entail_neg :: "('Var,'Val) lpred \<Rightarrow> ('Var,'Val) lpred \<Rightarrow> bool"
  (infix "!\<turnstile>" 50)
  where "P !\<turnstile> t \<equiv> P \<turnstile> (PNeg t)"


subsubsection \<open> Entailment Properties \<close>

lemma entail_refl [simp]:
  "P \<turnstile> P"
  by (simp add: entail_def)

lemma entail_trans [trans]:
  "P \<turnstile> P' \<Longrightarrow> P' \<turnstile> P'' \<Longrightarrow> P \<turnstile> P''"
  by (simp add: entail_def)

lemma entail_PConj:
  "P \<turnstile> P' \<Longrightarrow> P \<turnstile> P'' \<Longrightarrow> P \<turnstile> (PConj P' P'')"
  by (simp add: entail_def)

lemma entail_mono_PConj:
  "P \<turnstile> P' \<Longrightarrow> P \<turnstile> P'' \<Longrightarrow>  (PConj P P'') \<turnstile> (PConj P' P'')"
  by (simp add: entail_def)

lemma PConj_entail_mono:
  "(PConj P P') \<turnstile> P'' \<Longrightarrow> P''' \<turnstile> P \<Longrightarrow> (PConj P''' P') \<turnstile> P''"
  by (simp add: entail_def)

subsubsection \<open> Equiv Properties \<close>

lemma equiv_refl [simp]:
  shows "P =\<^sub>P P"
  unfolding equiv_def by auto

lemma equiv_trans [trans]:
  shows "P1 =\<^sub>P P2 \<Longrightarrow> P2 =\<^sub>P P3 \<Longrightarrow> P1 =\<^sub>P P3"
  unfolding equiv_def by auto

lemma pred_equiv_entail:
  shows "P =\<^sub>P P' = (P \<turnstile> P' \<and> P' \<turnstile> P)"
  unfolding equiv_def entail_def by auto

lemma pred_equiv_sym:
  shows "P1 =\<^sub>P P2 \<Longrightarrow> P2 =\<^sub>P P1"
  unfolding equiv_def by auto

lemma pred_equiv_common:
  shows "P =\<^sub>P P' \<Longrightarrow> P \<turnstile> t = (P' \<turnstile> t)"
  unfolding equiv_def entail_def by auto

lemma pred_equiv_common_inv:
  shows "P =\<^sub>P P' \<Longrightarrow> P !\<turnstile> t = (P' !\<turnstile> t)"
  unfolding equiv_def entail_def by auto


lemma PEx_nop [simp]:
  shows "PEx (\<lambda>x'. P) =\<^sub>P P"
  by (simp add: equiv_def)

lemma PEx_swap [simp]:
  "(Rep_pred (lift_PEx (\<lambda>x1. lift_PEx (\<lambda>x2. F x1 x2)))) =\<^sub>P (Rep_pred (lift_PEx (\<lambda>x2. lift_PEx (\<lambda>x1. F x2 x1))))"
  unfolding equiv_def entail_def by simp

lemma ex_equiv:
  assumes "(\<forall>x. P x =\<^sub>P P' x)"
  shows "(\<forall>x. P x =\<^sub>P P' x) \<Longrightarrow> (PEx P) =\<^sub>P (PEx P')"
  unfolding equiv_def by simp 

lemma conj_equiv:
  assumes "P =\<^sub>P P'"
  shows "(PConj P B) =\<^sub>P (PConj P' B)"
  using assms unfolding equiv_def by simp

lemma ex_entail:
  assumes "\<forall>x. P x \<turnstile> P' x"
  shows "(PEx (\<lambda>x. (P x))) \<turnstile> (PEx (\<lambda>x. (P' x)))"
  using assms unfolding entail_def by auto

lemma pred_fold_PConj:
  "eval\<^sub>p mem h (fold PConj Ps Q) \<Longrightarrow> (\<forall>P \<in> set Ps. eval\<^sub>p mem h P) \<and> eval\<^sub>p mem h Q"
  by (induct Ps arbitrary: Q , clarsimp; metis eval\<^sub>p.simps(4) fold_simps(2)  set_ConsD)

lemma pred_fold_PConj':
  "(\<forall>P \<in> set Ps. eval\<^sub>p mem h P) \<and> eval\<^sub>p mem h Q \<Longrightarrow> eval\<^sub>p mem h (fold PConj Ps Q)"
  by (induct Ps arbitrary: Q, auto)


subsubsection \<open> lexp Substitution \<close>

lemma subst\<^sub>e_mem_upd:
  assumes "(\<And>y. y \<noteq> x \<Longrightarrow> mem' y = mem y)"
  assumes "eval\<^sub>e mem' h e = mem x"
  shows "eval\<^sub>e mem' h (subst\<^sub>e f e x) = eval\<^sub>e mem h f"
  using assms by (induct f, auto)

lemma subste_mem_upd:
  assumes "(\<And>y. y \<noteq> x \<Longrightarrow> mem' y = mem y)"
  assumes "evale mem' h e = mem x"
  shows "evale mem' h (subste f e x) = evale mem h f"
  using assms by (simp add: evale.rep_eq subst\<^sub>e_mem_upd subste.rep_eq)

lemma subst\<^sub>e_nop [simp]:
  assumes "x \<notin> vars\<^sub>e f"
  shows "subst\<^sub>e f v x = f"
  using assms by (induct f, auto)

lemma subste_nop [simp]:
  assumes "x \<notin> varse f"
  shows "subste f v x = f"
  using assms by (metis Rep_exp_inject subst\<^sub>e_nop subste.rep_eq varse.rep_eq)

lemma subst\<^sub>e_vars [simp]:
  shows "vars\<^sub>e (subst\<^sub>e f v x) = vars\<^sub>e f - {x} \<union> (if x \<in> vars\<^sub>e f then vars\<^sub>e v else {})"
  by (induct f, auto)

lemma subste_vars [simp]:
  shows "varse (subste f v x) = varse f - {x} \<union> (if x \<in> varse f then varse v else {})"
  by (metis subst\<^sub>e_vars subste.rep_eq varse.rep_eq)

lemma subst\<^sub>e_comm_diff:
  assumes "x \<noteq> y"
  assumes "x \<notin> vars\<^sub>e v1"
  assumes "y \<notin> vars\<^sub>e v2"
  shows "(subst\<^sub>e (subst\<^sub>e f v1 y) v2 x) = (subst\<^sub>e (subst\<^sub>e f v2 x) v1 y)"
  using assms by (induct f, auto)

lemma subste_comm_diff:
assumes "x \<noteq> y"
  assumes "x \<notin> varse v1"
  assumes "y \<notin> varse v2"
  shows "(subste (subste f v1 y) v2 x) = (subste (subste f v2 x) v1 y)"
  using assms by (metis Rep_exp_inject subst\<^sub>e_comm_diff subste.rep_eq varse.rep_eq)

lemma subst\<^sub>e_comm_same [simp]:
  "subst\<^sub>e (subst\<^sub>e f v1 x) v2 x = subst\<^sub>e f (subst\<^sub>e v1 v2 x) x" 
  by (induct f, auto)

lemma subste_comm_same [simp]:
  "subste (subste f v1 x) v2 x = subste f (subste v1 v2 x) x" 
  by (metis Rep_exp_inject subst\<^sub>e_comm_same subste.rep_eq)

subsubsection \<open> lpred Substitution \<close>

lemma subst\<^sub>p_mem_upd:
  assumes "(\<And>y. y \<noteq> x \<Longrightarrow> mem' y = mem y)"
  assumes "eval\<^sub>e mem' h e = mem x"
  shows "eval\<^sub>p mem' h (subst\<^sub>p P e x) = eval\<^sub>p mem h P"
  using assms by (induct P, auto simp: subst\<^sub>e_mem_upd)

lemma substp_mem_upd:
  assumes "(\<And>y. y \<noteq> x \<Longrightarrow> mem' y = mem y)"
  assumes "evale mem' h e = mem x"
  shows "evalp mem' h (substp P e x) = evalp mem h P"
  using assms by (simp add: evale.rep_eq evalp.rep_eq subst\<^sub>p_mem_upd substp.rep_eq)

lemma subst\<^sub>p_nop [simp]:
  shows "x \<notin> vars\<^sub>p P \<Longrightarrow> subst\<^sub>p P v x = P"
  by (induct P, auto)

lemma substp_nop [simp]:
  shows "x \<notin> varsp P \<Longrightarrow> substp P v x = P"
  by (metis Rep_pred_inject subst\<^sub>p_nop substp.rep_eq varsp.rep_eq)

lemma subst\<^sub>p_vars [simp]:
  shows "vars\<^sub>p (subst\<^sub>p P v x) = vars\<^sub>p P - {x} \<union> (if x \<in> vars\<^sub>p P then vars\<^sub>e v else {})"
  by (induct P, auto split: if_splits)

lemma substp_vars [simp]:
  shows "varsp (substp P v x) = varsp P - {x} \<union> (if x \<in> varsp P then varse v else {})"
  by (metis subst\<^sub>p_vars substp.rep_eq varse.rep_eq varsp.rep_eq)

lemma subst\<^sub>p_comm_diff:
  assumes "x \<noteq> y"
  assumes "x \<notin> vars\<^sub>e v1"
  assumes "y \<notin> vars\<^sub>e v2"
  shows "(subst\<^sub>p (subst\<^sub>p P v1 y) v2 x) = (subst\<^sub>p (subst\<^sub>p P v2 x) v1 y)"
  using assms by (induct P, auto simp: subst\<^sub>e_comm_diff)

lemma substp_comm_diff:
  assumes "x \<noteq> y"
  assumes "x \<notin> varse v1"
  assumes "y \<notin> varse v2"
  shows "(substp (substp P v1 y) v2 x) = (substp (substp P v2 x) v1 y)"
  using assms by (metis Rep_pred_inject subst\<^sub>p_comm_diff substp.rep_eq varse.rep_eq)

lemma subst\<^sub>p_fold [simp]:
  shows "subst\<^sub>p (subst\<^sub>p P (Const v1) x) (Const v2) x = subst\<^sub>p P (Const v1) x"
  by (induct P, auto)

lemma substp_fold [simp]:
  shows "substp (substp P (lift_Const v1) x) (lift_Const v2) x = substp P (lift_Const v1) x"
  by (metis Rep_pred_inject lift_Const.rep_eq subst\<^sub>p_fold substp.rep_eq)

text \<open> Entailment is preserved across substitution \<close>
lemma subst\<^sub>p_entailment:
  shows "P \<turnstile> P' \<Longrightarrow> (subst\<^sub>p P (Const v) x) \<turnstile> (subst\<^sub>p P' (Const v) x)"
  unfolding entail_def
proof clarify
  fix mem h
  assume "\<forall>mem h. eval\<^sub>p mem h P \<longrightarrow> eval\<^sub>p mem h P'"
  moreover assume "eval\<^sub>p mem h (subst\<^sub>p P (Const v) x)"
  ultimately show "eval\<^sub>p mem h (subst\<^sub>p P' (Const v) x)"
    using subst\<^sub>p_mem_upd [of x mem "mem (x := v)"]
    by simp
qed
end


subsubsection \<open> Primed Variables \<close>

lemma leval_vars'_det:
  "\<forall>v \<in> vars'\<^sub>e e. h (snd v) (fst v) = h' (snd v) (fst v) \<Longrightarrow> eval\<^sub>e mem h e = eval\<^sub>e mem h' e"
  by (induct e, auto)

lemma leval_varse'_det:
  "\<forall>v \<in> varse' e. h (snd v) (fst v) = h' (snd v) (fst v) \<Longrightarrow> evale mem h e = evale mem h' e"
  by (simp add: evale.rep_eq leval_vars'_det varse'.rep_eq)

lemma lpred_eval_vars'_det:
  "\<forall>v \<in> vars'\<^sub>p P. h (snd v) (fst v) = h' (snd v) (fst v) \<Longrightarrow>  eval\<^sub>p mem h P = eval\<^sub>p mem h' P"
  by (induct P, (auto | metis leval_vars'_det UnCI)+)

lemma lpred_eval_varsp'_det:
  "\<forall>v \<in> varsp' P. h (snd v) (fst v) = h' (snd v) (fst v) \<Longrightarrow>  evalp mem h P = evalp mem h' P"
  by (simp add: evalp_def varsp'_def lpred_eval_vars'_det)

definition 
  assign :: "'Var \<Rightarrow> (_,_) lexp \<Rightarrow> (_,_) lpred \<Rightarrow> (_,_) lpred"
where
  "assign x e P = (PConj (prime\<^sub>p P {x}) (PCmp (=) (Var x) (prime\<^sub>e e {x})))"


definition 
  assign_lift :: "'Var \<Rightarrow> (_,_) exp \<Rightarrow> (_,_) pred \<Rightarrow> (_,_) pred"
where
  "assign_lift x e P = (lift_PConj (Abs_pred (prime\<^sub>p (Rep_pred P) {x})) 
    (lift_PCmp (=) (Abs_exp (Var x)) (Abs_exp (prime\<^sub>e (Rep_exp e) {x}))))"

definition 
  h_upd :: "('Var,'Val) His \<Rightarrow> ('Var,'Val) Mem \<Rightarrow> 'Var set \<Rightarrow> ('Var,'Val) His"
where
  "h_upd h mem V \<equiv> \<lambda>n x. if x \<in> V then (case n of 0 \<Rightarrow> mem x | Suc n' \<Rightarrow> h n' x) else h n x"

lemma h_upd_nop [simp]:
  shows "h_upd h mem' {} = h"
  by (auto simp: h_upd_def)

lemma prime_eval_lexp [simp]:
  assumes "\<forall>x \<in> - V. mem' x = mem x"
  shows "eval\<^sub>e mem' (h_upd h mem V) (prime\<^sub>e f V) = eval\<^sub>e mem h f"
  using assms by (induct f) (auto simp: h_upd_def)

lemma prime_eval_lpred [simp]:
  assumes "\<forall>x \<in> - V. mem' x = mem x"
  shows "eval\<^sub>p mem' (h_upd h mem V) (prime\<^sub>p P V) = eval\<^sub>p mem h P"
  using assms by (induct P) auto

lemma assign_eval [simp]:
  shows "eval\<^sub>p (mem(x := eval\<^sub>e mem h e)) (h_upd h mem {x}) (assign x e P) = eval\<^sub>p mem h P"
  by (auto simp: assign_def)

lemma eval\<^sub>e_no_prime:
  assumes "vars'\<^sub>e f  = {}"
  shows "\<forall>h\<^sub>1 h\<^sub>2. eval\<^sub>e mem h\<^sub>1 f = eval\<^sub>e mem h\<^sub>2 f"
  using assms 
proof (induct f)
  case (LBinOp c f1 f2)
  hence 
      "\<forall>h\<^sub>1 h\<^sub>2. eval\<^sub>e mem h\<^sub>1 f1 = eval\<^sub>e mem h\<^sub>2 f1" 
      "\<forall>h\<^sub>1 h\<^sub>2. eval\<^sub>e mem h\<^sub>1 f2 = eval\<^sub>e mem h\<^sub>2 f2"
    by auto
  then show ?case by (metis eval\<^sub>e.simps(3))
qed auto

lemma evale_no_prime:
  assumes "varse' f  = {}"
  shows "\<forall>h\<^sub>1 h\<^sub>2. evale mem h\<^sub>1 f = evale mem h\<^sub>2 f"
  by (metis assms empty_iff leval_varse'_det)

lemma eval\<^sub>p_no_prime:
  assumes "vars'\<^sub>p P = {}"
  shows "\<forall>h\<^sub>1 h\<^sub>2. eval\<^sub>p mem h\<^sub>1 P = eval\<^sub>p mem h\<^sub>2 P"
  using assms
proof (induct P)
  case (PCmp c f\<^sub>1 f\<^sub>2)
  hence a: "vars'\<^sub>e f\<^sub>1 = {}" "vars'\<^sub>e f\<^sub>2 = {}" by auto
  have "\<forall>h\<^sub>1 h\<^sub>2. eval\<^sub>e mem h\<^sub>1 f\<^sub>1 = eval\<^sub>e mem h\<^sub>2 f\<^sub>1"
     "\<forall>h\<^sub>1 h\<^sub>2. eval\<^sub>e mem h\<^sub>1 f\<^sub>2 = eval\<^sub>e mem h\<^sub>2 f\<^sub>2"
    using eval\<^sub>e_no_prime a by auto
  thus ?case by (metis eval\<^sub>p.simps(7))
next
  case (PAll x)
  then show ?case by fastforce
qed auto

lemma evalp_no_prime:
  assumes "varsp' P = {}"
  shows "\<forall>h\<^sub>1 h\<^sub>2. evalp mem h\<^sub>1 P = evalp mem h\<^sub>2 P"
  using assms by (metis empty_iff lpred_eval_varsp'_det)

lemma prime_vars\<^sub>e [simp]:
  shows "vars\<^sub>e (prime\<^sub>e f V) = vars\<^sub>e f - V"
  by (induct f, auto)


lemma prime_vars\<^sub>p [simp]:
  shows "vars\<^sub>p (prime\<^sub>p f V) = vars\<^sub>p f - V"
  by (induct f, auto)
end